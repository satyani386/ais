import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss']
})
export class ProductFilterComponent implements OnInit,OnChanges{
  @Input() Products: any;
  @Output() applyFilter = new EventEmitter();
  public isOpen = false;
  public ProductsObj: any;
  constructor() {
    if (this.Products) {
      this.ProductsObj = JSON.parse(JSON.stringify(this.Products));
    }
  }

  ngOnInit() {
  }
  ngOnChanges(changes:SimpleChanges){
    if(changes['Products']){
      this.ProductsObj = JSON.parse(JSON.stringify(this.Products));
    }
  }

  toggle() {
    this.isOpen = !this.isOpen
  }

  filter(condition) {
    if (condition) {
      this.ProductsObj = JSON.parse(JSON.stringify(this.Products));
    } else {
      this.Products = JSON.parse(JSON.stringify(this.ProductsObj));
    }
    this.toggle();

    const obj = [];

    this.ProductsObj.forEach(function(element){
      if(element.selected){
        obj.push(element.name);
      }
    });
    const data={
      data: obj,
      type:'AddressState'
    };

    this.applyFilter.emit(data);
  }
  checkAll(condition){
    this.Products.forEach((element:any) => {
      element.selected = condition;
    });
  }
}
