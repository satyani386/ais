import { Component, OnInit,Input,OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-state-filter',
  templateUrl: './state-filter.component.html',
  styleUrls: ['./state-filter.component.scss']
})
export class StateFilterComponent implements OnInit,OnChanges{
  @Input() States: any;
  @Output() applyFilter = new EventEmitter();
  public isOpen = false;
  public StatesObj: any;
  constructor() {
    if (this.States) {
      this.StatesObj = JSON.parse(JSON.stringify(this.States));
    }
  }

  ngOnInit() {
  }
  ngOnChanges(changes:SimpleChanges){
    if(changes['States']){
      this.StatesObj = JSON.parse(JSON.stringify(this.States));
    }
  }

  toggle() {
    this.isOpen = !this.isOpen
  }

  filter(condition) {
    if (condition) {
      this.StatesObj = JSON.parse(JSON.stringify(this.States));
    } else {
      this.States = JSON.parse(JSON.stringify(this.StatesObj));
    }
    this.toggle();
    const obj = [];

    this.StatesObj.forEach(function(element){
      if(element.selected){
        obj.push(element.name);
      }
    });
    const data={
      data: obj,
      type:'AddressState'
    };

    this.applyFilter.emit(data);
  }
  checkAll(condition){
    this.States.forEach((element:any) => {
      element.selected = condition;
    });
  }
}
