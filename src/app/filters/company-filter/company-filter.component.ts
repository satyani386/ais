import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-company-filter',
  templateUrl: './company-filter.component.html',
  styleUrls: ['./company-filter.component.scss']
})
export class CompanyFilterComponent implements OnInit,OnChanges{
  @Input() Company: any;
  @Output() applyFilter = new EventEmitter();
  public isOpen = false;
  public CompanyObj: any;
  constructor() {
    if (this.Company) {
      this.CompanyObj = JSON.parse(JSON.stringify(this.Company));
    }
  }

  ngOnInit() {
  }
  ngOnChanges(changes:SimpleChanges){
    if(changes['Company']){
      this.CompanyObj = JSON.parse(JSON.stringify(this.Company));
    }
  }

  toggle() {
    this.isOpen = !this.isOpen
  }

  filter(condition) {
    if (condition) {
      this.CompanyObj = JSON.parse(JSON.stringify(this.Company));
    } else {
      this.Company = JSON.parse(JSON.stringify(this.CompanyObj));
    }
    this.toggle();
    const obj = [];

    this.CompanyObj.forEach(function(element){
      if(element.selected){
        obj.push(element.name);
      }
    });
    const data={
      data: obj,
      type:'AddressState'
    };

    this.applyFilter.emit(data);
  }
  checkAll(condition){
    this.Company.forEach((element:any) => {
      element.selected = condition;
    });
  }
}
