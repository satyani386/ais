import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-status-filter',
  templateUrl: './status-filter.component.html',
  styleUrls: ['./status-filter.component.scss']
})
export class StatusFilterComponent implements OnInit,OnChanges{
  @Input() Status: any;
  @Output() applyFilter = new EventEmitter();
  public isOpen = false;
  public StatusObj: any;
  constructor() {
    if (this.Status) {
      this.StatusObj = JSON.parse(JSON.stringify(this.Status));
    }
  }

  ngOnInit() {
  }
  ngOnChanges(changes:SimpleChanges){
    if(changes['Status']){
      this.StatusObj = JSON.parse(JSON.stringify(this.Status));
    }
  }

  toggle() {
    this.isOpen = !this.isOpen
  }

  filter(condition) {
    if (condition) {
      this.StatusObj = JSON.parse(JSON.stringify(this.Status));
    } else {
      this.Status = JSON.parse(JSON.stringify(this.StatusObj));
    }
    this.toggle();
    const obj = [];

    this.StatusObj.forEach(function(element){
      if(element.selected){
        obj.push(element.name);
      }
    });
    const data={
      data: obj,
      type:'AddressState'
    };

    this.applyFilter.emit(data);
  }
  checkAll(condition){
    this.Status.forEach((element:any) => {
      element.selected = condition;
    });
  }
}
