import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import {
  AccordionModule,
  BsDatepickerModule,
  ModalModule,
  PaginationModule,
  TabsModule,
  TooltipModule
} from 'ngx-bootstrap';
import {CommonModule} from '@angular/common';
import {Router} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
  NgTableComponent,
  NgTableFilteringDirective,
  NgTablePagingDirective,
  NgTableSortingDirective
} from 'ng2-table/ng2-table';
import { DatepickerModule } from 'angular2-material-datepicker'

import {Daterangepicker} from "ng2-daterangepicker";

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MalihuScrollbarModule} from 'ngx-malihu-scrollbar';
import {AppRoutingModule} from './app.routes';
import {ServicesModule} from './services/services.module';
//services
import {AppAuthorizationHandler} from './services/authorization-handler/app.authorizationHandler.service';
import {AppManager} from './app.manager';
// //shared components
import {SearchUiComponent} from './components/shared/search-ui/search-ui.component';
//pipes
import {SearchPipe} from './pipes/search.pipe';
import {FilterPipe} from './pipes/filter.pipe';
//components
import {AppComponent} from './app.component';
import {FasComponent} from './components/fas/fas.component';
import {ErrorComponent} from './components/error/error.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
//import { ActivityComponent } from './components/activity/activity.component';
import {ViewSelectorComponent} from './components/view-selector/view-selector.component';
import {LeadsComponent} from './components/leads/leads.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {FollowupsComponent} from './components/followups/followups.component';
import {SearchComponent} from './components/search/search.component';
import {SearchFormComponent} from './components/search/search-form/search-form.component';
import {WorkspaceComponent} from './components/workspace/workspace.component';
import {TopControlComponent} from './components/leads/top-control/top-control.component';
import {NewLeadsComponent} from './components/leads/new-leads/new-leads.component';
import {MyLeadsComponent} from './components/leads/my-leads/my-leads.component';
import {StickyMenuComponent} from './components/sticky-menu/sticky-menu.component';
import {RecentActivityComponent} from './components/recent-activity/recent-activity.component';
import {ReportsComponent} from './components/reports/reports.component';
import {LinksComponent} from './components/links/links.component';
import {ProfileComponent} from './components/profile/profile.component';
import {SelectOptionsDirective} from './directives/select-options.directive';
import {ActivityHandler} from './components/activityHandler/ActivityHandler.component';
import {UnassignedLeadsComponent} from './components/leads/unassigned-leads/unassigned-leads.component';
import {IngeniusComponent} from './components/ingenius/ingenius.component';
import {SortDirective} from './directives/sort.directive';
import {PagenationPipe} from './pipes/pagenation.pipe';
import {UserleadComponent} from './components/leads/userlead/userlead.component';
import {SourceModalComponent} from './components/source-modal/source-modal.component';
import {TransferDataModalComponent} from './components/transfer-data-modal/transfer-data-modal.component';
import {MapsComponent} from './components/leads/maps/maps.component';
import {NotificationComponent} from './components/followups/closefolloup/notification.component';
import {KeysPipe, MyFolloupsPipe} from "./pipes/util.pipe";
import {FollowuprowclassDirective} from "./directives/followuprowclass.directive";
import {ScheduleFollowUpComponent} from "./components/schedule-follow-up/schedule-follow-up.component";
import {HTTP_INTERCEPTORS} from "@angular/common/http";

import {TokenInterceptor} from "./services/auth/auth.interceptor";
import { LeadModalComponent } from './components/leads/userlead/lead-modal/lead-modal.component';

import {StateFilterComponent} from './filters/state-filter/state-filter.component';
import {CompanyFilterComponent} from "./filters/company-filter/company-filter.component";
import {StatusFilterComponent} from "./filters/status-filter/status-filter.component";
import {ProductFilterComponent} from "./filters/product-filter/product-filter.component";

//import {LeadsUiAdaptorComponent} from "./components/leads-ui-adaptor/leads-ui-adaptor.component";

@NgModule({
  declarations: [
    AppComponent,
    LeadsComponent,
    FasComponent,
    ErrorComponent,
    PageNotFoundComponent,
    //ActivityComponent,
    ViewSelectorComponent,
    SearchUiComponent,
    FilterPipe,
    NgTableComponent,
    NgTableFilteringDirective,
    NgTablePagingDirective,
    NgTableSortingDirective,
    DashboardComponent,
    FollowupsComponent,
    SearchComponent,
    SearchFormComponent,
    WorkspaceComponent,
    TopControlComponent,
    NewLeadsComponent,
    MyLeadsComponent,
    StickyMenuComponent,
    RecentActivityComponent,
    ReportsComponent,
    LinksComponent,
    ProfileComponent,
    ActivityHandler,
    UserleadComponent,
    ScheduleFollowUpComponent,
    StateFilterComponent,
    StatusFilterComponent,
    ProductFilterComponent,
    CompanyFilterComponent,

    //LeadsUiAdaptorComponent,

    //Directives
    SelectOptionsDirective,
    SortDirective,
    FollowuprowclassDirective,
    UnassignedLeadsComponent,
    IngeniusComponent,
    PagenationPipe,
    MyFolloupsPipe,
    KeysPipe,

    SourceModalComponent,
    TransferDataModalComponent,
    MapsComponent,
    NotificationComponent,
    LeadModalComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    MalihuScrollbarModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    ServicesModule,
    Daterangepicker,
    DatepickerModule
  ],
  providers: [
    AppManager,
    AppAuthorizationHandler,
    {
      provide: APP_INITIALIZER,
      useFactory: appLaunch,
      deps: [AppManager, Injector],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  entryComponents: [
    NewLeadsComponent,
    UserleadComponent,
    SourceModalComponent,
    TransferDataModalComponent,
    NotificationComponent,
    MapsComponent,
    ScheduleFollowUpComponent,
    LeadModalComponent,
    StateFilterComponent,
    StatusFilterComponent,
    ProductFilterComponent,
    CompanyFilterComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function appLaunch(appManager: AppManager, injector: Injector): () => Promise<any> {
  return () => appManager.launch().then(() => {
    console.log('launch process succesfully triggered');
  }, () => {
    console.log('launch process failed. So falling back to error page');
    injector.get(Router).navigateByUrl('error');
  });
}
