export class FunctionalAreaSelector {
  public functionalAreasData: any;
  public defaultView: any;

  constructor() {
  }

  setFunctionalAreas(data) {
    this.functionalAreasData = data;
  }

  getFunctionalAreas() {
    return this.functionalAreasData;
  }

  setDefaultView(view) {
    this.defaultView = view;
    console.log('Deafult View: '+JSON.stringify(this.defaultView));
  }

  getDefaultView() {
    return this.defaultView;

  }
}
