import { TestBed, inject } from '@angular/core/testing';

import { FunctionalAreaSelector } from './functional-area-selector.service';

describe('FunctionalAreaSelectorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FunctionalAreaSelectorService]
    });
  });

  it('should be created', inject([FunctionalAreaSelectorService], (service: FunctionalAreaSelectorService) => {
    expect(service).toBeTruthy();
  }));
});
