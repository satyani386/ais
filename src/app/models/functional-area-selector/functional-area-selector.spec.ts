import { async, TestBed, inject } from '@angular/core/testing';

import { FunctionalAreaSelector } from './functional-area-selector';

import { HttpModule } from '@angular/http';

fdescribe('FunctionalAreaSelector', () => {
  let service: FunctionalAreaSelector;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [FunctionalAreaSelector]
    });
  });

  beforeEach(inject([FunctionalAreaSelector], s => {
    service = s;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
  // it('should get default view', async(() => {
  //   service.getDefaultView().subscribe(x => { 
  //     expect(x).toBeTruthy();
  //   });
  // }));

  // it('should get functional areas', async(() => {
  //   service.getFunctionalAreas().subscribe(x => { 
  //     expect(x).toBeTruthy();
  //   });
  // }));
  
}); 
