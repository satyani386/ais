import { AuthModel } from '../../models/auth/auth.model';
import { SessionModel } from '../../models/report/session';

export class AppCredentials {
    private _authToken:any;
    private _expiryToken:any;
     private _sessionToken:any;
    
    constructor(){}

    create({authToken, expiryToken}: AuthModel){
        this._authToken = authToken;
        this._expiryToken = expiryToken;
        return {authToken:this._authToken, expiryToken:this._expiryToken}
    }

    sessionCredentials({SessionToken}: SessionModel){
        this._sessionToken = SessionToken;
        return {sessionToken:this._sessionToken}
    }

};