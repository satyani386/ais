import {AuthModel} from '../../models/auth/auth.model';
import { SessionModel } from '../../models/report/session';

export class AppUserPrinciple {
  public credentials: any;
  public SessionCredentials: any;
  public profile: any;
  public permissions: any;
  public apisettings:any;

  public ContextObject: any;

  constructor() {
    this.ContextObject = {};
  }

  // set token credentials
  setCredentials(tokens): void {
    this.credentials = tokens;
  }

  // get token credentials
  getCredentials() {
    return this.credentials;
  }

   setSessionCredentials(SessionToken): void {
    this.SessionCredentials = SessionToken;
  }

  // get token credentials
  getSessionCredentials(): AuthModel {
    return this.SessionCredentials;
  }

  // set profile details
  setProfile(profile: any): void {
    this.profile = profile;
  } 

  getProfile(){
    return this.profile;
  }

  // set permission details
  setPermission(permissions: any): void {
    this.permissions = permissions;
  }

  setContextObject(type,data){
    this.ContextObject[type] = data;
  }
  getContextObject(type){
    return this.ContextObject[type];
  }
  removeContextObject(type){
    delete this.ContextObject['type']
  }
   setapisettings(data) {
    this.apisettings = data;
  }

  getFunctionalAreas() {
    return this.apisettings;
  }
}
