import {Directive, ElementRef, OnInit, Input, Renderer2} from '@angular/core';

import {AbstarctAdapterClass} from '../services/uiadaptors/AbstractAdaptor';

@Directive({
  selector: '[appValidValues]',
  providers: [AbstarctAdapterClass]
})

export class SelectOptionsDirective implements OnInit {
  @Input() state: any;

  constructor(public ele: ElementRef, private renderer: Renderer2, public _abstractAdaptor: AbstarctAdapterClass) {
  }

  getOptions(id) {
    let that = this;
    this._abstractAdaptor.getValidValues(this.state).subscribe(function (resp) {
      that.constructOptions(resp[id]);
    }, function (error) {
      console.log(error);
    });
  }

  constructOptions(obj) {
    let that = this;
    obj.forEach(function (item) {
      const child = document.createElement('option');
      child.setAttribute('value', item.code);

      const textNode = document.createTextNode(item.displayName);
      child.appendChild(textNode);

      that.renderer.appendChild(that.ele.nativeElement, child)
    })
  }

  ngOnInit() {
    this.getOptions(this.ele.nativeElement.getAttribute("id"));
  }
}
