import { Input, Directive, HostListener, ElementRef } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: '[appSort]'
})

export class SortDirective {
  @Input('data') data: any;
  @Input('sortObj') sortObj: any;
  @Input('reverse') reverse: any;
  constructor(private ref: ElementRef) {
  }

  @HostListener('click', ['$event'])
  click($event: any) {
    const DESC = 'fa-arrow-up';
    const ASC = 'fa-arrow-down';
    $event.stopPropagation();
    const key = $event.target.getAttribute('key');
    const args = [{
      name: key,
      reverse: this.reverse
    }]

    console.log(this.reverse);
    this.sortObj.forEach(function(item){
      args.push(item);
    });

    //fa-arrow-up

    $('.fa').removeClass(DESC);
    $('.fa').removeClass(ASC);
    $($event.target).closest('tr').find('.active').removeClass('active')
    $($event.target).addClass('active');

    if(this.reverse){
      $($event.target).find('i').addClass(DESC);
    }else{
      $($event.target).find('i').addClass(ASC);
    }

    this.filterData(args);
  }

  filterData(args) {
    const that =this;
    let direction = 1;
    if(args[0].reverse){
      direction = -1
    }

    const obj = JSON.parse(JSON.stringify(this.sortObj));
    obj.unshift(args[0].name)

    this.data.sort(function(a,b){
      let result;  
      for (let i = 0; i < obj.length; i++) {
        result = direction * that.compare(a,b,obj[i]);
        if (result !== 0) break;
      }
      return result;
    });
    this.reverse = !this.reverse;
  }

  compare(a,b,key){
    let x = a[key];
    let y = b[key];

    if(typeof x === 'string'){
      x = x.toLowerCase()
    }

    if(typeof y === 'string'){
      y = y.toLowerCase()
    }

    if (x < y) {
      return -1;
    }else if(x > y){
      return 1;
    }else if(x === y ){
      return 0;
    }
  }
}