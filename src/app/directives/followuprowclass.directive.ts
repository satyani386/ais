import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import * as moment from "moment";

@Directive({
  selector: '[appCustomClass]'
})

export class FollowuprowclassDirective implements OnInit  {
  @Input('appCustomClass') appCustomClass: string;

  constructor(private ele: ElementRef) {
  }

  ngOnInit() {
    const now = moment().format('YYMMDD');
    const then = moment(this.appCustomClass).format('YYMMDD');

    if(now !== then){
      this.ele.nativeElement.classList.add('bg-danger')
    }
  }
}
