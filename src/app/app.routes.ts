import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

//pages
import { AppComponent } from './app.component';
import { FasComponent } from './components/fas/fas.component';
import { ErrorComponent } from './components/error/error.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
//import { ActivityComponent } from './components/activity/activity.component';
import { ViewSelectorComponent } from './components/view-selector/view-selector.component';

//sub-pages
// import { HomeComponent } from './components/pages/home/home.component';
// //import { LeadsComponent } from './components/pages/leads/index/leads.component';
// //import { InsuredComponent } from './components/pages/leads/insured/insured.component';
// import { PolicyComponent } from './components/pages/policy/policy.component';
// import { MarketingComponent } from './components/pages/marketing/marketing.component';
// import { HelpComponent } from './components/pages/help/help.component';
// import { AdminComponent } from './components/pages/admin/admin.component';
import { WorkspaceComponent } from './components/workspace/workspace.component';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FollowupsComponent } from './components/followups/followups.component';
import { LeadsComponent } from './components/leads/leads.component';
import { SearchComponent } from './components/search/search.component';
const appRoutes: Routes = [
    {
        path: "",
        component: FasComponent,
        children:[
            {
                path:'',
                component: DashboardComponent
            },
            {
                path:'0',
                component: DashboardComponent
            },
            {
                path:'1',
                component: FollowupsComponent
            },
            {
                path:'2',
                component: LeadsComponent
            },
            {
                path:'5',
                component: SearchComponent
            }
        ]
    }

];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}
/*
,
	{
        path: "home",
        component: HomeComponent
    },
    {
        path: "leads/:id",
        children: [
            {
                path: '',
                component: LeadsComponent
            },
            {
                path: ':insured',
                component: LeadsComponent
            }
        ]
    },
    {
        path: "marketing/:id",
        component: MarketingComponent
    },
    {
        path: "policy/:id",
        component: PolicyComponent
    },
    {
        path: "help/:id",
        component: HelpComponent
    },
    {
        path: "admin",
        component: AdminComponent
    },
    {
        path: "**",
        component:PageNotFoundComponent
    },
    {
        path: "error",
        component: ErrorComponent
    }
*/
