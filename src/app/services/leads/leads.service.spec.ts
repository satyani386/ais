import { async, TestBed, inject } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { LeadsService } from './leads.service';

import { HttpModule, Http, Response, ResponseOptions, RequestMethod, RequestOptions, Headers, XHRBackend } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

fdescribe('LeadsService', () => {
  let service: LeadsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        LeadsService,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    });
  });

  it('should be created', inject([LeadsService], (service:LeadsService) => {
    expect(service).toBeTruthy();
  }));

  // getLeads
  it('should get leads', inject([LeadsService, XHRBackend], (service: LeadsService, mockBackEnd: MockBackend) => {
    let data;
    
    mockBackEnd.connections.subscribe((connection: MockConnection) => {
      // Check if http request type is correct
      expect(connection.request.method).toBe(RequestMethod.Get);

      // Check if the URL is correct
      expect(connection.request.url).toBe('assets/LEADS.json');

      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(''),
        status: 200
      })));
    });

    service.getLeads().subscribe(
      x => expect(x).toEqual(new Object())
    );
  }));

  // getLinks
  it('should get links', inject([LeadsService, XHRBackend], (service: LeadsService, mockBackEnd: MockBackend) => {
    mockBackEnd.connections.subscribe((connection: MockConnection) => {
      // Check if http request type is correct
      expect(connection.request.method).toBe(RequestMethod.Get);

      // Check if the URL is correct
      expect(connection.request.url).toBe('assets/Links.json');

      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(''),
        status: 200
      })));
    });

    service.getLinks().subscribe(
      x => expect(x).toEqual(new Object())
    );
  }));

});
