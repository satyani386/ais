import {Injectable} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {AppUserPrinciple} from '../../models/user-principle/app.userPrinciple';
import {AppCredentials} from '../../models/credentials/app.credentials';

import {AuthModel} from '../../models/auth/auth.model';
import {SessionModel} from '../../models/report/session';
import {Observable} from "rxjs/Observable";

@Injectable()
export class AppAuthorizationHandler {

  constructor(private _authService: AuthService,
              private _appUserPrinciple: AppUserPrinciple,
              private _appCredentials: AppCredentials) {
  }

  //get token from webservice
  getToken() {
    return new Promise((resolve, reject) => {
      this._authService.getToken()
        .subscribe((resp) => {
          //Todo remove static token
          

          //make an entry of the token
          // let credentials = this._appCredentials.create({ authToken, expiryToken });

          //make an entry to the app user principle
          this._appUserPrinciple.setCredentials(resp);

          //resolve with success
          resolve(resp);
        }, () => {
          resolve('Error Occured : While Fetching the token from server.');
        });

    });
  }

  // getSessionToken() {
  //   let data;
  //   return new Promise((resolve, reject) => {
  //     let sessionService = this._authService.getReportToken({}).subscribe(({SessionToken}: SessionModel) => {
  //       //make an entry of the token
  //       let sessionCredentials = this._appCredentials.sessionCredentials({SessionToken});

  //       //make an entry to the app user principle
  //       this._appUserPrinciple.setSessionCredentials(sessionCredentials);

  //       //resolve with success
  //       resolve({SessionToken});
  //     }, () => {
  //       reject('Error Occured : While Fetching the token from server.');
  //     });

  //   });
  // }
}

