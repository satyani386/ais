import { async, TestBed, inject } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { IngeniusService } from './ingenius.service';

import { HttpModule, Http, Response, ResponseOptions, RequestMethod, RequestOptions, Headers, XHRBackend } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

fdescribe('IngeniusService', () => {
  let service: IngeniusService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        IngeniusService,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    });
  });

  it('should be created', inject([IngeniusService], (service:IngeniusService) => {
    expect(service).toBeTruthy();
  }));

  // getApiSetting
  it('should get api settings', inject([IngeniusService, XHRBackend], (service: IngeniusService, mockBackEnd: MockBackend) => {
    mockBackEnd.connections.subscribe((connection: MockConnection) => {
      // Check if http request type is correct
      expect(connection.request.method).toBe(RequestMethod.Get);

      // Check if the URL is correct
      expect(connection.request.url).toBe('https://brdvwapp811.int.mgc.com/AISInGeniusDataService/api/iceapisettings/3000');

      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(''),
        status: 200
      })));
    });

    service.getApiSetting().subscribe(
      x => expect(x).toBe('')
    );
  }));

  // postEventLogs
  it('should post event logs', inject([IngeniusService, XHRBackend], (service: IngeniusService, mockBackEnd: MockBackend) => {
    mockBackEnd.connections.subscribe((connection: MockConnection) => {
      // Check if http request type is correct
      expect(connection.request.method).toBe(RequestMethod.Post);

      // Check if the URL is correct
      expect(connection.request.url).toBe('https://brdvwapp811.int.mgc.com/AISInGeniusDataService/api/eventlogs');

      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(''),
        status: 200
      })));
    });

    service.postEventLogs('').subscribe(
      x => expect(x).toBe('')
    );
  }));

  // partyNumber
  it('should search party number', inject([IngeniusService, XHRBackend], (service: IngeniusService, mockBackEnd: MockBackend) => {
    mockBackEnd.connections.subscribe((connection: MockConnection) => {
      // Check if http request type is correct
      expect(connection.request.method).toBe(RequestMethod.Post);

      // Check if the URL is correct
      expect(connection.request.url).toBe('https://brdvwapp811.int.mgc.com/AISInGeniusDataService/api/leadsearchlists');

      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(''),
        status: 200
      })));
    });

    service.partyNumber('').subscribe(
      x => expect(x).toBe('')
    );
  }));

});
