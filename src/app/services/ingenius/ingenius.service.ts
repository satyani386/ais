import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { EventData } from '../../components/ingenius/ingenius.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class IngeniusService {

    constructor(private http: Http) {
        
    }
  
    getApiSetting(): Observable<any> {
        console.log(" --get Setting request");
        return this.http.get(
           'https://brdvwapp811.int.mgc.com/AISInGeniusDataService/api/iceapisettings/3000'
        )
            .map(res => res.json())
            .catch(error => Observable.throw(error))
    }

  
    postEventLogs(eventData): Observable<any>  {
       
        console.log(" event data", eventData);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(
            'https://brdvwapp811.int.mgc.com/AISInGeniusDataService/api/eventlogs',
            eventData,
            options
        )
            .map(res => {
                return res.json()
            })
            .catch(err => Observable.throw(err))
    }
    partyNumber(phonedata): Observable<any> {
        //  console.log(" --post logs");
        // console.log(" event data", phonedata);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(
           'https://brdvwapp811.int.mgc.com/AISInGeniusDataService/api/leadsearchlists',
            phonedata, options)
            .map(res => {
                return res.json()
            })
            .catch(err => Observable.throw(err))
    }
   
   

}
