import { TestBed, inject } from '@angular/core/testing';

import { HideShowService } from './hide-show.service';

describe('HideShowService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HideShowService]
    });
  });

  it('should be created', inject([HideShowService], (service: HideShowService) => {
    expect(service).toBeTruthy();
  }));
});
