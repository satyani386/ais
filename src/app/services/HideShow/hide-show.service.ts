import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HideShowService {

  public States: any;

  private statesUrl = '/assets/config/';

  constructor(private _http: Http) {
    this.States = [];
  }

  private extractData(response: Response) {
    const _data = response.json();
    return _data || {};
  }

  private throwError(error) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  getStateHideShow(state): Observable<any> {
    return this._http.get(this.statesUrl + state + '/base/' + state + 'HideShowRules.json')
      .map(this.extractData)
      .catch(this.throwError);
  }

  // filterObject(obj) {
  //   let _data = [];
  //   return new Promise(function (resolve, reject) {
  //     if (Object.keys(obj).length > 0) {
  //       Object.keys(obj).forEach(function (item) {
  //         if ((obj[item] !== 'hide') && obj[item] !== 'false') {
  //           _data.push(item)
  //         }
  //       });
  //       resolve(_data);
  //     } else {
  //       reject([]);
  //     }
  //   })
  // }

  setStates(states) {
    this.States = states;
  }

  getStates() {
    return this.States
  }

  // getValidValues(state): Observable<any> {
  //   let that = this;
  //   return this._http.get(this.statesUrl + state + '/base/' + state + 'ValidValues.json')
  //     .map(function (response: Response) {
  //       that.States = response.json();
  //       return that.getStates();
  //     })
  //     .catch(this.throwError);
  // }
}

