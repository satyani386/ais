import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { ProfileService } from '../profile/profile.service';
import { PermissionService } from '../permission/permission.service';
import { UserProfileService } from '../user-profile/user-profile.service';
import { RecentItemsService } from '../recent-items/recent-items.service';
import { FunctionalAreaSelector } from '../../models/functional-area-selector/functional-area-selector';
import { ReportsService } from '../Reports/reports.service';
import { AppService } from '../app/app.service';
import { Observable } from "rxjs/Observable";
import { IngeniusService } from '../ingenius/ingenius.service';

@Injectable()
export class AppConfigurationHandler {

  constructor(private _appUserPrinciple: AppUserPrinciple,
    private _profileService: ProfileService,
    private _permissionService: PermissionService,
    private _userProfileService: UserProfileService,
    private _functionalAreaSelector: FunctionalAreaSelector,
    private _appService: AppService,
    private _recentItemsService: RecentItemsService,
    private _authService: AuthService,
    private _reportsService: ReportsService,private _ingeniusService: IngeniusService) {
  }

  getCredentials() {
    return this._appUserPrinciple.getCredentials();
  }

  getProfile() {
    return new Promise((resolve, reject) => {
      this._userProfileService.getProfile().subscribe((profileData) => {
        this._profileService.setProfile(profileData);
        this._appUserPrinciple.setProfile(profileData);
        resolve('Profile Data: Success');
      }, (error) => {
        console.log(error);
        console.log('Error occured while fetching the details of profile');
        //reject('Profile Data: Failure');
        resolve('Profile Data: Success');
      });
    });
  }


  getPermission() {
    return new Promise((resolve, reject) => {
      this._userProfileService.getPermission().subscribe((permission) => {
        this._permissionService.setPermission(permission);
        this._appUserPrinciple.setPermission(permission);
        resolve('Permission Data: Success');
      }, () => {
        console.log('Error occured while fetching permissions');
        // reject('Permission Data: Failure');
        resolve('Permission Data: Success');
      });
    });
  }
  
  getapisettings() {
    return new Promise((resolve, reject) => {
      this._ingeniusService.getApiSetting().subscribe((data) => {
        this._appUserPrinciple.setapisettings(data);
        resolve('Permission Data: Success');
        console.log(data);
      }, () => {
        console.log('Error occured while fetching permissions');
        // reject('Permission Data: Failure');
        resolve('Permission Data: Success');
        
      });
    });
  }

  getFunctionalAreas() {
    let promise = new Promise((resolve, reject) => {
      this._appService.getFunctionalAreas().subscribe((data) => {
        this._functionalAreaSelector.setFunctionalAreas(data);
        resolve('FAS Data:success');
      }, () => {
        console.log('Error occured while fetching FAS');
        reject('FAS Data:failure');
      });
    });
    return promise;
  }

  getDefaultView() {
    let promise = new Promise((resolve, reject) => {
      this._appService.getDefaultViewService().subscribe((data) => {
        this._functionalAreaSelector.setDefaultView(data);
        resolve('Default view Data:success');
        console.log(data);
      }, () => {
        console.log('error occured');
        reject('Default view Data:failure');
      });
    });
    return promise;
  }

  getReports() {
    //     let promise = new Promise((resolve, reject) => {
    //         this._reportsService.getReports().subscribe((data)=>{
    //             resolve('success');
    //         }, ()=>{
    //             console.log('error occured ');
    //             reject('failure');
    //         });
    //     });
    //     return promise;
  }

  getRecentItems() {

    // return this._recentItemsService.getRecentItems().map(function (resp) {
    //   return 'Success'
    // })
    //   .catch((err)=> Observable.throw('failure'));
    let promise = new Promise((resolve, reject) => {
      this._recentItemsService.getRecentItems().subscribe((data) => {
        resolve('Recent Items Data:success');
      }, () => {
        console.log('error occured ');
        reject('Recent Items Data:failure');
      });
    });
    return promise;
  }

}
