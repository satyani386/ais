import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { LinksService } from '../../services/links/links.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class LinksAdapterClass {
  Res: any = [];
  constructor(private http: Http, private _linksService: LinksService) {

  }

  getlinks()  {
    return this._linksService.getLinks().map(function (data) {
         return data 
        })
      .catch(err => Observable.throw(err) )
  }
     

}