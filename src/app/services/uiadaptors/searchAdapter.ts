import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { SearchService } from '../../services/search/search.service';

@Injectable()
export class SearchAdapterClass {
  constructor(private http: Http, private _searchService: SearchService) {

  }

  getSearchResults(searchFormData: any) {
    return this._searchService.getLeadSerchResults(searchFormData);
  }
    getInsuredSearchResults(searchFormData: any) {
    return this._searchService.getInsuredSerchResults(searchFormData);
  }

}