import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {IngeniusService} from '../../services/ingenius/ingenius.service';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class IngeniusAdapterClass {
  searchResults: any;
  public ConnectionObj: any;
  private subject = new Subject<any>();

  constructor(private http: Http, private _ingeniusService: IngeniusService) {

  }

  getConnectionObj() {
    return this.ConnectionObj;
  }

  setConnectionObj(obj) {
    this.ConnectionObj = obj;
  }

  getPhoneData(searchFormData): Observable<any> {
    return this._ingeniusService.partyNumber(searchFormData)
      .map(function (data) {
        return data
      })
      .catch(err => Observable.throw(err))
  }

  getApiSetting(): Observable<any> {
    return this._ingeniusService.getApiSetting()
      .map(function (data) {
        return data
      })
      .catch(err => Observable.throw(err))
  }

  postEventLog(eventData): Observable<any> {
    return this._ingeniusService.postEventLogs(eventData)
      .map(function (data) {
        return data
      })
      .catch(err => Observable.throw(err))
  }

  setOnCallResults(data) {
    this.subject.next({data: data});
  }

  getOnCallResults() {
    return this.subject.asObservable()
  }


}
