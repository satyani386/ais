import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ValidvaluesService } from '../validvalues/validvalues.service';
import { ValidationsService } from '../validations/validations.service';
import { LabelsService } from '../labels/labels.service';
import { PageValidationsService } from '../pagevalidation/page-validations.service';
import { HideShowService } from '../HideShow/Hide-Show.service';

@Injectable()
export class AbstarctAdapterClass {
  constructor(private http: Http, 
  private _validValuesService: ValidvaluesService,
  //   private _validationsService: ValidationsService,
  //   private _labelsService: LabelsService,
  //   private _pageValidationsService: PageValidationsService,
    // private _hideShowService: HideShowService,
  ) {

  }

  getValidValues(Data: any) {
    return this._validValuesService.getValidValues(Data);
  }
  // getValidations(Data: any) {
  //   return this._validationsService.getValidations(Data);
  // }
  // getHideShow(Data:any){
  //   return this._hideShowService.getStateHideShow(Data);
  // }
  
  // getLabels(Data: any) {
  //   return this._labelsService.getLabels(Data);
  // }
  // getPageValidations(Data: any) {
  //   return this._pageValidationsService.getPageValidations(Data);
  // }
}