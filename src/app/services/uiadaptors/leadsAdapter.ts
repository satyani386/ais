import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import {Subject} from 'rxjs/Subject';
import {LeadsService} from '../leads/leads.service';

@Injectable()
export class LeadAdapterClass {
  private subject = new Subject<any>();

  public onCallEvent: any;
  public MyLeadData: any;

  constructor(private http: Http, private _leadService: LeadsService) {

  }

  setOnCall(event) {
    this.onCallEvent = event;
    this.subject.next({event: event});
    console.log('connected 2')
  }

  getOnCall() {
    return this.subject.asObservable()
  }

  setOnCallData(data) {
    this.onCallEvent = data;
  }

  getOnCallData() {
    return this.onCallEvent;
  }

  getLeads() {
    return this._leadService.getLeads();
  }

  setOpenMyLead(data) {
    this.onCallEvent = data;
    this.subject.next({event: data});
  }

  getOpenMyLead() {

  }
}
