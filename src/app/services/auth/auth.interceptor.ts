import { Injectable, Injector } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

import { Observable } from 'rxjs/Observable';
import { AppUserPrinciple } from "../../models/user-principle/app.userPrinciple";
import { AuthService } from "./auth.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private inject: Injector) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const auth = this.inject.get(AuthService);
    const UserPrinciple = this.inject.get(AppUserPrinciple);


    request = request.clone({


      setHeaders: {
        //console.log(UserPrinciple.getCredentials());
        "app-token": `${UserPrinciple.getCredentials().access_token}`
      }
    });

    return next.handle(request)
      .catch((err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            console.log('Token refresh');
            return auth.refreshToken().switchMap((value: any) => {
              console.log('Token ', value);
              const httpHeaders = new HttpHeaders()
                .set('app-token', value.access_token);
              // const retry = request.clone({ headers: httpHeaders });
              const retry = request.clone({
                setHeaders: {
                  'app-token': value.access_token
                }
              });
              return next.handle(retry);
            })
          } else {
            Observable.throw(err);
          }
        }
      });
  }
}