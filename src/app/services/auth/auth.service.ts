import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthModel } from '../../models/auth/auth.model';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthService {

    private TokenData: any = {};

    private _authUrl: string = 'https://dev-mauth-int.aisinsurance.com/OAuthProvider/oauth/token?client_id=ais_wb2_dev1&client_secret=AIS_WB2.0_NonProd&grant_type=password';

    private _reportauthUrl: string = '/json-data-api/sessions';

    private _refreshToken = 'https://dev-mauth-int.aisinsurance.com/OAuthProvider/oauth/token';

    constructor(
        private _http: HttpClient,
        private _http1: Http,
        private UserPrinciple: AppUserPrinciple
    ) { }

    private extractData(response: Response) {
        const _data = response.json();
        console.log("_data=============>", _data)
        return _data || {};
    }

    private throwError(error) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
      getToken(): Observable<any> {
        const headers = new Headers();
        let body={};
        //headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._http1.post(this._authUrl,body, { headers: headers , withCredentials: true})
            .map((token) => {
                this.UserPrinciple.setCredentials(token.json());
                return token.json();
            }).catch(error => { 
                return Observable.throw(error) 
            });

    };

    // getToken1(): Observable<any> {
    //     let body = {
    //         SMENC: 'UTF-8',
    //         SMLOCALE: 'US-EN',
    //         USER: 'Sbodapunti',
    //         PASSWORD: 'narahcIAS@121',
    //         target: encodeURIComponent('https://dev-mauth-int.aisinsurance.com/OAuthProvider/oauth/token?client_id=ais_wb2_dev1&client_secret=AIS_WB2.0_NonProd&grant_type=password'),
    //         smquerydata: '',
    //         smauthreason: '0',
    //         smagentname: 'mth_aisint_ag',
    //         postpreservationdata: ''
    //     };

    //     const headers = new Headers();
    //     headers.append('Accept', 'application/json');
    //     headers.append('Content-Type', 'application/x-www-form-urlencoded');

    //     // const headers = new HttpHeaders({
    //     //     'Accept': 'application/json',
    //     //     'Content-Type': 'application/x-www-form-urlencoded',
    //     //     'type':'token'
    //     // });

    //     return this._http1.post(this._authUrl, body, { headers: headers })
    //         .map((token) => {
    //             return JSON.parse(localStorage.getItem('token'));
    //         });

    // };

    refreshToken(): Observable<any> {
        console.log('refresh token call');
        // const body = {
        //     grant_type: 'password',
        //     client_id: 'ais_wb2_dev1',
        //     client_secret: 'AIS_WB2.0_NonProd',
        //     refresh_token: this.UserPrinciple.getCredentials().refresh_token
        // };


        // const headers = new HttpHeaders({
        //     'Accept': 'application/json',
        //     'Content-Type': 'application/x-www-form-urlencoded',
        //     'type':'token'
        // });

        const headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        const body = `grant_type=password&client_id=ais_wb2_dev1&client_secret=AIS_WB2.0_NonProd&refresh_token=${this.UserPrinciple.getCredentials().refresh_token}`;

        console.log(body);

        return this._http1.post(this._refreshToken, body, { headers: headers, withCredentials: true })
            .map((token) => {
                console.log('Refresh token resp')
                this.UserPrinciple.setCredentials(token);
                return Observable.of(token).delay(200);
            }).catch(error => { 
                console.log('Refresh token error')
                return Observable.throw(error) 
            });
    };


    // getReportToken(body: any): Observable<any> {
    //     const headers = new HttpHeaders({
    //         'XIServerName': 'brqalmst211',
    //         'XPort': '34952',
    //         'XProjectName': 'AIS Business Reporting',
    //         'XUsername': "WBJson",
    //         'XPassword': 'WBjson',
    //         'XAuthMode': '1',
    //         'Content-Type': 'Accept: application/vnd.mstr.dataapi.v0+json',
    //         'Accept': 'Accept: application/vnd.mstr.dataapi.v0+json'
    //     });
    //     console.log(headers);
    //     return this._http.post(this._reportauthUrl, body, { headers: headers })
    //         .map(this.extractData)
    //         .do((tokenResponse: AuthModel) => {
    //             tokenResponse //will have to actual format as mentioned in flow diagram
    //         }).catch(this.throwError)

    // };
}