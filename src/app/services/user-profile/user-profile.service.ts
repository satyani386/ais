import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { profileData } from "../../components/profile/profile.component";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class UserProfileService {

  private _profileUrl: string = '/AISWBCoreDataService/api/appuserprofiles/';
  private _permissionUrl: string = "/AISWBCoreDataService/api/appsecurityroles/";
  private _quickLinksUrl: string = "/AISWBCoreDataService/api/appuserquicklinks";
  private _saveQuickLinksUrl: string = "/AISWBCoreDataService/api/appuserquicklinks/edit";
  private _editProfileUrl: string = '/AISWBCoreDataService/api/appuserprofiles/edit/';
  public profileCachedData;
  public UserPermissionData;
  public QuickLinksData;

  constructor(private _http: HttpClient, private userPrinciple: AppUserPrinciple, private UserPrinciple: AppUserPrinciple, private http: Http) {
    this.profileCachedData = this.userPrinciple.getProfile();
  }

  private errorCatch(error) {
    console.log('Error in geting profile');
    return Observable.throw(error);
  }

  public getProfile() {
    const that = this;
    if (this.profileCachedData) {
      return this.profileCachedData;
    } else {
      return this._http.get(this._profileUrl)
        .map((profile) => {
          console.log('Profile call success');
          that.profileCachedData = profile;
          that.userPrinciple.setProfile(that.profileCachedData);
          return that.profileCachedData;
        })
        .catch(this.errorCatch)
    }
  }

  // Universal call for profile and sales goales
  updateProfile(profileData, id): Observable<any> {
    console.log(" event data", profileData);
    const httpHeaders = new HttpHeaders()
      .set('content-type', 'application/json');

    return this._http.post(this._editProfileUrl + id,
      profileData, { headers: httpHeaders })
      .map(res => {
        return res;
      })
      .catch(err => Observable.throw(err))
  }

  public getPermission(): Observable<any> {
    if (this.UserPermissionData) {
      return Observable.of(this.UserPermissionData);
    } else {
      return this._http.get(this._permissionUrl)
        .map(res => { return res })
        .do((data) => {
          this.UserPermissionData = data[0];
        });
    }
  }

  getQuickLinks(): Observable<any> {
    if (this.QuickLinksData) {
      return Observable.of(this.QuickLinksData);
    } else {
      return this._http.get(this._quickLinksUrl)
        .map(res => { return res })
        .do((data: any) => {
          this.QuickLinksData = data;
          this.QuickLinksData.sort(function (a, b) { return a.sortOrder - b });
        });
    }
  }
  saveQuickLinks(obj): Observable<any> {
    const httpHeaders = new HttpHeaders()
      .set('content-type', 'application/json');
    return this._http.post(this._saveQuickLinksUrl, obj, { headers: httpHeaders })
      .map(res => { return res })
      .do((data: any) => {
        this.QuickLinksData = data;
        this.QuickLinksData.sort(function (a, b) { return a.sortOrder - b });
      });
  }
}
