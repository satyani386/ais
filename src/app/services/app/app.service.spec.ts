import { async, TestBed, inject } from '@angular/core/testing';

import { AppService } from './app.service';

import { HttpModule } from '@angular/http';

fdescribe('AppService', () => {
  let service: AppService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [AppService]
    });
  });

  beforeEach(inject([AppService], s => {
    service = s;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
  it('should get default view', async(() => {
    service.getDefaultViewService().subscribe(x => { 
      expect(x).toBeTruthy();
    });
  }));

  it('should get functional areas', async(() => {
    service.getFunctionalAreas().subscribe(x => { 
      expect(x).toBeTruthy();
    });
  }));
  
}); 
