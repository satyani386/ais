import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class AppService {

    public _url: string = "assets/FAS.json";
    public _url1: string = "assets/Default.json";
    public cachedData;

    constructor(private _http: Http) {
        //TODO
    }

    private extractData(response: Response) {
        const _data = response.json();
        return _data || {};
    }

    private throwError(error) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
    getFunctionalAreas(): Observable<any> {
        return this._http
            .get(this._url)
            .map(this.extractData)
            .catch(this.throwError)
    }

    getDefaultViewService(): Observable<any> {
        return this._http
            .get('assets/Default.json')
            .map(this.extractData)
            .catch(this.throwError)
    }


}
