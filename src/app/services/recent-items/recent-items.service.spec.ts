import { TestBed, inject } from '@angular/core/testing';

import { RecentItemsService } from './recent-items.service';

describe('RecentItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecentItemsService]
    });
  });

  it('should be created', inject([RecentItemsService], (service: RecentItemsService) => {
    expect(service).toBeTruthy();
  }));
});
