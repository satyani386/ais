import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class UtilService {
  public FasViewClick = new Subject();
  constructor() { }

  setFasView(data) {
    this.FasViewClick.next(data);
  }

  getFasView() {
    return this.FasViewClick.asObservable();
  }

  sortDefault(data , condition){
    const that = this;
    data.sort(function(a,b){
      let result;  
      result = that.compare(a,b,condition);
      return result;
    });
  }

  compare(a,b,key){
    let x = a[key];
    let y = b[key];

    if(typeof x === 'string'){
      x = x.toLowerCase()
    }

    if(typeof y === 'string'){
      y = y.toLowerCase()
    }

    if (x < y) {
      return -1;
    }else if(x > y){
      return 1;
    }else if(x === y ){
      return 0;
    }
  }
}