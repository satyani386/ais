import { TestBed, inject, async } from '@angular/core/testing';

import { LinksService } from './links.service';

import { Http, HttpModule } from '@angular/http';

describe('LinksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [LinksService]
    });
  });

  it('should be created', inject([LinksService], (service: LinksService) => {
    expect(service).toBeTruthy();
  }));

  it('should get links', async(() => {
    let _http: Http
    let service = new LinksService(_http);
  
    service.getLinks().subscribe(
      (result) => {
        expect(result).toBeDefined();
      }
    );
  }));

});
