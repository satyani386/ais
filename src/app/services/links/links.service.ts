import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class LinksService {

  constructor(private _http: Http) { }
    private extractData(response: Response) {
    const _data = response.json();
    return _data || {};
  }

  private throwError(error) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
   getLinks(): Observable<any> {
    return this._http
      .get('assets/Links.json')
      .map(this.extractData)
      .catch(this.throwError)
  }

}
