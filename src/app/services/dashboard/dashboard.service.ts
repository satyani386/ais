import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { profileData } from "../../components/profile/profile.component";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class DashboardService {
  private GetSalesGoalsUrl = '/AISWBCoreDataService/api/dashboardunitsalesgoals';
  private GetSUnitSalesUrl = '/AISWBCoreDataService/api/dashboardunitsales';
   private GetQuotesUrl = '/AISWBCoreDataService/api/dashboardquotes';
      private GetBoundPremiumUrl = '/AISWBCoreDataService/api/dashboardboundpremiums';
        private GetRankingUrl = '/AISWBCoreDataService/api/dashboardrankings';
          private GetAlertsUrl = '/AISWBCoreDataService/api/dashboardalerts';
           private GetMyCallLogUrl = '/AISWBCoreDataService/api/dashboardmycalllogs';
             private GetUnavailableUrl = '/AISWBCoreDataService/api/dashboardunavailabledurations';

  constructor(private _http: HttpClient, private userPrinciple: AppUserPrinciple, private UserPrinciple: AppUserPrinciple) { }

  getSalesGoals(): Observable<any> {
    return this._http.get(this.GetSalesGoalsUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }

  getUnitSales(): Observable<any> {
    return this._http.get(this.GetSUnitSalesUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }
  getQuotes(): Observable<any> {
    return this._http.get(this.GetQuotesUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }
    getBoundPremium(): Observable<any> {
    return this._http.get(this.GetBoundPremiumUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }
    getRanking(): Observable<any> {
    return this._http.get(this.GetRankingUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }
    getAlerts(): Observable<any> {
    return this._http.get(this.GetAlertsUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }
    getMyCallLog(): Observable<any> {
    return this._http.get(this.GetMyCallLogUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }
       getUnavailable(): Observable<any> {
    return this._http.get(this.GetUnavailableUrl)
      .map(res => { return res })
      .catch(error => Observable.throw(error));
  }
}
