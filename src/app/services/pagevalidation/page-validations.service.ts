

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PageValidationsService {

  public States: any;

  private statesUrl = '/assets/config/';

  constructor(private _http: Http) {
    this.States = [];
  }

  private extractData(response: Response) {
    const _data = response.json();
    return _data || {};
  }

  private throwError(error) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  getPageValidations(state): Observable<any> {
    return this._http.get(this.statesUrl + state + '/base/' + state + 'ValidValues.json')
      .map(this.extractData)
      .catch(this.throwError);
  }
}
