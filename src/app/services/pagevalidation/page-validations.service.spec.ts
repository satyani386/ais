import { TestBed, inject } from '@angular/core/testing';

import { PageValidationsService } from './page-validations.service';

describe('PageValidationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PageValidationsService]
    });
  });

  it('should be created', inject([PageValidationsService], (service: PageValidationsService) => {
    expect(service).toBeTruthy();
  }));
});
