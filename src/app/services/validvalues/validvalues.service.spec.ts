import { TestBed, inject } from '@angular/core/testing';

import { ValidvaluesService } from './validvalues.service';

describe('ValidvaluesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidvaluesService]
    });
  });

  it('should be created', inject([ValidvaluesService], (service: ValidvaluesService) => {
    expect(service).toBeTruthy();
  }));
});
