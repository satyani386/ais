import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';

@Injectable()
export class SearchService {

    public _url: string = "/AISWBESIDataService/api/LeadSearchLists";
    public _Insuredurl: string = "/AISWBESIDataService/api/InsuredSearchLists";
    constructor(private _http: HttpClient,  private UserPrinciple: AppUserPrinciple) {

     }

    getLeadSerchResults(obj: any): Observable<any> {
        return this._http
            .post(this._url,obj,{})
            .map((response) => { return response })
            .catch(error => Observable.throw(error))
    }
   getInsuredSerchResults(obj: any): Observable<any> {
        return this._http
            .post(this._Insuredurl,obj,{})
            .map((response) => { return response })
            .catch(error => Observable.throw(error))
    }


}
