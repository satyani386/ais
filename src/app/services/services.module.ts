
import { NgModule, APP_INITIALIZER, Injector } from '@angular/core';

//services
import { AppAuthorizationHandler } from './authorization-handler/app.authorizationHandler.service';
import { AppConfigurationHandler } from './configuration-handler/app.configurationHandler.service';
import { SharedService } from './shared/shared.service';
import { AuthService } from './auth/auth.service';
import { SearchService } from './search/search.service';
import { LeadsService } from './leads/leads.service';
import { AppService } from './app/app.service';
import { ReportsService } from './Reports/reports.service';
import { ProfileService } from './profile/profile.service';
import { ValidationsService } from './validations/validations.service';
import { ValidvaluesService } from './validvalues/validvalues.service';
import { PageValidationsService } from './pagevalidation/page-validations.service';
import { LabelsService } from './labels/labels.service';
import { HideShowService } from './HideShow/hide-show.service';
import { PermissionService } from './permission/permission.service';
import { UserProfileService } from './user-profile/user-profile.service';
import { RecentItemsService } from './recent-items/recent-items.service';
import { IngeniusService } from './ingenius/ingenius.service';
import { LinksService } from './links/links.service';
import { UtilService } from './util.service';
import { DashboardService } from './dashboard/dashboard.service';

//Modals and adapters
import { AppUserPrinciple } from '../models/user-principle/app.userPrinciple';
import { FunctionalAreaSelector } from '../models/functional-area-selector/functional-area-selector';
import { AppCredentials } from '../models/credentials/app.credentials';
import { IngeniusAdapterClass } from '../services/uiadaptors/IngeniusAdapter';
import { LinksAdapterClass } from '../services/uiadaptors/linksAdapter';
import { LeadAdapterClass } from '../services/uiadaptors/leadsAdapter';
import { AbstarctAdapterClass } from '../services/uiadaptors/AbstractAdaptor';
import { DashBoardAdapterClass } from '../services/uiadaptors/dashBoardAdapter';
import { FollowUpAdapterClass } from '../services/uiadaptors/followUpAdapter';
import { QuickLinksAdapterClass } from '../services/uiadaptors/quickLinksAdapter';
import { RecentActivityAdapterClass } from '../services/uiadaptors/recentActivityAdapter';
import { ReportsAdapterClass } from '../services/uiadaptors/reportsAdapter';
import { SearchAdapterClass } from '../services/uiadaptors/searchAdapter';
import { ProfileAdapterClass } from '../services/uiadaptors/profileAdapter';



@NgModule({
  declarations: [],
  imports: [],
  providers: [
    SearchService,
    AppService,
    LeadsService,
    LinksService,
    AuthService,
    ProfileService,
    PermissionService,
    ReportsService,
    HideShowService,
    PageValidationsService,
    LabelsService,
    ValidationsService,
    ValidvaluesService,
    AppConfigurationHandler,
    UserProfileService,
    RecentItemsService,
    SharedService,
    IngeniusService,
    UtilService,
    DashboardService,
    IngeniusAdapterClass,
    LinksAdapterClass,
    LeadAdapterClass,
    AbstarctAdapterClass,

    AppUserPrinciple,
    FunctionalAreaSelector,
    AppCredentials
  ]
})
export class ServicesModule { }
