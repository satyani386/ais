import {Injectable}     from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class SharedService {

    isVisibleSource: BehaviorSubject<any> = new BehaviorSubject('');
    isBooleanSource: BehaviorSubject<any> = new BehaviorSubject(false);

    constructor() { }
}
