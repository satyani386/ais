import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {

    transform(items,args):any{
        const Data = [];
        if(Object.keys(args).length === 0){
            return items;
        }

        Object.keys(args).forEach(function(x){
            args[x].forEach(function(y){
                items.forEach(function(a){
                    if(a[x] === y){
                        Data.push(a);
                    }
                });
            })
        });
        return Data;
    }
}