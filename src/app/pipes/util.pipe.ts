import {PipeTransform, Pipe} from '@angular/core';
import * as moment from "moment";

@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value): any {
    return Object.keys(value);
  }
}

@Pipe({name: 'myfollowups'})
export class MyFolloupsPipe implements PipeTransform {
  transform(value, args): any {
    const data = [];
    if(value){
      if(args === 'all'){
        return value;
      }

      value.forEach(function (item) {
        if(args === 'today'){
          if(moment(item.date).format('L') === moment().format('L')){
            data.push(item)
          }
        }else if(args === 'open'){
          if(!item.closed){
            data.push(item)
          }
        }else if(args === 'closed'){
          if(item.closed){
            data.push(item)
          }
        }
      });
      return data;
    }
  }
}
