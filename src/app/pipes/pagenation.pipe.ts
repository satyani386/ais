import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pagenation'
})
export class PagenationPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    console.log(value,args);
    let data = [];
    if(args === 1){
      for(let i = 0 ; i < 5; i ++){
        if(value[i]){
          data.push(value[i])
        }
      }
    }else{
      for(let i = ((args  - 1) * 5) ; i < ((args  - 1) * 5) + 5; i ++){
        if(value[i]){
          data.push(value[i])
        }
      }
    }
    return data;

  }
}
