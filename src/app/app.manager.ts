import { Injectable } from '@angular/core';
import { AppAuthorizationHandler } from './services/authorization-handler/app.authorizationHandler.service';
import { AppConfigurationHandler } from './services/configuration-handler/app.configurationHandler.service';
import { RecentItemsService } from './services/recent-items/recent-items.service';
import { Observable } from "rxjs/Observable";

@Injectable()
export class AppManager {

    constructor(
        private _appAuthorizationHandler: AppAuthorizationHandler,
        private _appConfigurationHandler: AppConfigurationHandler,
        private _recentItemsService: RecentItemsService,

    ) { }

    launch() {
        return new Promise((resolve, reject) => {
            this._appAuthorizationHandler.getToken().then(() => {
                this.callConfigurationHandler().then(() => {
                    resolve('Profile Data Fetched: Success');
                }, () => {
                    reject('Profile Data Fetched: Failed');
                });
            }).catch((error) => {
                reject('Tokens Fetched: Failed');
            });
        });
    }
    //  getSession() {
    //     return new Promise((resolve, reject) => {
    //         this._appAuthorizationHandler.getSessionToken().then(() => {
    //             this.callConfigurationHandler().then(() => {
    //                 resolve('Profile Data Fetched: Success');
    //             }, () => {
    //                 reject('Profile Data Fetched: Failed');
    //             });
    //         }).catch(() => {
    //             reject('Tokens Fetched: Failed');
    //         });
    //     });

    // }


    callConfigurationHandler() {
        this._appConfigurationHandler.getCredentials();
        return this.getProfile();
    }

    getProfile() {
        return new Promise((resolve, reject) => {
            this._appConfigurationHandler.getProfile().then(() => {
                console.log('Profile get success');
                this.getPermissions().then(() => {
                    resolve('Permission Data Fetched: Success');
                }, () => {
                    reject('Permission Data Fetched: Failed');
                });
            }).catch(() => {
                console.log('Profile get failed');
                reject('Profile Data Fetched: Failed');
            });
        });
    }
    getPermissions() {
        return new Promise((resolve, reject) => {
            this._appConfigurationHandler.getPermission().then(() => {
                this.getapisetings().then(() => {
                    resolve('Functional Area Data Fetched: Success');
                }, () => {
                    reject('Functional Area Data Fetched: Failed');
                });
            }).catch(() => {
                reject('Permission Data Fetched: Failed');
            });
        });
    }
     getapisetings() {
        return new Promise((resolve, reject) => {
            this._appConfigurationHandler.getapisettings().then(() => {
                this.getFunctionalAreas().then(() => {
                    resolve('Functional Area Data Fetched: Success');
                }, () => {
                    reject('Functional Area Data Fetched: Failed');
                });
            }).catch(() => {
                reject('Permission Data Fetched: Failed');
            });
        });
    }


    getFunctionalAreas() {
        return new Promise((resolve, reject) => {
            this._appConfigurationHandler.getFunctionalAreas().then(() => {
                this.getDefaultView().then(() => {
                    resolve('Recent Items Data Fetched: Success');
                }, () => {
                    reject('Recent Items Data Fetched: Failed');
                });
            }).catch(() => {
                reject('Functional Area Data Fetched: Failed');
            });
       });

    }
    getDefaultView() {
        return new Promise((resolve, reject) => {
            this._appConfigurationHandler.getDefaultView().then(() => {
                this.getRecentItems().then(() => {
                    resolve('Recent Items Data Fetched: Success');
                }, () => {
                    reject('Recent Items Data Fetched: Failed');
                });
            }).catch(() => {
                reject('Default View Fetched: Failed');
            });
        });
    }

    getRecentItems() {
        return new Promise((resolve, reject) => {
            this._appConfigurationHandler.getRecentItems().then(() => {
                this.configureUI();
                this.configureUI();
                resolve('Successfully obtained data');
            }, () => {
                reject('Data not obtained');
            });
        });
    }
    configureUI() {

        // return new Promise((resolve, reject) => {
        //     this._appAuthorizationHandler.getSessionToken().then(()=>{
        //         // this._appConfigurationHandler.getReports().then(()=>{
        //             resolve('Recent Items Data Fetched: Success');
        //         },()=>{
        //             reject('Recent Items Data Fetched: Failed');
        //         });
        //     }, ()=>{
        //         reject('Functional Area Data Fetched: Failed');
        //     });
        // });

    }

}

