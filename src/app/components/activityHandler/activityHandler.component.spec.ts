import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityHandler } from './ActivityHandler.component';

describe('LeadsUiAdaptorComponent', () => {
  let component: ActivityHandler;
  let fixture: ComponentFixture<ActivityHandler>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityHandler ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityHandler);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
