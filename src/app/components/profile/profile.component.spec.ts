import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { ProfileComponent } from './profile.component';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileComponent ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // edit
  it('should allow edit of Personal Information', () => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#profile-edit'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    // Inputs are added to DOM
    let displayNameInput = fixture.debugElement.query(By.css('#info-displayname-input'));
    let timeZoneInput = fixture.debugElement.query(By.css('#info-timezone-input'));
    let profilePictureInput = fixture.debugElement.query(By.css('#info-profile-picture-input'));

    expect(displayNameInput).toBeTruthy();
    expect(timeZoneInput).toBeTruthy();
    expect(profilePictureInput).toBeTruthy();
    expect(component.Edit.info).toEqual(true);
  });

  it('should allow edit of Sales Goals', () => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#sales-edit'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    let unitSalesInput = fixture.debugElement.query(By.css('#unitsales-input'));
    let premiumInput = fixture.debugElement.query(By.css('#premium-input'));

    expect(unitSalesInput).toBeTruthy();
    expect(premiumInput).toBeTruthy();
    expect(component.Edit.sales).toEqual(true);
  });

  it('should allow edit of Quick Links', () => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#links-edit'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    // Check for 10 links
    let displayNameInput:any;
    let urlInput:any;
    for (var i = 1; i <= 10; i++) {
      displayNameInput = fixture.debugElement.query(By.css('#links-displayname-input-' + i));
      urlInput = fixture.debugElement.query(By.css('#links-url-input-' + i));
      expect(displayNameInput).toBeTruthy();
      expect(urlInput).toBeTruthy();
    }

    expect(component.Edit.links).toEqual(true);
  });

  // save
  // NgModel is asynchronous so use async and whenStable
  it('should save changes to Display Name in Personal Information', async(() => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#profile-edit'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let input = fixture.debugElement.query(By.css('#info-displayname-input'));
      let inputEl = input.nativeElement;
      let newDisplayName = 'New Name';
      inputEl.value = newDisplayName;
      // Trigger NgModel's binding which listens to input event which is dispatched only on actual user input
      inputEl.dispatchEvent(new Event('input'));
      
      // Click Save
      btn = fixture.debugElement.query(By.css('#profile-save'));
      btn.triggerEventHandler('click', null);

      fixture.detectChanges();

      expect(component.ProfileInfo.display_name).toEqual(newDisplayName);
      expect(component.Edit.info).toEqual(false);
    })
  }));

  // cancel
  // NgModel is asynchronous so use async and whenStable
  it('should cancel changes in Personal Information', async(() => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#profile-edit'));
    btn.triggerEventHandler('click', null);

    let oldData = JSON.parse(JSON.stringify(component.ProfileInfo));

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // Edit display name
      let input = fixture.debugElement.query(By.css('#info-displayname-input'));
      let inputEl = input.nativeElement;
      inputEl.value = 'New Name';
      // Trigger NgModel's binding which listens to input event which is dispatched only on actual user input
      inputEl.dispatchEvent(new Event('input'));

      // Click Cancel
      btn = fixture.debugElement.query(By.css('#profile-cancel'));
      btn.triggerEventHandler('click', null);

      fixture.detectChanges();

      expect(component.ProfileInfo).toEqual(oldData);
    })
  }));

  it('should cancel changes in Sales Goals', async(() => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#sales-edit'));
    btn.triggerEventHandler('click', null);

    let oldData = JSON.parse(JSON.stringify(component.SalesInfo));

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let input = fixture.debugElement.query(By.css('#unitsales-input'));
      let inputEl = input.nativeElement;
      inputEl.value = 1000;
      // Trigger NgModel's binding which listens to input event which is dispatched only on actual user input
      inputEl.dispatchEvent(new Event('input'));

      // Click Cancel
      btn = fixture.debugElement.query(By.css('#sales-cancel'));
      btn.triggerEventHandler('click', null);

      fixture.detectChanges();

      expect(component.SalesInfo).toEqual(oldData);
    })
  }));

  it('should cancel changes in Quick Links', async(() => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#links-edit'));
    btn.triggerEventHandler('click', null);

    let oldData = JSON.parse(JSON.stringify(component.QuickLinks));

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let input = fixture.debugElement.query(By.css('#links-displayname-input-1'));
      let inputEl = input.nativeElement;
      inputEl.value = 'New Link';
      // Trigger NgModel's binding which listens to input event which is dispatched only on actual user input
      inputEl.dispatchEvent(new Event('input'));

      // Click Cancel
      btn = fixture.debugElement.query(By.css('#links-cancel'));
      btn.triggerEventHandler('click', null);

      fixture.detectChanges();

      expect(component.QuickLinks).toEqual(oldData);
    })
  }));

  let event = {
    'bubbles': false,
    'returnValue': true,
    'target': {
      'readyState': 2,
      'result': 'data:image/jpeg;base64, FOO',
      'onloadend': 'bar'
    }
  };

  // selectFile
  it('should allow select file for profile picture', async(() => {
    // Spy on selectFile function to check if called
    spyOn(component, 'selectFile').and.callThrough();

    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#profile-edit'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let input = fixture.debugElement.query(By.css('#info-profile-picture-input'));
      // Trigger event to call function
      input.triggerEventHandler('change', event);

      fixture.detectChanges();
  
      expect(component.selectFile).toHaveBeenCalled();
    })
  }));

  // it('selectFile should process event', async(() => {
  //   component.selectFile(event);

  //   fixture.detectChanges();

  //   fixture.whenStable().then(() => {
  //     expect(component.ProfileInfo.picture).toEqual(event.target.result);
  //   });
  // }));

  // defaultPicture
  it('should show Default Picture in Personal Information', async(() => {
    // Click on pencil icon
    let btn = fixture.debugElement.query(By.css('#profile-edit'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // Click Default Picture
      btn = fixture.debugElement.query(By.css('#info-profile-picture-default'));
      btn.triggerEventHandler('click', null);

      fixture.detectChanges();

      expect(component.ProfileInfo['picture']).not.toBeDefined();
    })
  }));

});