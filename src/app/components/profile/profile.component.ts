import { Component, OnInit } from '@angular/core';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { UserProfileService } from '../../services/user-profile/user-profile.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public Edit = {
    info: false,
    links: false,
    sales: false
  };

  public ProfileInfo: any;
  public QuickLinks: any;
  public SalesInfo: any;

  public OldData = {};

  constructor(private _userprinciple: AppUserPrinciple, private _userprofile: UserProfileService) {
    const that = this;
    this.ProfileInfo = this._userprinciple.getProfile();
    this.QuickLinks = this._userprofile.QuickLinksData;
    this._userprofile.getQuickLinks().subscribe(function (resp) {
      that.initQuickLinks(resp);
    }, function (error) {
      console.log(error);
    });
    console.log(this.ProfileInfo);
    this.SalesInfo = {
      unit_sales: '75',
      premium: '2,000'
    };

    this.QuickLinks = [
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":0
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":1
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":2
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":3
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":4
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":5
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":6
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":7
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":8
      },
      {
        "displayName": "",
        "uriLink": "",
        "appUserId": this.ProfileInfo.id,
        "sortOrder":9
      }
    ]
  }

  ngOnInit() {
  }

  initQuickLinks(data) {
    const that = this;
    data.forEach((element,index) => {
      that.QuickLinks[index] = element;
    });
  }

  edit(type) {
    if (type === 'info') {
      this.OldData[type] = JSON.parse(JSON.stringify(this.ProfileInfo));
    } else if (type === 'sales') {
      this.OldData[type] = JSON.parse(JSON.stringify(this.SalesInfo));
    } else if (type === 'links') {
      this.OldData[type] = JSON.parse(JSON.stringify(this.QuickLinks));
    }
    this.Edit[type] = true;
  }
  deleteLink(link){
    link.displayName = '';
    link.uriLink = '';
  }

  

  save(type) {
    this.Edit[type] = false;
    if (type != 'links') {
      const reqobj = {
        "appUserDisplayName": this.ProfileInfo.appUserDisplayName,
        "appUserTimeZone": this.ProfileInfo.appUserTimeZone,
        "monthlyNoOfSalesGoal": (this.ProfileInfo.monthlyNoOfSalesGoal - 0), //Type Conversion using arthematic operation
        "monthlyBoundPremiumGoal": (this.ProfileInfo.monthlyBoundPremiumGoal - 0) //Type Conversion using arthematic operation
      };
      this._userprofile.updateProfile(reqobj, this.ProfileInfo.id).subscribe(function (data) {

      }, function (error) {
        console.log('error');
      })
    }else{
      this._userprofile.saveQuickLinks(this.QuickLinks).subscribe(function(resp){
        console.log(resp);
      },function(error){
        console.log('Error')
      });
    }
  }

  cancel(type) {
    if (type === 'info') {
      this.ProfileInfo = JSON.parse(JSON.stringify(this.OldData[type]));
    } else if (type === 'sales') {
      this.SalesInfo = JSON.parse(JSON.stringify(this.OldData[type]));
    } else if (type === 'links') {
      this.QuickLinks = JSON.parse(JSON.stringify(this.OldData[type]));
    }

    this.Edit[type] = false;
  }

  selectFile(event) {
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.ProfileInfo.picture = myReader.result
    };
    myReader.readAsDataURL(event.target.files[0]);
  }

  defaultPicture() {
    delete this.ProfileInfo['picture'];
  }
}
