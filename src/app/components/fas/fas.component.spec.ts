import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { FasComponent } from './fas.component';
import { WorkspaceComponent } from '../workspace/workspace.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { FollowupsComponent } from '../followups/followups.component';
import { LeadsComponent } from '../leads/leads.component';
import { MyLeadsComponent } from '../leads/my-leads/my-leads.component';
import { NewLeadsComponent } from '../leads/new-leads/new-leads.component';
import { UnassignedLeadsComponent } from '../leads/unassigned-leads/unassigned-leads.component';
import { TopControlComponent } from '../leads/top-control/top-control.component';
import { ViewSelectorComponent } from '../view-selector/view-selector.component';
import { RecentActivityComponent } from '../recent-activity/recent-activity.component';
import { ReportsComponent } from '../reports/reports.component';
import { LinksComponent } from '../links/links.component';
import { SearchComponent } from '../search/search.component';
import { ProfileComponent } from '../profile/profile.component';
import { ActivityHandler } from '../activityHandler/activityHandler.component';
import { SearchFormComponent } from '../search/search-form/search-form.component';

import { LeadAdapterClass } from '../../services/uiadaptors/leadsAdapter';
import { LeadsService } from '../../services/leads/leads.service';
import { LinksAdapterClass } from 'app/services/uiadaptors/linksAdapter';
import { FunctionalAreaSelector } from '../../models/functional-area-selector/functional-area-selector'

import { MyFolloupsPipe } from '../../pipes/util.pipe';
import { KeysPipe } from '../../pipes/util.pipe';
import { SortDirective } from '../../directives/sort.directive';
import { FollowuprowclassDirective } from '../../directives/followuprowclass.directive';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { BsDatepickerModule, TabsModule } from 'ngx-bootstrap';
import { Router, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppService } from '../../services/app/app.service';
import { UtilService } from '../../services/util.service';

import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';

import { DatepickerModule } from 'angular2-material-datepicker'

describe('FasComponent', () => {
  let component: FasComponent;
  let fixture: ComponentFixture<FasComponent>;
  let functionalAreaSelector: FunctionalAreaSelector;
  let appService: AppService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        FasComponent,
        WorkspaceComponent,
        DashboardComponent,
        FollowupsComponent,
        LeadsComponent,
        MyLeadsComponent,
        NewLeadsComponent,
        UnassignedLeadsComponent,
        TopControlComponent,
        ViewSelectorComponent,
        SortDirective,
        RecentActivityComponent,
        ReportsComponent,
        LinksComponent,
        SearchComponent,
        ProfileComponent,
        MyFolloupsPipe,
        KeysPipe,
        FollowuprowclassDirective,
        ActivityHandler,
        SearchFormComponent
       ],
       imports: [
         FormsModule,
         MalihuScrollbarModule.forRoot(),
         BsDatepickerModule.forRoot(),
         TabsModule.forRoot(),
         HttpModule,
         DatepickerModule
       ],
       providers: [
         LeadAdapterClass,
         LeadsService,
         LinksAdapterClass,
         { 
          provide: Router, 
          useClass: class { navigate = jasmine.createSpy("navigate"); }
         },
         FunctionalAreaSelector,
         AppService,
         UtilService,
         AppUserPrinciple
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FasComponent);
    component = fixture.componentInstance;
    // functionalAreaSelector = fixture.debugElement.injector.get(FunctionalAreaSelector);
    // appService = fixture.debugElement.injector.get(AppService);
    // appService.getDefaultViewService().subscribe(x => component.defaultView = x);

    // component.defaultView = functionalAreaSelector.getDefaultView();
    // component.activeIndex = component.defaultView.key;

    // component.defaultView = {};
    // component.defaultView.key = 0;

    // component.defaultView = {
    //   "key": 0
    // };

    // component.defaultView = {
    //   "key": 0,
    //   "app_logo": "assets/images/ais.png"
    // };

    fixture.detectChanges();
  });

  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });

  // it('should be created', async(() => {
  //   appService = fixture.debugElement.injector.get(AppService);
  //   appService.getDefaultViewService().subscribe(x => component.defaultView = x);

  //   fixture.whenStable().then(() => {
  //     expect(component).toBeTruthy();
  //   });
  // }));

  // menuClick
  // menuClick(ev, item) {
  //   if (/Recent Activity/ig.test(ev.label)) {
  //     this.recent = !this.recent;
  //   } else {
  //     this.activeIndex = item;
  //     this.selectedItem = item;
  //     this.updateView();
  //   }
  // }
  it('should show Dashboard', () => {
    spyOn(component, 'menuClick').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#fas-menu-1'));
    // btn.triggerEventHandler('click', null);
    let btnEl = btn.nativeElement;
    btnEl.click();

    fixture.detectChanges();
    
    expect(component.menuClick).toHaveBeenCalledWith(1, 1);
  });

  // getList (not in html, called by ngOnInit)
  // public getList() {
  //   this.groups = this._functionalAreaSelector.getFunctionalAreas();
  // }

  // getLabelForKey (not in html)
  // public getLabelForKey(selectedKey: string) {
  //   return _.where(this.menus, {Key: selectedKey});
  // }

  // getSubFunctionForKey (not in html)
  // public getSubFunctionForKey() {
  // }

  // toggleMenu (not in html)
  // toggleMenu() {
  //   // 1-line if statement that toggles the value:
  //   this.menuState = this.menuState === 'out' ? 'in' : 'out';
  // }

  // initRecentClicks (not in html, called by ngOnInit)
  // initRecentClicks() {
  //   this.RecentClicks = this.renderer.listen('document', 'click', (evt) => {
  //     this.recentEvent = evt;
  //     let found = false;
  //     for (let i = 0; i < evt.path.length; i++) {
  //       if (evt.path[i].classList && evt.path[i].classList.length > 0 && (evt.path[i].classList.contains('recent-activity') || evt.path[i].classList.contains('Recent'))) {
  //         found = true;
  //       }
  //     }

  //     if (!found) {
  //       this.recent = true;
  //     }
  //   })
  // }

  // updateView (not in html, called by menuClick)
  // updateView(){
  //   this.ref.detectChanges();
  // }

});
