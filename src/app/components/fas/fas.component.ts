import { animate, Component, OnDestroy, OnInit, Renderer2, state, style, ChangeDetectorRef, transition, trigger, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'underscore';

import { FunctionalAreaSelector } from '../../models/functional-area-selector/functional-area-selector';
import { LeadAdapterClass } from "../../services/uiadaptors/leadsAdapter";

import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';

import {UtilService} from '../../services/util.service';


@Component({
  selector: 'app-fas',
  templateUrl: './fas.component.html',
  styleUrls: ['./fas.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})


export class FasComponent implements OnInit, OnDestroy {
  @ViewChild('fas') fas: ElementRef;
  public menus = [];
  public selectedMenu;

  public RecentClicks: any;
  public recentEvent: any;

  groups: any;
  public recent = true;
  public defaultView : any;
  public activeIndex: any;
  public lastLabel: any;
  public app_logo: any;
  selectedItem: number;
  activeContent: number;

  public menuState = 'out';


  constructor(private _router: Router,
    private ref: ChangeDetectorRef,
    private Util: UtilService,
    private _functionalAreaSelector: FunctionalAreaSelector,
    private _appUserPrinciple: AppUserPrinciple, private renderer: Renderer2, public leadAdapterClass: LeadAdapterClass) {
    let that = this;
    this.defaultView = this._functionalAreaSelector.defaultView;
    this.activeIndex = this.defaultView.key;
    this.lastLabel=this._appUserPrinciple.getProfile().appUserDisplayName;
    this.app_logo = this.defaultView.app_logo
    if (!isNaN(this.activeIndex)) {
      this.selectedItem = this.activeIndex;
    }

    // const newUrl = '/' + this.activeIndex;
    // console.log("new url:/" + newUrl);
    this.leadAdapterClass.getOnCall().subscribe(function (resp:any) {
      that.menuClick(that.groups[2], 2)
    })

    this.Util.getFasView().subscribe(function(resp:any){
      that.menuClick(that.groups[resp.index], resp.index);
    });
  };

  ngOnInit() {
    this.getList();
    // if (this._router.url === '/') {
    //   this.selectedItem = this.activeContent = this.activeIndex;
    // } else {
    //   this.selectedItem = this.activeContent = this.activeIndex = eval(this._router.url.replace('/', ''));
    // }
    this.initRecentClicks();

  }

  menuClick(ev, item) {
    if (/Recent Activity/ig.test(ev.label)) {
      this.recent = !this.recent;
    } else {
      this.activeIndex = item;
      this.selectedItem = item;
      this.updateView();
    }
  }

  public getList() {
    this.groups = this._functionalAreaSelector.getFunctionalAreas();
    this.groups[this.groups.length - 1].label = this.lastLabel;
  }

  public getLabelForKey(selectedKey: string) {
    return _.where(this.menus, { Key: selectedKey });
  }


  public getSubFunctionForKey() {
  }

  toggleMenu() {
    // 1-line if statement that toggles the value:
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  }

  ngOnDestroy() {
  }

  initRecentClicks() {
    this.RecentClicks = this.renderer.listen('document', 'click', (evt) => {
      this.recentEvent = evt;
      let found = false;
      for (let i = 0; i < evt.path.length; i++) {
        if (evt.path[i].classList && evt.path[i].classList.length > 0 && (evt.path[i].classList.contains('recent-activity') || evt.path[i].classList.contains('Recent'))) {
          found = true;
        }
      }

      if (!found) {
        this.recent = true;
      }
    })
  }

  updateView() {
    this.ref.detectChanges();
  }
}
