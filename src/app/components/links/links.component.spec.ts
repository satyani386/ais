import { async, fakeAsync, tick, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { LinksComponent } from './links.component';
import { LinksAdapterClass } from '../../services/uiadaptors/linksAdapter';
import { LinksService } from '../../services/links/links.service';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

import { Http, HttpModule } from '@angular/http';

import { Observable } from 'rxjs/Observable';

fdescribe('LinksComponent', () => {
  let component: LinksComponent;
  let fixture: ComponentFixture<LinksComponent>;
  let linksService: LinksService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinksComponent ],
      imports: [ 
        HttpModule,
        MalihuScrollbarModule.forRoot() 
      ],
      providers: [ 
        LinksAdapterClass,
        LinksService 
      ]
    })
    .compileComponents();

    linksService = getTestBed().get(LinksService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should get links from the links service', async(() => {
    linksService.getLinks().subscribe(
      (result) => {
        expect(result).toBeDefined();
      }
    );
  }));

  // it('should have all Personal Lines Carriers', (done) => { 
  //   fixture.detectChanges();
    
  //   let spy = spyOn(linksService, 'getLinks').and.returnValue(Observable.of({
  //     "name": "Internet Passwords Natl and CA",
  //     "URL": "http://ais-poliseekportal/restools/Internet%20Passwords%20Natl%20%20CA/Internet%20Passwords%20NATL%20and%20CA.xlsx"
  //   }));

  //   component.ngOnInit();

  //   spy.calls.mostRecent().returnValue.then(() => { 
  //     fixture.detectChanges();
      
  //     let link = fixture.debugElement.query(By.css('#personal-lines-carriers-a-1'));
  //     let linkEl = link.nativeElement;

  //     expect(linkEl.href).toEqual("http://ais-poliseekportal/restools/Internet%20Passwords%20Natl%20%20CA/Internet%20Passwords%20NATL%20and%20CA.xlsx");

  //     done(); 
  //   });
  // });

  // it('should have all Personal Lines Carriers', async(() => {
  //   fixture.detectChanges();

  //   spyOn(linksService, 'getLinks').and.returnValue(Observable.of({
  //     "name": "Internet Passwords Natl and CA",
  //     "URL": "http://ais-poliseekportal/restools/Internet%20Passwords%20Natl%20%20CA/Internet%20Passwords%20NATL%20and%20CA.xlsx"
  //   }));

  //   fixture.whenStable().then(() => {
  //     fixture.detectChanges();
      
  //     let link = fixture.debugElement.query(By.css('#personal-lines-carriers-a-1'));
  //     let linkEl = link.nativeElement;
  
  //     expect(linkEl.href).toEqual("http://ais-poliseekportal/restools/Internet%20Passwords%20Natl%20%20CA/Internet%20Passwords%20NATL%20and%20CA.xlsx");
  //     // expect(component.Links.length).toBeGreaterThan(0);
  //   });
  //   component.ngOnInit();
  // }));

  // it('should have all Personal Lines Carriers', fakeAsync(() => {
  //   fixture.detectChanges();

  //   spyOn(linksService, 'getLinks').and.returnValue(Observable.of({
  //     "name": "Internet Passwords Natl and CA",
  //     "URL": "http://ais-poliseekportal/restools/Internet%20Passwords%20Natl%20%20CA/Internet%20Passwords%20NATL%20and%20CA.xlsx"
  //   }));

  //   component.ngOnInit();

  //   tick();

  //   fixture.detectChanges();
      
  //   let link = fixture.debugElement.query(By.css('#personal-lines-carriers-a-1'));
  //   let linkEl = link.nativeElement;
  
  //   expect(linkEl.href).toEqual("http://ais-poliseekportal/restools/Internet%20Passwords%20Natl%20%20CA/Internet%20Passwords%20NATL%20and%20CA.xlsx");
  //   // expect(component.Links.length).toBeGreaterThan(0);
  // }));

});

