import { Component, OnInit } from '@angular/core';
import { LinksAdapterClass } from '../../services/uiadaptors/linksAdapter';


@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {
  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  scrollbarOptions2 = { axis: 'y', theme: 'dark-3' };
  public Links: any = [];

  constructor(private _linksAdapterClass: LinksAdapterClass) { 
 
  }

  ngOnInit() {
      this._linksAdapterClass.getlinks()
                         .subscribe(result => {
                           this.Links=result
                           console.log(result);
                         }),error => alert(error);
       console.log(this.Links)                  
  }

}
