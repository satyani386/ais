import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { SearchFormComponent } from './search-form.component';
import { SearchAdapterClass } from '../../../services/uiadaptors/searchAdapter';
import { SearchService } from '../../../services/search/search.service';

import { Http, HttpModule } from '@angular/http';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../../services/leads/leads.service';

fdescribe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;
  let searchAdapterClass: SearchAdapterClass;
  // let searchService: SearchService;  

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchFormComponent ],
      imports: [
        HttpModule,
        MalihuScrollbarModule.forRoot(),
      ],
      providers: [
        SearchService,
        LeadAdapterClass,
        LeadsService
      ]
    })
    .compileComponents();

    // Get injected service into the tests
    // searchService = getTestBed().get(SearchService);    
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    searchAdapterClass = fixture.debugElement.injector.get(SearchAdapterClass);
    spyOn(searchAdapterClass, 'getSearchResults').and.callThrough();

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // searchFormSubmit
  // async because Error: Cannot make XHRs from within a fake async test
  it('should submit search form', async(() => {
    spyOn(component, 'searchFormSubmit').and.callThrough();

    let btn = fixture.debugElement.query(By.css('#search-form-submit'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    // searchAdapterClass.getSearchResults(searchFormData).subscribe(
    //   (result) => {
    //     expect(result).toBeDefined();
    //   }
    // );

    fixture.whenStable().then(() => {
      expect(component.searchFormSubmit).toHaveBeenCalled();
      expect(searchAdapterClass.getSearchResults).toHaveBeenCalled();
    });
  }));

  // showLead
  it('should show lead', () => {
    spyOn(component, 'showLead').and.callThrough();

    let btn = fixture.debugElement.query(By.css('#search-form-show-lead-1'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.showLead).toHaveBeenCalled();
  });

  // toggleFilter
  it('should toggle Product filter in Lead Results', () => {
    spyOn(component, 'toggleFilter').and.callThrough();

    let oldData = component.FilterDrop['leadProd'];

    let btn = fixture.debugElement.query(By.css('#toggle-lead-product'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    // https://github.com/angular/angular-cli/issues/4848
    /* "Actually it is pretty quick if you test pure javascript functionality, 
    but if you have lot of big TestBed-s with lot of DebugElement.query-s, 
    then it becomes somehow slow. I think asynchronism plays a role in performance, as well." */
    // let ok = fixture.debugElement.query(By.css('#toggle-lead-product-ok'));

    expect(component.toggleFilter).toHaveBeenCalled();
    expect(component.FilterDrop['leadProd']).toEqual(!oldData);
    // expect(ok).toBeTruthy();
  });

  it('should toggle Product filter in Insured Results', () => {
    spyOn(component, 'toggleFilter').and.callThrough();

    let oldData = component.FilterDrop['insureProd'];

    let btn = fixture.debugElement.query(By.css('#toggle-insured-product'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    // let ok = fixture.debugElement.query(By.css('#toggle-insured-product-ok'));

    expect(component.toggleFilter).toHaveBeenCalled();
    expect(component.FilterDrop['insureProd']).toEqual(!oldData);
    // expect(ok).toBeTruthy();
  });

  // searchFormReset (not in html)

  // Switch between Leads and Insured search results (not in ts)
  it('should show Leads search results', () => {
    let btn = fixture.debugElement.query(By.css('#show-leads-result-table'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    let leadTable = fixture.debugElement.query(By.css('#lead-results-table'));
    let leadTableEl = leadTable.nativeElement;

    let insuredTable = fixture.debugElement.query(By.css('#insured-results-table'));
    let insuredTableEl = insuredTable.nativeElement;

    expect(leadTableEl.hasAttribute('hidden')).toEqual(false);
    expect(insuredTableEl.hasAttribute('hidden')).toEqual(true);
  });

  it('should show Insured search results', () => {
    let btn = fixture.debugElement.query(By.css('#show-insured-results-table'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    let leadTable = fixture.debugElement.query(By.css('#lead-results-table'));
    let leadTableEl = leadTable.nativeElement;

    let insuredTable = fixture.debugElement.query(By.css('#insured-results-table'));
    let insuredTableEl = insuredTable.nativeElement;

    expect(leadTableEl.hasAttribute('hidden')).toEqual(true);
    expect(insuredTableEl.hasAttribute('hidden')).toEqual(false);
  });

});
