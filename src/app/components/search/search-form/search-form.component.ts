import { Component, OnInit } from '@angular/core';
import { SearchAdapterClass } from '../../../services/uiadaptors/searchAdapter';
import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { UserleadComponent } from "../../leads/userlead/userlead.component"

import {UtilService} from "../../../services/util.service";

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  providers: [SearchAdapterClass]
})
export class SearchFormComponent implements OnInit {

  public resultTable = 'leads';
  public FilterDrop = {
    insureProd: false,
    insureState: false,
    insureStatus: false,
    insureCompany: false,
    leadProd: false,
    leadState: false,
    leadcompany: false
  };
  public insureFilterObj: any = {};
  public States = [
    { name: 'CA' },
    { name: 'AZ' },
    { name: 'MD' },
    { name: 'TX' },
    { name: 'NJ' },
    { name: 'NY' },
    { name: 'IL' },
    { name: 'GA' }
  ];
  public LeadResults: any = [];
  public InsuredResults: any = [];
  public searchForm = {};
  records = 0;
  fetchedData: any = [];
  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  constructor(private _searchAdapter: SearchAdapterClass, private LeadAdapterClass: LeadAdapterClass, private Util: UtilService) { }

  ngOnInit() {
  }
  reset() {
    this.searchForm = {};
    this.InsuredResults = [];
    this.LeadResults = [];
  }
  searchFormSubmit() {
    const that = this;
    let obj = {
      "PageSize": 1000
    };
    Object.keys(this.searchForm).forEach(function (key) {
      if (that.searchForm[key] && that.searchForm[key].trim().length > 0) {
        obj[key] = that.searchForm[key];
      }
    })

    this._searchAdapter.getSearchResults(obj)
      .subscribe((resp) => {
        that.LeadResults = resp;
        that.LeadResults.Insured = that.Util.sortDefault(resp.Insured , 'Name');
        console.log(that.LeadResults);
      });
    this._searchAdapter.getInsuredSearchResults(obj)
      .subscribe((resp) => {
        that.InsuredResults = resp;
        console.log(that.InsuredResults);
      });
  }

  showLead(obj) {
    alert('will work with realtime data');
    /*obj['component'] = UserleadComponent;
    obj['type'] = 'my-leads';
    obj['isNew'] = true;
    obj['active'] = true;
    obj['removable'] = true;
    this.LeadAdapterClass.setOnCall(obj)*/
  }

  toggleFilter(type) {
    this.FilterDrop[type] = !this.FilterDrop[type];
  }

  applyFilter(event) {
    if(event.data.length === 0){
      delete this.insureFilterObj[event.type]
    }else{
      this.insureFilterObj[event.type] = event.data;
    }
  }
}