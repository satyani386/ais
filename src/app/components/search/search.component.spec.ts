import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { SearchService } from '../../services/search/search.service';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { Http, HttpModule } from '@angular/http';
import { LeadAdapterClass } from "../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../services/leads/leads.service';

fdescribe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [
        SearchComponent,
        SearchFormComponent
      ],
      imports: [
        MalihuScrollbarModule.forRoot(),
        HttpModule
      ],
      providers: [
        SearchService,
        LeadAdapterClass,
        LeadsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
