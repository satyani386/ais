import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferDataModalComponent } from './transfer-data-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

describe('TransferDataModalComponent', () => {
  let component: TransferDataModalComponent;
  let fixture: ComponentFixture<TransferDataModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferDataModalComponent ],
      providers: [
        BsModalRef,
        BsModalService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
