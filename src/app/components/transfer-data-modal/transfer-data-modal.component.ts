import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap"


@Component({
  selector: 'app-transfer-data-modal',
  templateUrl: './transfer-data-modal.component.html',
  styleUrls: ['./transfer-data-modal.component.scss']
})
export class TransferDataModalComponent implements OnInit {

  constructor(public modelRef: BsModalRef) { }

  ngOnInit() {
  }

}
