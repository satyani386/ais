import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  AfterViewInit,
  ViewContainerRef,
  ViewChild,
  Input,
  ComponentRef,
  Type,
  ComponentFactoryResolver,
  Compiler
} from '@angular/core';

@Component({
  selector: 'app-leads-ui-adaptor',
  templateUrl: './leads-ui-adaptor.component.html',
  styleUrls: ['./leads-ui-adaptor.component.scss']
})
export class LeadsUiAdaptorComponent implements OnDestroy, OnChanges, AfterViewInit {
  @ViewChild('target', {read: ViewContainerRef}) target: ViewContainerRef;
  @Input() type: Type<Component>;
  cmpRef: ComponentRef<Component>;
  private isViewInitialized: boolean = false;

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private compiler: Compiler) {

  }

  updateComponent() {
    if(!this.isViewInitialized) {
      return;
    }
    if(this.cmpRef) {
      this.cmpRef.destroy();
    }

    let factory = this.componentFactoryResolver.resolveComponentFactory(this.type);
    console.log(factory);
    this.cmpRef = this.target.createComponent(factory)
  }

  ngOnChanges() {
    console.log(this.type);
    this.updateComponent();
  }

  ngAfterViewInit() {
    this.isViewInitialized = true;
    this.updateComponent();
  }

  ngOnDestroy() {
    if(this.cmpRef) {
      this.cmpRef.destroy();
    }
  }
}
