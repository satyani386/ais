import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadsUiAdaptorComponent } from './leads-ui-adaptor.component';

describe('LeadsUiAdaptorComponent', () => {
  let component: LeadsUiAdaptorComponent;
  let fixture: ComponentFixture<LeadsUiAdaptorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadsUiAdaptorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadsUiAdaptorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
