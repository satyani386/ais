import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap"


@Component({
  selector: 'app-source-modal',
  templateUrl: './source-modal.component.html',
  styleUrls: ['./source-modal.component.scss']
})
export class SourceModalComponent implements OnInit {
  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  scrollbarOptions2 = { axis: 'y', theme: 'dark-3' };
  constructor(public modelRef: BsModalRef) { }

  ngOnInit() {
  }

}
