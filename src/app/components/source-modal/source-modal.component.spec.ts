import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceModalComponent } from './source-modal.component';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

describe('SourceModalComponent', () => {
  let component: SourceModalComponent;
  let fixture: ComponentFixture<SourceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceModalComponent ],
      imports: [
        MalihuScrollbarModule.forRoot()
      ],
      providers: [
        BsModalRef,
        BsModalService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
