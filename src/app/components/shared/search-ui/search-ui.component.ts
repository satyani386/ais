import { Component, OnInit, Output, EventEmitter,Input  } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as _ from 'underscore';
import { SharedService } from '../../../services/shared/shared.service';

@Component({
  selector: 'app-search-ui',
  templateUrl: './search-ui.component.html',
  styleUrls: ['./search-ui.component.scss']
})
export class SearchUiComponent implements OnInit {

  @Output() userSearched = new EventEmitter();
  @Output() userReset = new EventEmitter();

  _fields: Array<any> = [];
  @Input()
  set fields(fields: Array<any>) {
    this._fields = fields;
    this.setFields()
  }
  get fields() { return this._fields; }

  userForm: any;

  setFields(){
    let obj = {};
    _.each(this._fields,(field)=>{
      obj[field] = ['']
    })

    this.userForm = this.fb.group(obj);
  }

  resetFormGroup(){
    this.userForm.reset();
  }

  constructor(private fb: FormBuilder, private sharedService: SharedService) {
   }

  public search() {
    this.userSearched.emit(this.userForm.value);
  }

  public reset(){
    this.userReset.emit();
  }

  ngOnInit() {
    this.sharedService.isBooleanSource.subscribe((val: any) => {
        if(val){
          this.resetFormGroup();
        }
    });
  }

}
