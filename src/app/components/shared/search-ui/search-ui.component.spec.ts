import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchUiComponent } from './search-ui.component';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { SharedService } from '../../../services/shared/shared.service';

describe('SearchUiComponent', () => {
  let component: SearchUiComponent;
  let fixture: ComponentFixture<SearchUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchUiComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        SharedService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
