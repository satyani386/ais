import {Component, Input, OnChanges, OnInit, SimpleChanges, ChangeDetectorRef} from '@angular/core';
//import {LeadsService} from "../../services/leads/leads.service";
import { LeadAdapterClass } from '../../services/uiadaptors/leadsAdapter';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss']
})
export class WorkspaceComponent implements OnInit, OnChanges {
  public selectedItem: number;
  @Input() selectedIndex: any;
  @Input() recent: any;

  public newLead= {};
  public leadOnCall = {};

  constructor(public LeadAdapterClass: LeadAdapterClass, private cdRef:ChangeDetectorRef) {
    this.selectedItem = 0;
  }

  ngOnInit() {
    let that = this;
    this.LeadAdapterClass.getOnCall().subscribe((resp) => {
      console.log('On Call connect', resp);
      that.selectedItem = 2;
      that.newLead = resp;
      that.cdRef.detectChanges();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedIndex']) {
      this.selectedItem = this.selectedIndex;
    }
  }

  openNewLead(event){
    this.selectedIndex = 2;
    this.selectedItem = this.selectedIndex;
    if(!this.leadOnCall[event.RemotePartyNumber]){
      console.log(event);
      this.leadOnCall[event.RemotePartyNumber] = event;
      this.newLead = event;
    }
  }
}
