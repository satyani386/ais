import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges, ChangeDetectorRef } from '@angular/core';

import { WorkspaceComponent } from './workspace.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { FollowupsComponent } from '../followups/followups.component';
import { LeadsComponent } from '../leads/leads.component';
import { MyLeadsComponent } from '../leads/my-leads/my-leads.component';
import { UnassignedLeadsComponent } from '../leads/unassigned-leads/unassigned-leads.component';
import { RecentActivityComponent } from '../recent-activity/recent-activity.component';
import { ReportsComponent } from '../reports/reports.component';
import { LinksComponent } from '../links/links.component';
import { SearchComponent } from '../search/search.component';
import { SearchFormComponent } from '../search/search-form/search-form.component';
import { ProfileComponent } from '../profile/profile.component';
import { TopControlComponent } from '../leads/top-control/top-control.component';
import { ViewSelectorComponent } from '../view-selector/view-selector.component';
import { ActivityHandler } from '../activityHandler/activityHandler.component';

import { MyFolloupsPipe } from '../../pipes/util.pipe';
import { FollowuprowclassDirective } from '../../directives/followuprowclass.directive';
import { SortDirective } from '../../directives/sort.directive'; 

import { LeadAdapterClass } from '../../services/uiadaptors/leadsAdapter';
import { LeadsService } from '../../services/leads/leads.service';
import { LinksAdapterClass } from 'app/services/uiadaptors/linksAdapter';
import { LinksService } from '../../services/links/links.service';
import { SearchService } from '../../services/search/search.service';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { UtilService } from '../../services/util.service';
import { UserProfileService } from '../../services/user-profile/user-profile.service';
import { DashboardService } from '../../services/dashboard/dashboard.service';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule, TabsModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DaterangepickerConfig } from "ng2-daterangepicker"
// import { DatepickerModule } from 'ngx-bootstrap/datepicker/datepicker.module';
import { DatepickerModule } from 'angular2-material-datepicker'
import { Router, RouterModule } from '@angular/router';

import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('WorkspaceComponent', () => {
  let component: WorkspaceComponent;
  let fixture: ComponentFixture<WorkspaceComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        WorkspaceComponent,
        DashboardComponent,
        FollowupsComponent,
        LeadsComponent,
        MyLeadsComponent,
        UnassignedLeadsComponent,
        RecentActivityComponent,
        ReportsComponent,
        LinksComponent,
        SearchComponent,
        SearchFormComponent,
        ProfileComponent,
        TopControlComponent,
        ViewSelectorComponent,
        ActivityHandler,
        MyFolloupsPipe,
        FollowuprowclassDirective,
        SortDirective
      ],
      imports: [
        MalihuScrollbarModule.forRoot(),
        FormsModule,
        BsDatepickerModule.forRoot(),
        TabsModule.forRoot(),
        HttpModule,
        HttpClientModule,
        // DatepickerModule.forRoot()
        DatepickerModule
      ],
      providers: [
        LeadAdapterClass,
        LeadsService,
        LinksAdapterClass,
        LinksService,
        SearchService,
        AppUserPrinciple,
        BsModalRef,
        BsModalService,
        DaterangepickerConfig,
        { 
          provide: Router, 
          useClass: class { navigate = jasmine.createSpy("navigate"); }
        },
        UtilService,
        UserProfileService,
        DashboardService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // ngOnChanges
  // https://stackoverflow.com/questions/37408801/testing-ngonchanges-lifecycle-hook-in-angular-2
  it('should change selected index', () => {
    spyOn(component, 'ngOnChanges').and.callThrough();

    let prev_value = "old";
    let new_value = "new";
    let is_first_change: boolean = false;

    let changesObj: SimpleChanges = {
      selectedIndex: new SimpleChange(prev_value, new_value, is_first_change)
    };

    component.ngOnChanges(changesObj);

    fixture.detectChanges();

    expect(component.ngOnChanges).toHaveBeenCalledWith(changesObj);
    expect(component.selectedItem).toEqual(component.selectedIndex);
  });

  // openNewLead (not in html)
  // openNewLead(event){
  //   this.selectedIndex = 2;
  //   this.selectedItem = this.selectedIndex;
  //   if(!this.leadOnCall[event.RemotePartyNumber]){
  //     console.log(event);
  //     this.leadOnCall[event.RemotePartyNumber] = event;
  //     this.newLead = event;
  //   }
  // }

});
