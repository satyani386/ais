import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { UtilService } from '../../services/util.service';
import { UserProfileService } from '../../services/user-profile/user-profile.service';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { DashboardService } from '../../services/dashboard/dashboard.service';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

import { Http, HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  let _http: HttpClient;
  let userPrinciple: AppUserPrinciple;
  let UserPrinciple: AppUserPrinciple;
  let http: Http

  let Util: UtilService;
  let DashBoardService: DashboardService

  let userProfileService: UserProfileService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent
      ],
      imports: [
        HttpModule,
        HttpClientModule,
        MalihuScrollbarModule.forRoot()
      ],
      providers: [
        UtilService,
        UserProfileService,
        DashboardService,
        AppUserPrinciple
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    // fixture = TestBed.createComponent(DashboardComponent);
    // component = fixture.componentInstance;
    // fixture.detectChanges();

    // userProfileService = fixture.debugElement.injector.get(UserProfileService);

    // To fix: Uncaught TypeError: Cannot read property 'appSecurityRoleName' of undefined
    // component.UserPermissions = {};
    // component.UserPermissions.appSecurityRoleName = 'AIS - Commercial';
    // component.UserPermissions = {'appSecurityRoleName': 'AIS - Commercial'};
    // component.UserPermissions = [];
    // component.UserPermissions.push({'appSecurityRoleName': 'AIS - Commercial'});

    userProfileService = new UserProfileService(_http, userPrinciple, UserPrinciple, http);
    component = new DashboardComponent(Util, userProfileService, UserPrinciple, DashBoardService);

    spyOn(userProfileService, 'getPermission').and.returnValue({'appSecurityRoleName': 'AIS - Commercial'});
    spyOn(userPrinciple, 'getProfile').and.returnValue({});
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  let quotes = {
    'leftArrow': '#quote-tile .carousel-left-btn',
    'rightArrow': '#quote-tile .carousel-right-btn',
    'leftRadio': '#quote-tile .twisty-left',
    'middleRadio': '#quote-tile .twisty-middle',
    'rightRadio': '#quote-tile .twisty-right',
    'today': '#quote-today',
    'yesterday': '#quote-yesterday',
    'mtd':'#quote-mtd',
    'property': 'currentQuote'
  };

  let sales = {
    'leftArrow': '#sales-tile .carousel-left-btn',
    'rightArrow': '#sales-tile .carousel-right-btn',
    'leftRadio': '#sales-tile .twisty-left',
    'middleRadio': '#sales-tile .twisty-middle',
    'rightRadio': '#sales-tile .twisty-right',
    'today': '#sales-today',
    'yesterday': '#sales-yesterday',
    'mtd':'#sales-mtd',
    'property': 'currentSalesCard'
  };

  let bp = {
    'leftArrow': '#bp-tile .carousel-left-btn',
    'rightArrow': '#bp-tile .carousel-right-btn',
    'leftRadio': '#bp-tile .twisty-left',
    'middleRadio': '#bp-tile .twisty-middle',
    'rightRadio': '#bp-tile .twisty-right',
    'today': '#bp-today',
    'yesterday': '#bp-yesterday',
    'mtd':'#bp-mtd',
    'property': 'currentBPCard'
  };

  let callLog = {
    'leftArrow': '#call-log-tile .carousel-left-btn',
    'rightArrow': '#call-log-tile .carousel-right-btn',
    'leftRadio': '#call-log-tile .twisty-left',
    'middleRadio': '#call-log-tile .twisty-middle',
    'rightRadio': '#call-log-tile .twisty-right',
    'today': '#call-log-today',
    'yesterday': '#call-log-yesterday',
    'mtd':'#call-log-mtd',
    'property': 'currentCallLog'
  };

  let salesRanking = {
    'leftRadio': '#ranking-sales .twisty-left',
    'middleRadio': '#ranking-sales .twisty-middle',
    'rightRadio': '#ranking-sales .twisty-right',
    'today': '#ranking-sales-today',
    'mtd':'#ranking-sales-mtd',
    'property': 'currentSalesRanking'
  };

  let premiumRanking = {
    'leftRadio': '#ranking-premium .twisty-left',
    'middleRadio': '#ranking-premium .twisty-middle',
    'rightRadio': '#ranking-premium .twisty-right',
    'today': '#ranking-premium-today',
    'mtd':'#ranking-premium-mtd',
    'property': 'currentPremiumRanking'
  };

  function radioThreeButtons(obj) {
    let yesterday = fixture.debugElement.query(By.css(obj.yesterday));
    let yesterdayEl = yesterday.nativeElement;

    let today = fixture.debugElement.query(By.css(obj.today));
    let todayEl = today.nativeElement;

    let mtd = fixture.debugElement.query(By.css(obj.mtd));
    let mtdEl = mtd.nativeElement;

    // Today (default)
    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(false);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(2);

    // Yesterday (left)
    let left = fixture.debugElement.query(By.css(obj.leftRadio));
    left.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(false);
    expect(todayEl.hasAttribute('hidden')).toEqual(true);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(1);

    // Today (middle)
    let middle = fixture.debugElement.query(By.css(obj.middleRadio));
    middle.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(false);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(2);

    // MTD (right)
    let right= fixture.debugElement.query(By.css(obj.rightRadio));
    right.triggerEventHandler('click', null);    

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(true);
    expect(mtdEl.hasAttribute('hidden')).toEqual(false);
    expect(component[obj.property]).toEqual(3);
  }

  function radioTwoButtons(obj) {
    let today = fixture.debugElement.query(By.css(obj.today));
    let todayEl = today.nativeElement;

    let mtd = fixture.debugElement.query(By.css(obj.mtd));
    let mtdEl = mtd.nativeElement;

    // Today (default)
    expect(todayEl.hasAttribute('hidden')).toEqual(false);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(1);

    // Today (left)
    let left = fixture.debugElement.query(By.css(obj.leftRadio));
    left.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(todayEl.hasAttribute('hidden')).toEqual(false);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(1);

    // MTD (right)
    let right= fixture.debugElement.query(By.css(obj.rightRadio));
    right.triggerEventHandler('click', null);    

    fixture.detectChanges();

    expect(todayEl.hasAttribute('hidden')).toEqual(true);
    expect(mtdEl.hasAttribute('hidden')).toEqual(false);
    expect(component[obj.property]).toEqual(2);
  }

  function arrows(obj) {
    let yesterday = fixture.debugElement.query(By.css(obj.yesterday));
    let yesterdayEl = yesterday.nativeElement;

    let today = fixture.debugElement.query(By.css(obj.today));
    let todayEl = today.nativeElement;

    let mtd = fixture.debugElement.query(By.css(obj.mtd));
    let mtdEl = mtd.nativeElement;

    // Today (default)
    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(false);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(2);

    // Yesterday from Today (left)
    let left = fixture.debugElement.query(By.css(obj.leftArrow));
    left.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(false);
    expect(todayEl.hasAttribute('hidden')).toEqual(true);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(1);

    // MTD from Yesterday (left)
    left.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(true);
    expect(mtdEl.hasAttribute('hidden')).toEqual(false);
    expect(component[obj.property]).toEqual(3);

    // Today from MTD (left)
    left.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(false);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(2);

    // MTD from Today (right)
    let right= fixture.debugElement.query(By.css(obj.rightArrow));
    right.triggerEventHandler('click', null);    

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(true);
    expect(mtdEl.hasAttribute('hidden')).toEqual(false);
    expect(component[obj.property]).toEqual(3);

    // Yesterday from MTD (right)
    right.triggerEventHandler('click', null);    

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(false);
    expect(todayEl.hasAttribute('hidden')).toEqual(true);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(1);

    // Today from Yesterday (right)
    right.triggerEventHandler('click', null);    

    fixture.detectChanges();

    expect(yesterdayEl.hasAttribute('hidden')).toEqual(true);
    expect(todayEl.hasAttribute('hidden')).toEqual(false);
    expect(mtdEl.hasAttribute('hidden')).toEqual(true);
    expect(component[obj.property]).toEqual(2);
  }

  // twistyNavBtnClick
  it('should have working radio buttons for Quotes and show today by default', () => {
    radioThreeButtons(quotes);
  });

  it('should have working radio buttons for Unit Sales and show today by default', () => {
    radioThreeButtons(sales);
  });

  it('should have working radio buttons for Bound Premium and show today by default', () => {
    radioThreeButtons(bp);
  });

  it('should have working radio buttons for Call Log and show today by default', () => {
    radioThreeButtons(callLog);
  });

  it('should have working radio buttons for Unit Sales Rankings and show today by default', () => {
    radioTwoButtons(salesRanking);
  });

  it('should have working radio buttons for Premium Rankings and show today by default', () => {
    radioTwoButtons(premiumRanking);
  });

  // nav
  it('should have working arrows for Quotes and show today by default', () => {
    arrows(quotes);
  });

  it('should have working arrows for Unit Sales and show today by default', () => {
    arrows(sales);
  });

  it('should have working arrows for Bound Premium and show today by default', () => {
    arrows(bp);
  });

  it('should have working arrows for Call Log and show today by default', () => {
    arrows(callLog);
  });

  // Default is Unit Sales Goal
  it('should show Unit Sales Goal by default', () => {
    let salesGoal = fixture.debugElement.query(By.css('#sales-goal'));
    let salesEl = salesGoal.nativeElement;
    let boundPremiumGoal = fixture.debugElement.query(By.css('#bound-premium-goal'));
    let boundPremiumEl = boundPremiumGoal.nativeElement;

    expect(component.defaultProgress2Show).toEqual(0);
    expect(salesEl.hasAttribute('hidden')).toEqual(false);
    expect(boundPremiumEl.hasAttribute('hidden')).toEqual(true);
  });

  // Default is Unit Sales Rankings
  it('should show Unit Sales Rankings by default', () => {
    let btn = fixture.debugElement.query(By.css('#toggle-ranking-sales'));
    let sales = fixture.debugElement.query(By.css('#ranking-tile .sales'));
    let salesEl = sales.nativeElement;
    let premium = fixture.debugElement.query(By.css('#ranking-tile .premium'));
    let premiumEl = premium.nativeElement;

    expect(component.currentRankingFilter).toEqual(1);
    expect(salesEl.hasAttribute('hidden')).toEqual(false);
    expect(premiumEl.hasAttribute('hidden')).toEqual(true);
  });

  // toggleRanking
  it('should toggle rankings to Unit Sales', () => {
    let btn = fixture.debugElement.query(By.css('#toggle-ranking-sales'));
    let sales = fixture.debugElement.query(By.css('#ranking-tile .sales'));
    let salesEl = sales.nativeElement;
    let premium = fixture.debugElement.query(By.css('#ranking-tile .premium'));
    let premiumEl = premium.nativeElement;

    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.currentRankingFilter).toEqual(1);
    expect(salesEl.hasAttribute('hidden')).toEqual(false);
    expect(premiumEl.hasAttribute('hidden')).toEqual(true);
  });

  it('should toggle rankings to Premium', () => {
    let btn = fixture.debugElement.query(By.css('#toggle-ranking-premium'));
    let sales = fixture.debugElement.query(By.css('#ranking-tile .sales'));
    let salesEl = sales.nativeElement;
    let premium = fixture.debugElement.query(By.css('#ranking-tile .premium'));
    let premiumEl = premium.nativeElement;

    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.currentRankingFilter).toEqual(2);
    expect(salesEl.hasAttribute('hidden')).toEqual(true);
    expect(premiumEl.hasAttribute('hidden')).toEqual(false);
  });

  // toggleProgress
  it('should toggle goal bar to Unit Sales', () => {
    component.defaultProgress2Show = 'bound';

    let btn = fixture.debugElement.query(By.css('#toggle-progress'));
    btn.triggerEventHandler('click', null);

    let salesGoal = fixture.debugElement.query(By.css('#sales-goal'));
    let salesEl = salesGoal.nativeElement;
    let boundPremiumGoal = fixture.debugElement.query(By.css('#bound-premium-goal'));
    let boundPremiumEl = boundPremiumGoal.nativeElement;

    fixture.detectChanges();

    expect(component.defaultProgress2Show).toEqual('unit');
    expect(salesEl.hasAttribute('hidden')).toEqual(false);
    expect(boundPremiumEl.hasAttribute('hidden')).toEqual(true);
  });

  it('should toggle goal bar to Bound Premium', () => {
    component.defaultProgress2Show = 'unit';

    let btn = fixture.debugElement.query(By.css('#toggle-progress'));
    btn.triggerEventHandler('click', null);

    let salesGoal = fixture.debugElement.query(By.css('#sales-goal'));
    let salesEl = salesGoal.nativeElement;
    let boundPremiumGoal = fixture.debugElement.query(By.css('#bound-premium-goal'));
    let boundPremiumEl = boundPremiumGoal.nativeElement;

    fixture.detectChanges();

    expect(component.defaultProgress2Show).toEqual('bound');
    expect(salesEl.hasAttribute('hidden')).toEqual(true);
    expect(boundPremiumEl.hasAttribute('hidden')).toEqual(false);
  });

});
