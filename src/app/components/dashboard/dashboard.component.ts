import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util.service';
import { UserProfileService } from '../../services/user-profile/user-profile.service';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { DashboardService } from '../../services/dashboard/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  defaultProgress2Show: string = 'unit';
  currentQuote: number = 2;
  currentCallLog: number = 2;
  currentSalesBp: number = 2;
  currentSalesRanking: number = 1;
  currentPremiumRanking: number = 1;
  currentSalesCard: number = 2;
  currentBPCard: number = 2;
  currentRankingFilter: number = 1;
  currentRanking: number = 2;
  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  scrollbarOptions2 = { axis: 'y', theme: 'dark-3' };
  // followupDate: any = null;
  // followupDates: any = [];
  // days: any = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  // fullDays: any = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  // monthNames: any = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  // followupTitles: any = [];
  // numberOfDays: number = 5;
  // activeFollowupDate: number = 0;
  public Salesdata = [];
  public monthdata = [];
  public rankingInfo=[];
  public alertsInfo=[];
  public DashBoardInfo = {
    goals: {
      "totalSales": 0,
      "salesGoal": 0,
      "totalBoundPremiums": 0,
      "boundPremiumGoal": 0,
    },
    unitSales: {
      "today": 0,
      "yesterday": 0,
      "monthToDate": 0
    },
     Quotes: {
      "today": 0,
      "yesterday": 0,
      "monthToDate": 0
    }, boundPremium: {
      "today": 0,
      "yesterday": 0,
      "monthToDate": 0
    }, 
       callLog: {
   "callsReceivedToday": 0,
    "callsAnsweredToday": 0,
    "callsReceivedYesterday": 0,
    "callsAnsweredYesterday": 0,
    "callsReceivedMonthToDate": 0,
    "callsAnsweredMonthToDate": 0

    },
    unavailable:{
    "hoursToday": 0,
    "minutesToday": 0,
    // "hoursYesterday": 0,
    // "minutesYesterday": 0,
    // "hoursMonthToDate": 0,
    // "minutesMonthToDate": 0
}

    
  }

  public UserPermissions: any = {};
  public UserProfile: any = {};

  constructor(private Util: UtilService, private UserProfileService: UserProfileService, private UserPrinciple: AppUserPrinciple, private DashBoardService: DashboardService) {
    const that = this;
    this.UserPermissions = this.UserProfileService.UserPermissionData;
    this.UserProfile = this.UserPrinciple.getProfile();

    this.DashBoardService.getSalesGoals().subscribe(function (resp) {
      that.DashBoardInfo.goals = resp;
    }, function (error) {
      console.log(error);
    });

    this.DashBoardService.getUnitSales().subscribe(function (resp) {
      that.DashBoardInfo.unitSales = resp;
    }, function (error) {

    });
       this.DashBoardService.getQuotes().subscribe(function (resp) {
      that.DashBoardInfo.Quotes = resp;
    }, function (error) {

    });
       this.DashBoardService.getBoundPremium().subscribe(function (resp) {
      that.DashBoardInfo.boundPremium = resp;
    }, function (error) {

    });
       this.DashBoardService.getRanking().subscribe(function (resp) {
      that.rankingInfo = resp;
    }, function (error) {

    });
       this.DashBoardService.getAlerts().subscribe(function (resp) {
      that.alertsInfo = resp;
    }, function (error) {

    });
       this.DashBoardService.getMyCallLog().subscribe(function (resp) {
      that.DashBoardInfo.callLog = resp;
    }, function (error) {

    });
       this.DashBoardService.getUnavailable().subscribe(function (resp) {
      that.DashBoardInfo.unavailable = resp;
    }, function (error) {

    });


    //Change as per requirement
    this.defaultProgress2Show = (this.UserPermissions.appSecurityRoleName === "AIS - Commercial") ? 'bound' : 'unit';
    this.currentRankingFilter = (this.UserPermissions.appSecurityRoleName === "AIS - Commercial") ? 2 : 1;


    this.Salesdata = [
      { 'name': 'Mike Smith', 'rank': '1', 'sales': '25' },
      { 'name': 'John Smith', 'rank': '2', 'sales': '22' },
      { 'name': 'Tim Sherman', 'rank': '3', 'sales': '20' },
      { 'name': 'Eric Welgat', 'rank': '4', 'sales': '19' },
      { 'name': 'Rahul Datir', 'rank': '5', 'sales': '18' },
      { 'name': 'Demond Jordon', 'rank': '6', 'sales': '12' },
      { 'name': 'Sai Charan', 'rank': '7', 'sales': '12' },
      { 'name': 'Mahyar babaie', 'rank': '8', 'sales': '10' },
      { 'name': 'louis Soh', 'rank': '9', 'sales': '5' },
      { 'name': 'Nancy Chan', 'rank': '10', 'sales': '4' },
      { 'name': 'Samantha', 'rank': '11', 'sales': '3' },
      { 'name': 'Ron', 'rank': '12', 'sales': '3' },
      { 'name': 'John', 'rank': '13', 'sales': '3' },
      { 'name': 'David Blane', 'rank': '14', 'sales': '3' },
      { 'name': 'Steve Smith', 'rank': '15', 'sales': '3' },
      { 'name': 'Samantha', 'rank': '16', 'sales': '3' },
      { 'name': 'Ron', 'rank': '17', 'sales': '3' },
      { 'name': 'John', 'rank': '18', 'sales': '3' },
      { 'name': 'David Blane', 'rank': '19', 'sales': '3' },
      { 'name': 'Steve Smith', 'rank': '20', 'sales': '3' },
      { 'name': 'Samantha', 'rank': '21', 'sales': '3' },
      { 'name': 'Ron', 'rank': '22', 'sales': '3' },
      { 'name': 'John', 'rank': '23', 'sales': '3' },
      { 'name': 'David Blane', 'rank': '24', 'sales': '3' },
      { 'name': 'Steve Smith', 'rank': '25', 'sales': '3' },

    ]
    this.monthdata = [
      { 'name': 'Mike Smith', 'rank': '1', 'sales': '200' },
      { 'name': 'John Smith', 'rank': '2', 'sales': '198' },
      { 'name': 'Tim Sherman', 'rank': '3', 'sales': '150' },
      { 'name': 'Eric Welgat', 'rank': '4', 'sales': '145' },
      { 'name': 'Rahul Datir', 'rank': '5', 'sales': '140' },
      { 'name': 'Demond Jordon', 'rank': '6', 'sales': '138' },
      { 'name': 'Sai Charan', 'rank': '7', 'sales': '130' },
      { 'name': 'Mahyar babaie', 'rank': '8', 'sales': '125' },
      { 'name': 'louis Soh', 'rank': '9', 'sales': '118' },
      { 'name': 'Nancy Chan', 'rank': '10', 'sales': '115' },
      { 'name': 'Samantha', 'rank': '11', 'sales': '113' },
      { 'name': 'Ron', 'rank': '12', 'sales': '110' },
      { 'name': 'John', 'rank': '13', 'sales': '109' },
      { 'name': 'David Blane', 'rank': '14', 'sales': '103' },
      { 'name': 'Steve Smith', 'rank': '15', 'sales': '103' },
      { 'name': 'Samantha', 'rank': '16', 'sales': '113' },
      { 'name': 'Ron', 'rank': '17', 'sales': '110' },
      { 'name': 'John', 'rank': '18', 'sales': '109' },
      { 'name': 'David Blane', 'rank': '19', 'sales': '103' },
      { 'name': 'Steve Smith', 'rank': '20', 'sales': '103' },
      { 'name': 'Samantha', 'rank': '21', 'sales': '113' },
      { 'name': 'Ron', 'rank': '22', 'sales': '110' },
      { 'name': 'John', 'rank': '23', 'sales': '109' },
      { 'name': 'David Blane', 'rank': '24', 'sales': '103' },
      { 'name': 'Steve Smith', 'rank': '25', 'sales': '103' },

    ]
    //   this.followupDate = new Date();
    //   for (let i: number = 0; i < this.numberOfDays; i++) {
    //     this.followupDates[i] = new Date();
    //     this.followupDates[i].setDate(this.followupDate.getDate() + i);
    //     let fullDateString = this.fullDays[this.followupDates[i].getDay()] + ' ' + this.monthNames[this.followupDates[i].getMonth()] + ' ' + this.stringDate(this.followupDates[i].getDate());
    //     this.followupTitles.push({ 'day': this.days[this.followupDates[i].getDay()], 'date': this.followupDates[i].getDate(), 'fullDate': fullDateString, 'isToday': i });
    //   }
    //   this.activeFollowupDate = this.followupTitles[0].date;
    // }
    // changeActiveFollowupDate(date) {
    //   this.activeFollowupDate = date;
    // }
    // stringDate(dateNumber) {
    //   if (dateNumber % 10 == 1) return dateNumber + 'st';
    //   if (dateNumber % 10 == 2) return dateNumber + 'nd';
    //   if (dateNumber % 10 == 3) return dateNumber + 'rd';
    //   else return dateNumber + 'th';
  }
  ngOnInit() {
  }

  getBoundProgressValue() {
    if (this.DashBoardInfo.goals) {
      return 150;
      //Math.round((this.DashBoardInfo.goals.totalBoundPremiums / this.DashBoardInfo.goals.boundPremiumGoal) * 100);
    }
  }
  getSalesProgressValue() {
    if (this.DashBoardInfo.goals) {
      return Math.round((this.DashBoardInfo.goals.totalSales / this.DashBoardInfo.goals.salesGoal) * 100);
    }
  }

  OpenReports() {
    this.Util.setFasView({ index: 4 })
  }

  twistyNavBtnClick(element, index) {
    //alert(element +','+ index);
    if (element == 'quote') this.currentQuote = index;
    if (element == 'sales') this.currentSalesCard = index;
    if (element == 'bp') this.currentBPCard = index;
    if (element == 'myCallLog') this.currentCallLog = index;

    if (element == 'salesRanking') this.currentSalesRanking = index;
    if (element == 'premiumRanking') this.currentPremiumRanking = index;
  }
  nav(element, direction) {
    if (element == 'quote') {
      if (direction == 'pre') {
        if (this.currentQuote === 1) this.currentQuote = 3;
        else this.currentQuote--;
      } else {
        if (this.currentQuote === 3) this.currentQuote = 1;
        else this.currentQuote++;
      }
    }
    if (element == 'myCallLog') {
      if (direction == 'pre') {
        if (this.currentCallLog === 1) this.currentCallLog = 3;
        else this.currentCallLog--;
      } else {
        if (this.currentCallLog === 3) this.currentCallLog = 1;
        else this.currentCallLog++;
      }
    }
    if (element == 'sales') {
      if (direction == 'pre') {
        if (this.currentSalesCard === 1) this.currentSalesCard = 3;
        else this.currentSalesCard--;
      } else {
        if (this.currentSalesCard === 3) this.currentSalesCard = 1;
        else this.currentSalesCard++;
      }
    }
    if (element == 'bp') {
      if (direction == 'pre') {
        if (this.currentBPCard === 1) this.currentBPCard = 3;
        else this.currentBPCard--;
      } else {
        if (this.currentBPCard === 3) this.currentBPCard = 1;
        else this.currentBPCard++;
      }
    }

    // if (element == 'salesRanking') {

    //     if (this.currentRanking === 1) this.currentRanking = 2;
    //     else this.currentRanking--;
    //    if (this.currentRanking === 2) this.currentRanking = 1;
    //    else this.currentRanking++;

    // }
    // if (element == 'premiumRanking') {
    //   if (direction == 'pre') {
    //     if (this.currentPremiumRanking === 1) this.currentPremiumRanking = 2;
    //     else this.currentPremiumRanking--;
    //   } else {
    //     if (this.currentPremiumRanking === 2) this.currentPremiumRanking = 1;
    //     else this.currentPremiumRanking++;
    //   }
    // }
  }
  // togleSalesBP(currentCardSubView) {
  //   this.currentSalesBp = currentCardSubView;
  // }
  toggleRanking(currentCardSubView) {
    this.currentRankingFilter = currentCardSubView;
  }
  toggleProgress() {
    this.defaultProgress2Show = (this.defaultProgress2Show == 'unit') ? 'bound' : 'unit';
  }
}