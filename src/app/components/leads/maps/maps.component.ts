import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap"

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {

  constructor(public modelRef: BsModalRef) { }

  ngOnInit() {
  }

}
