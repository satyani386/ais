
import { Component, OnInit,Input,OnChanges, SimpleChanges } from '@angular/core';
@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss']
})
export class LeadsComponent implements OnInit, OnChanges {
  @Input() selectedTab: any;
  @Input() newLeadOnCall: any;

  public currentComponent = 'my-leads';
  tabs = [];
  constructor() {
    this.tabs = [];
    this.showView('my-leads');
  }

  ngOnChanges(changes:SimpleChanges){
    if(changes['selectedTab']){
    }else if(changes['newLeadOnCall']){
      if(changes['newLeadOnCall'] && Object.keys(changes['newLeadOnCall'].currentValue).length > 0){
        this.showView('new-lead');
      }
    }
    // }if(changes['newLeadOnCall']){
    //   this.showView('new-lead')
    // }
  }
  // ngDoCheck(){
  //   if(this.newLeadOnCall){
  //     this.showView('new-lead')
  //   }
  // }

  showView(currentView: string){
    this.currentComponent = currentView;
  }
  ngOnInit() {}
}
