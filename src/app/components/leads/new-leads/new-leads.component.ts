import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { IngeniusAdapterClass } from "../../../services/uiadaptors/IngeniusAdapter";
import { AbstarctAdapterClass } from "../../../services/uiadaptors/AbstractAdaptor";
import { HideShowService } from '../../../services/HideShow/Hide-Show.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserleadComponent } from "../userlead/userlead.component"
import { SourceModalComponent } from "../../source-modal/source-modal.component"
import { MapsComponent } from "../maps/maps.component"
import { TransferDataModalComponent } from "../../transfer-data-modal/transfer-data-modal.component"
import * as $ from 'jquery';

@Component({
  selector: 'app-new-leads',
  templateUrl: './new-leads.component.html',
  styleUrls: ['./new-leads.component.scss'],
  providers: [HideShowService]
})
export class NewLeadsComponent implements OnInit, OnChanges {
  @Input() currentTab: any;
  bsModalRef: BsModalRef;
  public ValidValues: any;
  public PhoneData: any = [];
  public state: string;
  public parkedAt: string;
  public stateHideShow: any;
  public content: any;
  public stateflow = 'creation';
  public Data = [];

  public EditLeftForm = false;


  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  scrollbarOptions2 = { axis: 'y', theme: 'dark-3' };

  constructor(private AbstarctAdapterClass: AbstarctAdapterClass, private modalService: BsModalService,
    private HideShowService: HideShowService,
    private LeadAdapterClass: LeadAdapterClass,
    private ref: ChangeDetectorRef,
    private IngeniusAdapterClass: IngeniusAdapterClass) {
    this.Data = [
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },

    ]
    const that = this;
    this.content = this.LeadAdapterClass.getOnCallData();

    // To fix: TypeError: Cannot read property 'email' of undefined
    //this.content = [];
    //this.content.email = [];
    //this.content.email.push('Test@example.com');

    this.ValidValues = {
      state: []
    };
    this.stateHideShow = [];
    this.AbstarctAdapterClass.getValidValues('NY').subscribe(function (resp) {
      that.ValidValues = resp;
      that.updateView();
    }, function (error) {

    });

    this.IngeniusAdapterClass.getOnCallResults().subscribe((data) => {
      that.PhoneData = data.data;
      console.log(that.PhoneData);
      that.updateView();

    })
  }

  /*Left Panel Click Events*/
  editLeftForm(event) {
    this.EditLeftForm = !this.EditLeftForm;
  }

  sourceData() {
    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(SourceModalComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }

  TransferData() {
    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(TransferDataModalComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }

  maps(event) {
    event.target.classList.add('active')

    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(MapsComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }

  ngOnInit() {
    this.LeadAdapterClass.getOnCall().subscribe((resp) => {
      console.log('On Call connect', resp)
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['currentTab']) {
      this.content = this.currentTab;
    }
  }

  addPhone() {
    if (this.content.phone.length < 5) {
      this.content.phone.push({ type: 'home' })
    }
  }

  addProduct() {
    if (this.content.product.length < 5) {
      this.content.product.push({ type: 'Auto' })
    }
  }

  addStar(obj) {
    obj.star = !obj.star;
  }

  nextState() {
    const x = ['creation', 'policy', 'drive', 'vehicle', 'coverage', 'carrier', 'quote', 'rates'];
    if (x.indexOf(this.stateflow) !== (x.length - 1)) {
      const newIndex = x.indexOf(this.stateflow) + 1;
      const action = x[newIndex];
      $('.newleadsnext').find('a.active').closest('li').next().find('a').addClass('active');
      this.selectType(false, x[newIndex]);
    }
  }

  phoneTypeChange(event, obj) {
    obj.type = event.target.value;
  }

  removePhone(i) {
    this.content.phone.splice(i, 1);
  }
  removeEmail(i) {
    this.content.email.splice(i, 1);
  }
  removeProduct(i) {
    this.content.product.splice(i, 1);
  }

  addEmail() {
    if (this.content.email.length < 3) {
      this.content.email.push('')
    }
  }

  selectState(data) {
    this.stateHideShow = [];
    const that = this;
    if (data.trim().length === 0) {
      return false;
    }
    this.HideShowService.getStateHideShow(data).subscribe(function (resp) {
      that.stateHideShow = resp;
    }, function (err) {
      console.log(err)
    })
  }

  makeCall(phone) {
    const ctiApi = this.IngeniusAdapterClass.getConnectionObj();
    ctiApi.Dial(phone).then(function (data) {
      console.log(data);
    })
  }

  updateView() {
    this.ref.detectChanges();
  }

  selectType(event, type) {
    this.stateflow = type;
    if (event && event.target.parentNode.nodeName === 'A') {
      event.target.parentNode.classList.add('active');
    }
  }

  showLead(obj) {
    console.log('Open Lead');
    obj['component'] = UserleadComponent;
    obj['type'] = 'my-leads';
    obj['isNew'] = true;
    obj['active'] = true;
    obj['removable'] = true;
    console.log(obj);
    this.LeadAdapterClass.setOnCall(obj)
  }
}
