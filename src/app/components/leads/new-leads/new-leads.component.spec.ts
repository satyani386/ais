import { async, fakeAsync, tick, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { Component, DebugElement, getDebugNode } from "@angular/core";
import { NewLeadsComponent } from './new-leads.component';
import { SourceModalComponent } from "../../source-modal/source-modal.component";
import { MapsComponent } from "../maps/maps.component";
import { TransferDataModalComponent } from "../../transfer-data-modal/transfer-data-modal.component";
import { UserleadComponent } from "../userlead/userlead.component";

import { HttpModule } from '@angular/http';
import { AbstarctAdapterClass } from "../../../services/uiadaptors/AbstractAdaptor";
import { ValidvaluesService } from "../../../services/validvalues/validvalues.service";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerModule, ModalModule, PaginationModule, TabsModule } from 'ngx-bootstrap';
import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../../services/leads/leads.service';
import { IngeniusAdapterClass } from "../../../services/uiadaptors/IngeniusAdapter";
import { IngeniusService } from "../../../services/ingenius/ingenius.service";
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { SearchComponent } from 'app/components/search/search.component';

fdescribe('NewLeadsComponent', () => {
  let component: NewLeadsComponent;
  let fixture: ComponentFixture<NewLeadsComponent>;
  let bsModalService: BsModalService;
  let bsModalRef: BsModalRef;
  let leadAdapterClass: LeadAdapterClass;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        NewLeadsComponent, 
        SourceModalComponent,
        MapsComponent,
        TransferDataModalComponent,
        UserleadComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        HttpModule,
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        MalihuScrollbarModule.forRoot()
      ],
      providers: [
        AbstarctAdapterClass,
        ValidvaluesService,
        BsModalRef,
        BsModalService,
        LeadAdapterClass,
        LeadsService,
        IngeniusAdapterClass,
        IngeniusService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewLeadsComponent);
    component = fixture.componentInstance;
    bsModalService = fixture.debugElement.injector.get(BsModalService);
    bsModalRef = fixture.debugElement.injector.get(BsModalRef);
    leadAdapterClass = fixture.debugElement.injector.get(LeadAdapterClass);
    spyOn(bsModalService, 'show').and.callThrough();
    spyOn(bsModalRef, 'hide').and.callThrough();
    spyOn(leadAdapterClass, 'setOnCall').and.callThrough();

    // To fix: Uncaught TypeError: Cannot read property 'email' of undefined
    component.content = [];
    component.content.email = [];
    component.content.email.push('Test@example.com');

    component.content.phone = [];
    component.content.phone.push('123-456-7890');

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // editLeftForm
  // it('should enable edit left form', () => {
  //   spyOn(component, 'editLeftForm').and.callThrough();
  //   // Change to NOT 'creation' so edit appears
  //   let btn = fixture.debugElement.query(By.css('#next-state'));
  //   btn.triggerEventHandler('click', null);

  //   fixture.detectChanges();

  //   let edit = fixture.debugElement.query(By.css('#edit-left-form'));
  //   edit.triggerEventHandler('click', null);

  //   fixture.detectChanges();
    
  //   expect(component.editLeftForm).toHaveBeenCalled();
  //   expect(component.EditLeftForm).toEqual(true);
  // });

  // sourceData
  it('should show Source modal', fakeAsync(() => {
    spyOn(component, 'sourceData').and.callThrough();

    let list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];

    // Open modal
    let btn = fixture.debugElement.query(By.css('#source-data'));
    btn.triggerEventHandler('click', null);

    tick();
    fixture.detectChanges();

    // sourceData function
    expect(component.sourceData).toHaveBeenCalled();
    expect(bsModalService.show).toHaveBeenCalledWith(SourceModalComponent);
    expect(bsModalRef).toBeTruthy();
    // expect(bsModalRef.content.title).toEqual('Modal with component');
    // expect(bsModalRef.content.list).toEqual(list);

    // Modal displayed
    // let modal = fixture.debugElement.query(By.css('app-source-modal'));
    // expect(modal).toBeTruthy();

    // Close modal
    // let close = fixture.debugElement.query(By.css('#close-source-modal'));
    // close.triggerEventHandler('click', null);

    // tick();
    // fixture.detectChanges();

    // // Modal closed
    // expect(modal).not.toBeTruthy();
  }));

  // TransferData
  it('should show Transferable Data modal', fakeAsync(() => {
    spyOn(component, 'TransferData').and.callThrough();

    let btn = fixture.debugElement.query(By.css('#transfer-data'));
    btn.triggerEventHandler('click', null);

    tick();
    fixture.detectChanges();

    expect(component.TransferData).toHaveBeenCalled();
    expect(bsModalService.show).toHaveBeenCalledWith(TransferDataModalComponent);
    expect(bsModalRef).toBeTruthy();
  }));

  // maps(event)
  it('should show Map modal', fakeAsync(() => {
    spyOn(component, 'maps').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#show-map'));
    let btnEl = btn.nativeElement;
    let classList = btnEl.classList;

    // classList is DOMTokenList
    let event = {
      'target': {
        'classList': classList
      }
    };

    expect(event.target.classList).not.toContain('active');

    // let event = {
    //   'target': {
    //     'classList': []
    //   }
    // };

    // var event = document.createEvent('Event');

    btn.triggerEventHandler('click', event);

    tick();
    fixture.detectChanges();

    expect(component.maps).toHaveBeenCalled();
    expect(event.target.classList).toContain('active');
    expect(bsModalService.show).toHaveBeenCalledWith(MapsComponent);
    expect(bsModalRef).toBeTruthy();
  }));

  // maps(event)
  // Error: No component factory found for MapsComponent. Did you add it to @NgModule.entryComponents?
  // it('should show Map modal', fakeAsync(() => {
  //   let btn = fixture.debugElement.query(By.css('#show-map'));
  //   let btnEl = btn.nativeElement;
  //   let classList = btnEl.classList;

  //   // classList is DOMTokenList
  //   let event = {
  //     'target': {
  //       'classList': classList
  //     }
  //   };

  //   component.maps(event);

  //   tick();
  //   fixture.detectChanges();

  //   expect(bsModalService.show).toHaveBeenCalledWith(MapsComponent);
  // }));

  // addPhone
  it('should add phone number', () => {
    spyOn(component, 'addPhone').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#add-phone'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.addPhone).toHaveBeenCalled();
    // beforeEach: added one phone number
    // addPhone: adds one more phone number
    expect(component.content.phone.length).toEqual(2);
    expect(component.content.phone).toContain({type: 'home'});
  });

  // addPhone
  it('should NOT add more phone numbers after 5 phone numbers', () => {
    spyOn(component, 'addPhone').and.callThrough();
    
    // beforeEach: added one phone number
    // addPhone: attempts to add 5 more numbers
    let btn = fixture.debugElement.query(By.css('#add-phone'));
    btn.triggerEventHandler('click', null);
    btn.triggerEventHandler('click', null);
    btn.triggerEventHandler('click', null);
    btn.triggerEventHandler('click', null);
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.addPhone).toHaveBeenCalled();
    expect(component.content.phone.length).toEqual(5);
    expect(component.content.phone).toContain({type: 'home'});
  });

  // addStar
  it('should add star', () => {
    spyOn(component, 'addStar').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#add-star-1'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.addStar).toHaveBeenCalled();
  });

  // nextState
  it('should go to next state', () => {
    spyOn(component, 'nextState').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#next-state'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    let leadCreation = fixture.debugElement.query(By.css('#select-creation'));
    let leadCreationEl = leadCreation.nativeElement;

    let policyInfo = fixture.debugElement.query(By.css('#select-policy'));
    let policyInfoEl = policyInfo.nativeElement;

    let driverInfo = fixture.debugElement.query(By.css('#select-drive'));
    let driverInfoEl = driverInfo.nativeElement;
    
    expect(component.nextState).toHaveBeenCalled();
    // Clicked red arrow from Lead creation to Policy information
    // First two circles should be changed from grey to blue (active)
    expect(leadCreationEl.getAttribute('class')).toContain('active');
    expect(policyInfoEl.getAttribute('class')).toContain('active');
    expect(driverInfoEl.getAttribute('class')).not.toContain('active');
  });

  // phoneTypeChange
  it('should change phone type', () => {
    spyOn(component, 'phoneTypeChange').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#change-phone-type'));
    btn.triggerEventHandler('change', null);

    fixture.detectChanges();
    
    expect(component.phoneTypeChange).toHaveBeenCalled();
  });

  // removePhone
  it('should remove phone number', async() => {
    spyOn(component, 'removePhone').and.callThrough();

    // Add phone so remove phone is available
    let btn = fixture.debugElement.query(By.css('#add-phone'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // Remove phone
    let remove = fixture.debugElement.query(By.css('#remove-phone-1'));
    remove.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.removePhone).toHaveBeenCalled();
    });
  });

  // addEmail
  it('should add email address', () => {
    spyOn(component, 'addEmail').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#add-email'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.addEmail).toHaveBeenCalled();
    expect(component.content.email.length).toEqual(2);
    expect(component.content.email).toContain('');
  });

  it('should NOT add more email addresses after 3 email addresses', () => {
    spyOn(component, 'addEmail').and.callThrough();
    
    // beforeEach: added one email address
    // addPhone: attempts to add 3 more email addresses
    let btn = fixture.debugElement.query(By.css('#add-email'));
    btn.triggerEventHandler('click', null);
    btn.triggerEventHandler('click', null);
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.addEmail).toHaveBeenCalled();
    expect(component.content.email.length).toEqual(3);
    expect(component.content.email).toContain('');
  });

  // selectState (not in html)

  // makeCall
  it('should make call', () => {
    spyOn(component, 'makeCall').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#make-call'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.makeCall).toHaveBeenCalled();
  });

  // selectType
  it('should select Lead creation', () => {
    spyOn(component, 'selectType').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#select-creation'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.selectType).toHaveBeenCalled();
    expect(component.stateflow).toEqual('creation');
  });

  it('should select Policy information', () => {
    spyOn(component, 'selectType').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#select-policy'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.selectType).toHaveBeenCalled();
    expect(component.stateflow).toEqual('policy');
  });

  it('should select Driver information', () => {
    spyOn(component, 'selectType').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#select-drive'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.selectType).toHaveBeenCalled();
    expect(component.stateflow).toEqual('drive');
  });

  it('should select Vehicle', () => {
    spyOn(component, 'selectType').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#select-vehicle'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.selectType).toHaveBeenCalled();
    expect(component.stateflow).toEqual('vehicle');
  });

  it('should select Coverage', () => {
    spyOn(component, 'selectType').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#select-coverage'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.selectType).toHaveBeenCalled();
    expect(component.stateflow).toEqual('coverage');
  });

  it('should select Carrier', () => {
    spyOn(component, 'selectType').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#select-carrier'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.selectType).toHaveBeenCalled();
    expect(component.stateflow).toEqual('carrier');
  });

  it('should select Rates', () => {
    spyOn(component, 'selectType').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#select-rates'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.selectType).toHaveBeenCalled();
    expect(component.stateflow).toEqual('rates');
  });

  // showLead
  it('should show Use Lead modal', () => {
    spyOn(component, 'showLead').and.callThrough();

    let obj = {};

    let btn = fixture.debugElement.query(By.css('#use-lead'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.showLead).toHaveBeenCalled();
  });

  it('should show Use Lead modal (pass obj)', () => {
    spyOn(component, 'showLead').and.callThrough();

    let obj = {};

    component.showLead(obj);

    fixture.detectChanges();
    
    expect(component.showLead).toHaveBeenCalled();
    expect(obj['component']).toEqual(UserleadComponent);
    expect(obj['type']).toEqual('my-leads');
    expect(obj['isNew']).toEqual(true)
    expect(obj['active']).toEqual(true);
    expect(obj['removable']).toEqual(true);
    expect(leadAdapterClass.setOnCall).toHaveBeenCalledWith(obj);
  });
});
