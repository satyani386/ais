import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { UserleadComponent } from '../userlead/userlead.component';

import { DaterangepickerConfig } from "ng2-daterangepicker"

import * as moment from "moment";


@Component({
  selector: 'app-my-leads',
  templateUrl: './my-leads.component.html',
  styleUrls: ['./my-leads.component.scss']
})

export class MyLeadsComponent implements OnInit {

  /*ngx-bootstrap date range picker*/
  minDate = new Date(2017, 5, 10);
  maxDate = new Date(2018, 9, 15);
  bsValue: Date = new Date();
  bsRangeValue: any = [new Date(2017, 7, 4), new Date(2017, 7, 20)];

  /*Daterangepicker options*/
  public mainInput = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(6, 'month')
  };

  public prodFilter = false;
  public stateFilter = false;

  @Input() currentTab: any;
  @Output() showMyLead = new EventEmitter();
  public MyLeads: any = [];
  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  scrollbarOptions2 = { axis: 'y', theme: 'dark-3' };
  public scrollOptions = { axis: 'y', theme: 'dark-4' };

  constructor(private _leadAdapter: LeadAdapterClass, private DateRanageConfig: DaterangepickerConfig) {

    this.DateRanageConfig.settings = {
      locale: { format: 'MM-DD-YYYYY' },
      alwaysShowCalendars: false,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'day'), moment()],
        'Last 7 Days': [moment().subtract(7, 'day'), moment()],
        'Month-to-date': [moment().subtract(1, 'month'), moment()],
        'Last month': [moment().subtract(1, 'month'), moment()],
        'Year-to-date': [moment().subtract(12, 'month'), moment()],
      }
    };

    this.MyLeads =
      [
        { date: 1, name: 'Mark', lastname: 'xyz', username: '1234569' },
        { date: 3, name: 'Larry4', lastname: 'the Bird', username: '@twitter' },
        { date: 5, name: 'Larry3', lastname: 'the Bird', username: '@twitter' },
        { date: 2, name: 'Jacob', lastname: 'Thornton', username: '@fat' },
        { date: 6, name: 'Larry4', lastname: 'the Bird', username: '@twitter' },
        { date: 7, name: 'Larry5', lastname: 'the Bird', username: '@twitter' },
        { date: 3, name: 'Larry1', lastname: 'the Bird', username: '@twitter' },
        { date: 8, name: 'Larry6', lastname: 'the Bird', username: '@twitter' },
        { date: 10, name: 'Larry8', lastname: 'the Bird', username: '@twitter' },
        { date: 11, name: 'Larry9', lastname: 'the Bird', username: '@twitter' },
        { date: 12, name: 'Larry10', lastname: 'the Bird', username: '@twitter' },
        { date: 9, name: 'Larry7', lastname: 'the Bird', username: '@twitter' },
        { date: 13, name: 'a', lastname: 'the Bird1', username: '@twitter' },
        { date: 13, name: 'a', lastname: 'bird0', username: '@twitter' },
        { date: 13, name: 'z', lastname: 'the Bird', username: '@twitter' },
        { date: 13, name: 'gv', lastname: 'the Bird', username: '@twitter' },
        { date: 13, name: 'bx', lastname: 'the Bird', username: '@twitter' },
        { date: 13, name: 'aa', lastname: 'the Bird', username: '@twitter' },
        { date: 13, name: 'ba', lastname: 'the Bird', username: '@twitter' },
        { date: 14, name: 'Larry12', lastname: 'the Bird', username: '@twitter' }
      ]
  }

  ngOnInit() {

  }

  group = this._leadAdapter.getLeads();

  toggleFilter(type) {
    if (type === 'prod') {
      this.prodFilter = !this.prodFilter;
    } else if (type === 'state') {
      this.stateFilter = !this.stateFilter;
    }
  }

  showLead(obj) {
    obj.contentId = "tab_content_" + obj.id;
    obj.active = true;
    obj.removable = true;
    obj.component = UserleadComponent;
    obj.phone = [{ type: 'home' }];
    obj.email = [''];
    obj.product = [{ type: 'Auto' }];
    this.showMyLead.emit(obj)
  }

  selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    console.log(dateInput);
  }
}
