import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { MyLeadsComponent } from './my-leads.component';
import { UserleadComponent } from '../userlead/userlead.component';
import { SortDirective } from '../../../directives/sort.directive'; 
import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../../services/leads/leads.service';
import { HttpModule } from '@angular/http';
import { Daterangepicker } from "ng2-daterangepicker";
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { TabsModule, AccordionModule } from 'ngx-bootstrap';

import { FormsModule } from '@angular/forms';

fdescribe('MyLeadsComponent', () => {
  let component: MyLeadsComponent;
  let fixture: ComponentFixture<MyLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        MyLeadsComponent,
        SortDirective,
        UserleadComponent
       ],
      imports: [
        HttpModule,
        Daterangepicker,
        MalihuScrollbarModule.forRoot(),
        TabsModule.forRoot(),
        AccordionModule.forRoot(),
        FormsModule
      ],
      providers: [
        LeadAdapterClass,
        LeadsService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // toggleFilter
  it('should show product filter', () => {
    let btn = fixture.debugElement.query(By.css('#filter-product'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.prodFilter).toEqual(true);
  });

  it('should hide product filter', () => {
    component.prodFilter = true;

    let btn = fixture.debugElement.query(By.css('#filter-product'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.prodFilter).toEqual(false);
  });

  // toggleFilter(state)
  it('should show state filter', () => {
    let btn = fixture.debugElement.query(By.css('#filter-state'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.stateFilter).toEqual(true);
  });

  it('should hide state filter', () => {
    component.stateFilter = true;

    let btn = fixture.debugElement.query(By.css('#filter-state'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.stateFilter).toEqual(false);
  });

  // showLead
  it('should show lead', (done) => {
    component.showMyLead.subscribe(x => {
      expect(x.contentId).toEqual("tab_content_" + x.id);
      expect(x.active).toEqual(true);
      expect(x.removable).toEqual(true);
      expect(x.component).toEqual(UserleadComponent);
      done();
    });

    let btn = fixture.debugElement.query(By.css('#show-lead-1'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
  });

  let event = {
    'target': {
      'key': 'date'
    }
  };

  // TODO: sort
  // it('should sort by initial date contacted', () => {
  //   spyOn(component, 'sort').and.callThrough();
  //   let btn = fixture.debugElement.query(By.css('#initial-date'));
  //   btn.triggerEventHandler('click', event);
  //   // btn.triggerEventHandler('click', null);

  //   fixture.detectChanges();

  //   expect(component.sort).toHaveBeenCalled();
  //   // expect(component.SortObject[event.target.key]).toEqual(true);
  // });

  // selectedDate
  it('should select date', fakeAsync(() => {
    spyOn(component, 'selectedDate').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#daterangepicker'));
    btn.triggerEventHandler('selected', event);

    tick();
    fixture.detectChanges();

    // let calendar = fixture.debugElement.query(By.css('.daterangepicker .dropdown-menu .ltr .opensright'));
    // let calendar = fixture.debugElement.query(By.css('.range_inputs'));

    expect(component.selectedDate).toHaveBeenCalled();
    // expect(calendar).toBeTruthy();
  }));

});
