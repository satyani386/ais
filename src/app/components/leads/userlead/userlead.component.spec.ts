import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { UserleadComponent } from './userlead.component';
import { SourceModalComponent } from "../../source-modal/source-modal.component"
import { MapsComponent } from "../maps/maps.component";
import { ScheduleFollowUpComponent } from 'app/components/schedule-follow-up/schedule-follow-up.component';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { TabsModule, AccordionModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { AbstarctAdapterClass } from "../../../services/uiadaptors/AbstractAdaptor";
import { ValidvaluesService } from '../../../services/validvalues/validvalues.service';
import { BsModalRef, BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { IngeniusAdapterClass } from "../../../services/uiadaptors/IngeniusAdapter";
import { IngeniusService } from "../../../services/ingenius/ingenius.service";

fdescribe('UserleadComponent', () => {
  let component: UserleadComponent;
  let fixture: ComponentFixture<UserleadComponent>;
  let bsModalService: BsModalService;
  let bsModalRef: BsModalRef;
  let ingeniusAdapterClass: IngeniusAdapterClass;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        UserleadComponent,
        SourceModalComponent,
        MapsComponent,
        ScheduleFollowUpComponent
      ],
      imports: [
        MalihuScrollbarModule.forRoot(),
        TabsModule.forRoot(),
        AccordionModule.forRoot(),
        HttpModule,
        ModalModule.forRoot()
      ],
      providers: [
        AbstarctAdapterClass,
        ValidvaluesService,
        BsModalService,
        BsModalRef,
        IngeniusAdapterClass,
        IngeniusService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserleadComponent);
    component = fixture.componentInstance;
    bsModalService = fixture.debugElement.injector.get(BsModalService);
    bsModalRef = fixture.debugElement.injector.get(BsModalRef);
    ingeniusAdapterClass = fixture.debugElement.injector.get(IngeniusAdapterClass);
    spyOn(bsModalService, 'show').and.callThrough();
    spyOn(bsModalRef, 'hide').and.callThrough();
    spyOn(ingeniusAdapterClass, 'getConnectionObj').and.callThrough();

    // To fix: Uncaught TypeError: Cannot read property 'phone' of undefined
    component.content = [];
    component.content.phone = [];
    component.content.phone.push('123-456-7890');

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // sourceData
  it('should show Source modal', () => {
    spyOn(component, 'sourceData').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#user-lead-source-data'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.sourceData).toHaveBeenCalled();
    expect(bsModalService.show).toHaveBeenCalledWith(SourceModalComponent);
  });

  // TransferData (not in html)

  // maps
  it('should show Map modal', fakeAsync(() => {
    spyOn(component, 'maps').and.callThrough();

    let btn = fixture.debugElement.query(By.css('#user-lead-show-map'));
    let btnEl = btn.nativeElement;
    let classList = btnEl.classList;

    // classList is DOMTokenList
    let event = {
      'target': {
        'classList': classList
      }
    };

    expect(event.target.classList).not.toContain('active');

    btn.triggerEventHandler('click', event);

    tick();
    fixture.detectChanges();

    expect(component.maps).toHaveBeenCalled();
    expect(event.target.classList).toContain('active');
    expect(bsModalService.show).toHaveBeenCalledWith(MapsComponent);
    expect(bsModalRef).toBeTruthy();
  }));

  // scheduleFollowUp
  it('should show Schedule Follow-Up modal', () => {
    spyOn(component, 'scheduleFollowUp').and.callThrough();
    
    let btn = fixture.debugElement.query(By.css('#user-lead-schedule-followup'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.scheduleFollowUp).toHaveBeenCalled();
    expect(bsModalService.show).toHaveBeenCalledWith(ScheduleFollowUpComponent);
  });

  // addPhone (commented out in html)

  // phoneTypeChange (commented out in html)

  // addEmail (commented out in html)

  // selectState (not in html)

  // makeCall
  it('should make call', fakeAsync(() => {
    spyOn(component, 'makeCall').and.callThrough();

    let phone = '123-456-7890';
    
    let btn = fixture.debugElement.query(By.css('#user-lead-make-call'));
    btn.triggerEventHandler('click', phone);

    tick();
    fixture.detectChanges();
    
    expect(component.makeCall).toHaveBeenCalled();
    expect(ingeniusAdapterClass.getConnectionObj).toHaveBeenCalled();
  }));

  // Leads tab
  it('should show Leads tab', () => {
    let leadLink = fixture.debugElement.query(By.css('#user-lead-lead-state-link'));

    // let leadsTab = fixture.debugElement.query(By.css('#tab1'));
    // let leadsTabEl = leadsTab.nativeElement;
    // let quoteTab = fixture.debugElement.query(By.css('#tab2'));
    // let quoteTabEl = quoteTab.nativeElement;
    // let policyTab = fixture.debugElement.query(By.css('#tab3'));
    // let policyTabEl = policyTab.nativeElement;

    let leadsTab = fixture.debugElement.query(By.css('#user-lead-lead-state'));
    let leadsTabEl = leadsTab.nativeElement;
    let quoteTab = fixture.debugElement.query(By.css('#user-lead-quote-state'));
    let quoteTabEl = quoteTab.nativeElement;
    let policyTab = fixture.debugElement.query(By.css('#user-lead-policy-state'));
    let policyTabEl = policyTab.nativeElement;

    leadLink.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    // let leadsTable = fixture.debugElement.query(By.css('#user-lead-leads-table'));
    // let quoteTable = fixture.debugElement.query(By.css('#user-lead-quote-table'));
    // let policyTable = fixture.debugElement.query(By.css('#user-lead-policy-table'));
    
    expect(leadsTabEl.getAttribute('class')).toContain('active');
    expect(quoteTabEl.getAttribute('class')).not.toContain('active');
    expect(policyTabEl.getAttribute('class')).not.toContain('active');
  });

  // Quote tab
  it('should show Quote tab', () => {
    let quoteLink = fixture.debugElement.query(By.css('#user-lead-quote-state-link'));

    let leadsTab = fixture.debugElement.query(By.css('#user-lead-lead-state'));
    let leadsTabEl = leadsTab.nativeElement;
    let quoteTab = fixture.debugElement.query(By.css('#user-lead-quote-state'));
    let quoteTabEl = quoteTab.nativeElement;
    let policyTab = fixture.debugElement.query(By.css('#user-lead-policy-state'));
    let policyTabEl = policyTab.nativeElement;

    quoteLink.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    // let leadsTable = fixture.debugElement.query(By.css('#user-lead-leads-table'));
    // let quoteTable = fixture.debugElement.query(By.css('#user-lead-quote-table'));
    // let policyTable = fixture.debugElement.query(By.css('#user-lead-policy-table'));
    
    expect(leadsTabEl.getAttribute('class')).not.toContain('active');
    expect(quoteTabEl.getAttribute('class')).toContain('active');
    expect(policyTabEl.getAttribute('class')).not.toContain('active');
  });

  // Policy tab
  it('should show Policy tab', () => {
    let policyLink = fixture.debugElement.query(By.css('#user-lead-policy-state-link'));

    let leadsTab = fixture.debugElement.query(By.css('#user-lead-lead-state'));
    let leadsTabEl = leadsTab.nativeElement;
    let quoteTab = fixture.debugElement.query(By.css('#user-lead-quote-state'));
    let quoteTabEl = quoteTab.nativeElement;
    let policyTab = fixture.debugElement.query(By.css('#user-lead-policy-state'));
    let policyTabEl = policyTab.nativeElement;

    policyLink.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    // let leadsTable = fixture.debugElement.query(By.css('#user-lead-leads-table'));
    // let quoteTable = fixture.debugElement.query(By.css('#user-lead-quote-table'));
    // let policyTable = fixture.debugElement.query(By.css('#user-lead-policy-table'));
    
    expect(leadsTabEl.getAttribute('class')).not.toContain('active');
    expect(quoteTabEl.getAttribute('class')).not.toContain('active');
    expect(policyTabEl.getAttribute('class')).toContain('active');
  });

  // Quote, Cross-Sell, Rerate Buttons
  it('should enable Quote button when Leads tab selected', () => {
    let leadLink = fixture.debugElement.query(By.css('#user-lead-lead-state-link'));

    let quote = fixture.debugElement.query(By.css('#quote-option'));
    let quoteEl = quote.nativeElement;
    let crossSell = fixture.debugElement.query(By.css('#cross-sell-option'));
    let crossSellEl = crossSell.nativeElement;
    let rerate = fixture.debugElement.query(By.css('#rerate-option'));
    let rerateEl = rerate.nativeElement;

    leadLink.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(quoteEl.getAttribute('class')).toContain('active');
    expect(crossSellEl.getAttribute('class')).not.toContain('active');
    expect(rerateEl.getAttribute('class')).not.toContain('active');
  });

  it('should enable Quote and Cross-Sell buttons when Quote tab selected', () => {
    let quoteLink = fixture.debugElement.query(By.css('#user-lead-quote-state-link'));

    let quote = fixture.debugElement.query(By.css('#quote-option'));
    let quoteEl = quote.nativeElement;
    let crossSell = fixture.debugElement.query(By.css('#cross-sell-option'));
    let crossSellEl = crossSell.nativeElement;
    let rerate = fixture.debugElement.query(By.css('#rerate-option'));
    let rerateEl = rerate.nativeElement;

    quoteLink.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(quoteEl.getAttribute('class')).toContain('active');
    expect(crossSellEl.getAttribute('class')).toContain('active');
    expect(rerateEl.getAttribute('class')).not.toContain('active');
  });

  it('should enable Quote, Cross-Sell, and Rerate buttons when Policy tab selected', () => {
    let policyLink = fixture.debugElement.query(By.css('#user-lead-policy-state-link'));

    let quote = fixture.debugElement.query(By.css('#quote-option'));
    let quoteEl = quote.nativeElement;
    let crossSell = fixture.debugElement.query(By.css('#cross-sell-option'));
    let crossSellEl = crossSell.nativeElement;
    let rerate = fixture.debugElement.query(By.css('#rerate-option'));
    let rerateEl = rerate.nativeElement;

    policyLink.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(quoteEl.getAttribute('class')).toContain('active');
    expect(crossSellEl.getAttribute('class')).toContain('active');
    expect(rerateEl.getAttribute('class')).toContain('active');
  });

  // Referral tab
  it('should show Referral tab', () => {
    let referralLink = fixture.debugElement.query(By.css('#user-lead-referral-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    referralLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).not.toContain('active', 'email');
    expect(textEl.getAttribute('class')).not.toContain('active', 'text');
    expect(chatEl.getAttribute('class')).not.toContain('active', 'chat');
  });

  // Email tab
  it('should show Email tab', () => {
    let emailLink = fixture.debugElement.query(By.css('#user-lead-email-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    emailLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).not.toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).toContain('active', 'email');
    expect(textEl.getAttribute('class')).not.toContain('active', 'text');
    expect(chatEl.getAttribute('class')).not.toContain('active', 'chat');
  });

  it('should show Email tab - Sent/Opened Emails', () => {
    let emailLink = fixture.debugElement.query(By.css('#user-lead-email-link'));
    let emailSentLink = fixture.debugElement.query(By.css('#user-lead-email-sent-opened-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let emailSent = fixture.debugElement.query(By.css('#user-lead-email-sent-opened'));
    let emailSentEl = emailSent.nativeElement;
    let emailSend = fixture.debugElement.query(By.css('#user-lead-email-send'));
    let emailSendEl = emailSend.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    emailLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    emailSentLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).not.toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).toContain('active', 'email');
    expect(emailSentEl.getAttribute('class')).toContain('active', 'email sent');
    expect(emailSendEl.getAttribute('class')).not.toContain('active', 'email send');
    expect(textEl.getAttribute('class')).not.toContain('active', 'text');
    expect(chatEl.getAttribute('class')).not.toContain('active', 'chat');
  });

  it('should show Email tab - Send Emails', () => {
    let emailLink = fixture.debugElement.query(By.css('#user-lead-email-link'));
    let emailSendLink = fixture.debugElement.query(By.css('#user-lead-email-send-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let emailSent = fixture.debugElement.query(By.css('#user-lead-email-sent-opened'));
    let emailSentEl = emailSent.nativeElement;
    let emailSend = fixture.debugElement.query(By.css('#user-lead-email-send'));
    let emailSendEl = emailSend.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    emailLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    emailSendLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).not.toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).toContain('active', 'email');
    expect(emailSentEl.getAttribute('class')).not.toContain('active', 'email sent');
    expect(emailSendEl.getAttribute('class')).toContain('active', 'email send');
    expect(textEl.getAttribute('class')).not.toContain('active', 'text');
    expect(chatEl.getAttribute('class')).not.toContain('active', 'chat');
  });

  // Text tab
  it('should show Text tab', () => {
    let textLink = fixture.debugElement.query(By.css('#user-lead-text-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    textLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).not.toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).not.toContain('active', 'email');
    expect(textEl.getAttribute('class')).toContain('active', 'text');
    expect(chatEl.getAttribute('class')).not.toContain('active', 'chat');
  });

  it('should show Text tab - Sent Texts', () => {
    let textLink = fixture.debugElement.query(By.css('#user-lead-text-link'));
    let textSentLink = fixture.debugElement.query(By.css('#user-lead-text-sent-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let textSent = fixture.debugElement.query(By.css('#user-lead-text-sent'));
    let textSentEl = textSent.nativeElement;
    let textSend = fixture.debugElement.query(By.css('#user-lead-text-send'));
    let textSendEl = textSend.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    textLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    textSentLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).not.toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).not.toContain('active', 'email');
    expect(textEl.getAttribute('class')).toContain('active', 'text');
    expect(textSentEl.getAttribute('class')).toContain('active', 'text sent');
    expect(textSendEl.getAttribute('class')).not.toContain('active', 'text send');
    expect(chatEl.getAttribute('class')).not.toContain('active', 'chat');
  });

  it('should show Text tab - Send Texts', () => {
    let textLink = fixture.debugElement.query(By.css('#user-lead-text-link'));
    let textSendLink = fixture.debugElement.query(By.css('#user-lead-text-send-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let textSent = fixture.debugElement.query(By.css('#user-lead-text-sent'));
    let textSentEl = textSent.nativeElement;
    let textSend = fixture.debugElement.query(By.css('#user-lead-text-send'));
    let textSendEl = textSend.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    textLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    textSendLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).not.toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).not.toContain('active', 'email');
    expect(textEl.getAttribute('class')).toContain('active', 'text');
    expect(textSentEl.getAttribute('class')).not.toContain('active', 'text sent');
    expect(textSendEl.getAttribute('class')).toContain('active', 'text send');
    expect(chatEl.getAttribute('class')).not.toContain('active', 'chat');
  });

  // Chat tab
  it('should show Chat tab', () => {
    let chatLink = fixture.debugElement.query(By.css('#user-lead-chat-link'));

    let referral = fixture.debugElement.query(By.css('#user-lead-referral'));
    let referralEl = referral.nativeElement;
    let email = fixture.debugElement.query(By.css('#user-lead-email'));
    let emailEl = email.nativeElement;
    let text = fixture.debugElement.query(By.css('#user-lead-text'));
    let textEl = text.nativeElement;
    let chat = fixture.debugElement.query(By.css('#user-lead-chat'));
    let chatEl = chat.nativeElement;

    chatLink.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(referralEl.getAttribute('class')).not.toContain('active', 'referral');
    expect(emailEl.getAttribute('class')).not.toContain('active', 'email');
    expect(textEl.getAttribute('class')).not.toContain('active', 'text');
    expect(chatEl.getAttribute('class')).toContain('active', 'chat');
  });

});
