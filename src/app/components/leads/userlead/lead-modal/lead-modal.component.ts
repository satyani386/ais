import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap"

@Component({
  selector: 'app-lead-modal',
  templateUrl: './lead-modal.component.html',
  styleUrls: ['./lead-modal.component.scss']
})
export class LeadModalComponent implements OnInit {

  constructor(public modelRef: BsModalRef) { } 

  ngOnInit() {
  }
  public LeadsData = [
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
  ];
}
