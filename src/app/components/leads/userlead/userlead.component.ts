
import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { IngeniusAdapterClass } from "../../../services/uiadaptors/IngeniusAdapter";
import { AbstarctAdapterClass } from "../../../services/uiadaptors/AbstractAdaptor";
import { HideShowService } from '../../../services/HideShow/Hide-Show.service';
import { ScheduleFollowUpComponent } from "../../schedule-follow-up/schedule-follow-up.component"
import { SourceModalComponent } from "../../source-modal/source-modal.component"
import { LeadModalComponent } from "./lead-modal/lead-modal.component"
import { MapsComponent } from "../maps/maps.component"
import { TransferDataModalComponent } from "../../transfer-data-modal/transfer-data-modal.component"


@Component({
  selector: 'app-userlead',
  templateUrl: './userlead.component.html',
  styleUrls: ['./userlead.component.scss'],
  providers: [HideShowService]
})
export class UserleadComponent implements OnInit {
  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  scrollbarOptions2 = { axis: 'y', theme: 'dark-3' };
  bsModalRef: BsModalRef;
  public ValidValues: any;
  public PhoneData: any = [];
  public EditLeftForm = true;

  public state: string;
  public parkedAt: string;
  public stateHideShow: any;
  public content: any;
  public navstate = 'leadstate';

  public LeadsData = [
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
  ];
  public PolicyData = [
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    },
    {
      "date": "06/12/2017",
      "initialdate": "06/12/2017",
      "producer": "Mike Millane",
      "product": "Auto",
      "status": "Active",
      "name": "Jhon Smith",
      "source": "Google"
    }
  ];

  constructor(private AbstarctAdapterClass: AbstarctAdapterClass,

    private HideShowService: HideShowService,
    private LeadAdapterClass: LeadAdapterClass,
    private modalService: BsModalService,
    private IngeniusAdapterClass: IngeniusAdapterClass) {
    this.content = this.LeadAdapterClass.getOnCallData();
  }

  ngOnInit() {
  }
  edit() {
    this.EditLeftForm = !this.EditLeftForm;
  }
  sourceData() {
    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(SourceModalComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }

  TransferData() {
    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(TransferDataModalComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }

  maps(event) {
    event.target.classList.add('active')

    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(MapsComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }

  scheduleFollowUp() {
    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(ScheduleFollowUpComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }
  
  LeadData() {
    const list = [
      'Open a modal with component',
      'Pass your data',
      'Do something else',
      '...'
    ];
    this.bsModalRef = this.modalService.show(LeadModalComponent);
    this.bsModalRef.content.title = 'Modal with component';
    this.bsModalRef.content.list = list;
    setTimeout(() => {
      list.push('PROFIT!!!');
    }, 2000);
  }
  addPhone() {
    if (this.content.phone.length < 5) {
      this.content.phone.push({ type: 'home' })
    }
  }
  phoneTypeChange(event, obj) {
    obj.type = event.target.value;
  }

  removePhone(i) {
    this.content.phone.splice(i, 1);
  }

  addEmail() {
    if (this.content.email.length < 3) {
      this.content.email.push('')
    }
  }
  removeEmail(i) {
    this.content.email.splice(i, 1);
  }
  addProduct() {
    if (this.content.product.length < 5) {
      this.content.product.push({ type: 'Auto' })
    }
  }
  removeProduct(i) {
    this.content.product.splice(i, 1);
  }

  createNewQuote(){
    console.log('Open create new quote');
  }

  selectState(data) {
    this.stateHideShow = [];
    let that = this;
    if (data.trim().length === 0) {
      return false;
    }
    this.HideShowService.getStateHideShow(data).subscribe(function (resp) {
      that.stateHideShow = resp;
    }, function (err) {
      console.log(err)
    })
  }

  makeCall(phone) {
    let ctiApi = this.IngeniusAdapterClass.getConnectionObj();
    ctiApi.Dial(phone).then(function (data) {
      console.log(data);
    })
  }
}
