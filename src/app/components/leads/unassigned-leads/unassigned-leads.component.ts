import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LeadAdapterClass} from "../../../services/uiadaptors/leadsAdapter";
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {UserleadComponent} from '../userlead/userlead.component';

@Component({
  selector: 'app-unassigned-leads',
  templateUrl: './unassigned-leads.component.html',
  styleUrls: ['./unassigned-leads.component.scss'],
  providers:[LeadAdapterClass]
})

export class UnassignedLeadsComponent implements OnInit {

  @Output() showMyLead = new EventEmitter();
  public UnassignLeads: any = [];
  scrollbarOptions = {axis: 'y', theme: 'dark-3'};
  scrollbarOptions2 = {axis: 'y', theme: 'dark-3'};
  public scrollOptions = { axis: 'y', theme: 'dark-3' };

  constructor(private _leadAdapter: LeadAdapterClass) {
    this.UnassignLeads = [
      {id: 1, name: 'Mark', lastname: 'Otto', username: '@mdo'},
      {id: 2, name: 'Jacob', lastname: 'Thornton', username: '@fat'},
      {id: 3, name: 'Larry1', lastname: 'the Bird', username: '@twitter'},
      {id: 4, name: 'Larry2', lastname: 'the Bird', username: '@twitter'},
      {id: 5, name: 'Larry3', lastname: 'the Bird', username: '@twitter'},
      {id: 6, name: 'Larry4', lastname: 'the Bird', username: '@twitter'},
      {id: 7, name: 'Larry5', lastname: 'the Bird', username: '@twitter'},
      {id: 8, name: 'Larry6', lastname: 'the Bird', username: '@twitter'},
      {id: 9, name: 'Larry7', lastname: 'the Bird', username: '@twitter'},
      {id: 10, name: 'Larry8', lastname: 'the Bird', username: '@twitter'},
      {id: 11, name: 'Larry9', lastname: 'the Bird', username: '@twitter'},
      {id: 12, name: 'Larry10', lastname: 'the Bird', username: '@twitter'},
      {id: 13, name: 'Larry11', lastname: 'the Bird', username: '@twitter'},
      {id: 14, name: 'Larry12', lastname: 'the Bird', username: '@twitter'}, {id: 11, name: 'Larry9', lastname: 'the Bird', username: '@twitter'},
      {id: 12, name: 'Larry10', lastname: 'the Bird', username: '@twitter'},
      {id: 13, name: 'Larry11', lastname: 'the Bird', username: '@twitter'},
      {id: 14, name: 'Larry12', lastname: 'the Bird', username: '@twitter'}
    ]
  }

  ngOnInit() {

  }

  group = this._leadAdapter.getLeads();


 showLead(obj) {
    obj.contentId = "tab_content_" + obj.id;
    obj.active = true;
    obj.removable = true;
    obj.component = UserleadComponent;
    this.showMyLead.emit(obj)
  }
}
