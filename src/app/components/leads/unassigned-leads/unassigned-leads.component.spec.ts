import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { UnassignedLeadsComponent } from './unassigned-leads.component';
import { UserleadComponent } from '../userlead/userlead.component';

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { HttpModule } from '@angular/http';
import { LeadsService } from '../../../services/leads/leads.service';
import { TabsModule, AccordionModule } from 'ngx-bootstrap';

import { FormsModule } from '@angular/forms';

describe('UnassignedLeadsComponent', () => {
  let component: UnassignedLeadsComponent;
  let fixture: ComponentFixture<UnassignedLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        UnassignedLeadsComponent,
        UserleadComponent
       ],
      imports: [
        MalihuScrollbarModule.forRoot(),
        HttpModule,
        TabsModule.forRoot(),
        AccordionModule.forRoot(),
        FormsModule
      ],
      providers: [
        LeadsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnassignedLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // showLead
  it('should show lead', (done) => {
    component.showMyLead.subscribe(x => {
      expect(x.contentId).toEqual("tab_content_" + x.id);
      expect(x.active).toEqual(true);
      expect(x.removable).toEqual(true);
      expect(x.component).toEqual(UserleadComponent);
      done();
    });

    let btn = fixture.debugElement.query(By.css('#show-unassigned-lead-1'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
  });

});
