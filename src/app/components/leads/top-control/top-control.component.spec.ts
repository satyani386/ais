import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { By }              from '@angular/platform-browser';
import { TopControlComponent } from './top-control.component';
import { NewLeadsComponent } from '../new-leads/new-leads.component';
// import { ViewSelectorComponent } from '../../view-selector/view-selector.component';
import { ActivityHandler } from '../../activityHandler/activityHandler.component';

import { TabsModule } from 'ngx-bootstrap';
import { LeadAdapterClass } from "../../../services/uiadaptors/leadsAdapter";
import { HttpModule } from '@angular/http';
import { LeadsService } from '../../../services/leads/leads.service';
import { Router, RouterModule } from '@angular/router';
import { AppUserPrinciple } from '../../../models/user-principle/app.userPrinciple';
import { Event } from '@angular/router/src/events';

import { MockComponent } from 'ng2-mock-component';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

describe('TopControlComponent', () => {
  let component: TopControlComponent;
  let fixture: ComponentFixture<TopControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        TopControlComponent,
        NewLeadsComponent,
        // ViewSelectorComponent,
        MockComponent({ 
          selector: 'app-view-selector', 
          inputs: ['newTab', 'current'], 
          outputs: [ 'tabChange' ]
        }),
        ActivityHandler
       ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
      imports: [
        TabsModule.forRoot(),
        HttpModule,
        MalihuScrollbarModule.forRoot()
      ],
      providers: [
        LeadAdapterClass,
        LeadsService,
        { 
          provide: Router, 
          useClass: class { navigate = jasmine.createSpy("navigate"); }
        },
        AppUserPrinciple
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // renderLead
  it('should render new lead', () => {
    spyOn(component, 'renderLead').and.callThrough();

    let event = {
      'active': false,
      'isNew': false,
    };

    component.renderLead(event);

    expect(component.renderLead).toHaveBeenCalled();
    expect(event.active).toEqual(true);
    expect(event.isNew).toEqual(true);
    expect(component.newTab).toEqual(event);
    expect(component.currentComponentTop).toEqual('new-lead');
  });

  // showView
  it('should show My Leads', () => {
    spyOn(component, 'showView').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#btn-my-leads'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.showView).toHaveBeenCalled();
    expect(component.currentComponentTop).toEqual('my-leads');
  });

  it('should show Unassigned Leads', () => {
    spyOn(component, 'showView').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#btn-unassigned'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.showView).toHaveBeenCalled();
    expect(component.currentComponentTop).toEqual('unassigned');
  });

  it('should show Create Leads', () => {
    spyOn(component, 'showView').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#btn-new-lead'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.showView).toHaveBeenCalled();
    expect(component.currentComponentTop).toEqual('new-lead');
  });

  it('should contain ViewSelectorComponent', () => {
    let vs = fixture.debugElement.query(By.css('app-view-selector'));
    expect(vs).toBeTruthy();
  });

  // sendTab
  // Error with mock event: Error: No component factory found for NewLeadsComponent. Did you add it to @NgModule.entryComponents?
  it('should send tab', () => {
    spyOn(component, 'sendTab').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#top-send-tab'));

    // let event = [{
    //   name: 'New Lead',
    //   type: 'new',
    //   active: true,
    //   removable: true,
    //   id: 'newTab',
    //   isNew: true,
    //   component: NewLeadsComponent,
    //   phone: [{type: 'home'}],
    //   email: ['']
    // }];

    btn.triggerEventHandler('tabChange', null);

    fixture.detectChanges();
    
    expect(component.sendTab).toHaveBeenCalled();
    // active = false by default
    expect(component.currentComponentTop).toEqual('my-leads');
  });

  // Mock ViewSelectorComponent and event
  // Errors when use real or mock ViewSelectorComponent
  // [object ErrorEvent] thrown
  // Uncaught Error: ViewDestroyedError: Attempt to use a destroyed view: detectChanges
  // it('should send tab', () => {
  //   spyOn(component, 'sendTab').and.callThrough();
  //   let vs = fixture.debugElement.query(By.css('app-view-selector'));

  //   // let event = [{
  //   //   name: 'New Lead',
  //   //   type: 'new',
  //   //   active: true,
  //   //   removable: true,
  //   //   id: 'newTab',
  //   //   isNew: true,
  //   //   component: NewLeadsComponent,
  //   //   phone: [{type: 'home'}],
  //   //   email: ['']
  //   // }];

  //   vs.triggerEventHandler('tabChange', null);

  //   // fixture.detectChanges();

  //   expect(component.sendTab).toHaveBeenCalled();
  //   // expect(component.sendTab).toHaveBeenCalledWith(event);
  //   // change to active = true and this.tabs.length > 0
  //   // expect(component.currentComponentTop).toEqual('new-lead');
  // });

  // changeView (not in HTML, called by renderLead and showView)
});
