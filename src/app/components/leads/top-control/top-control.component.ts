import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {setTimeout} from "timers";
import {NewLeadsComponent} from '../new-leads/new-leads.component';
import {LeadAdapterClass} from "../../../services/uiadaptors/leadsAdapter";


@Component({
  selector: 'app-top-control',
  templateUrl: './top-control.component.html',
  styleUrls: ['./top-control.component.scss']
})
export class TopControlComponent implements OnChanges {
  @Output() tabCloseBtnClick = new EventEmitter();
  @Output() setView = new EventEmitter();
  @Input() newLeadOnCall: any;
  // @ViewChild('myDiv') myDiv: ElementRef;
  public tabs = [];
  public newTab: any;
  public currentTab: any;
  public currentComponentTop: any;

  constructor(private ref: ChangeDetectorRef,
              private LeadAdapterClass: LeadAdapterClass) {
    this.tabs = [];
    this.currentTab = {};
    const that = this;

    this.LeadAdapterClass.getOnCall().subscribe((data) => {
      if (data.isNew) {
        that.showView(data.type, data.isNew, data);
      } else {
        data.event.id = new Date().getTime();
        //Todo Remote Temp ID
        that.renderLead(data.event);
      }
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['newLeadOnCall']) {
      console.log(this.newLeadOnCall)
    }
  }

  renderLead(Obj) {
    Obj.active = true;
    Obj.isNew = true;
    console.log(Obj);
    this.newTab = Obj;
    this.changeView('new-lead');
  }

  showView(selectedView: string, isNewTab: boolean, data) {
    this.changeView(selectedView);
    if (!isNewTab) {
      return false
    }

    const tab = {
      name: 'New Lead',
      type: 'new',
      active: true,
      removable: true,
      id: 'newTab',
      isNew: true,
      component: NewLeadsComponent,
      phone: [{type: 'home'}],
      email: [''],
      product:[{type:'Auto'}]
    };

    if (data) {
      tab['phone'][0]['phone'] = data.event.RemotePartyNumber;
    } else {
      tab['phone'][0] = {type: 'home'};
    }

    this.newTab = tab;
    this.ref.detectChanges()
  }

  sendTab(event) {
    const that = this;
    let active = false;
    if (event) {
      this.tabs = event;
      this.tabs.forEach(function (ele) {
        if (ele.active) {
          that.currentTab = ele;
          active = true;
        }
      });
    }

    if (!active) {
      this.changeView('my-leads');
    } else {
      this.tabs.length > 0 ? this.changeView('new-lead') : this.changeView('my-leads');
    }
    setTimeout(function () {
      that.ref.detectChanges();
    }, 100)
  }


  changeView(view) {
    this.currentComponentTop = view;
    //this.ref.markForCheck()
    this.setView.emit(view);
  }
}
