import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { LeadsComponent } from './leads.component';
import { MyLeadsComponent } from './my-leads/my-leads.component';
import { NewLeadsComponent } from './new-leads/new-leads.component';
import { TopControlComponent } from './top-control/top-control.component';
import { UnassignedLeadsComponent } from './unassigned-leads/unassigned-leads.component';
import { ViewSelectorComponent } from '../view-selector/view-selector.component';
import { ActivityHandler } from '../activityHandler/activityHandler.component';

import { BsDatepickerModule, PaginationModule, TabsModule, TooltipModule, ModalModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { LeadAdapterClass } from "../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../services/leads/leads.service';
import { SortDirective } from '../../directives/sort.directive'; 
import { Router, RouterModule } from '@angular/router';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { Daterangepicker } from "ng2-daterangepicker";

import * as moment from "moment";

describe('LeadsComponent', () => {
  let component: LeadsComponent;
  let fixture: ComponentFixture<LeadsComponent>;
 
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        LeadsComponent,
        MyLeadsComponent,
        NewLeadsComponent,
        TopControlComponent,
        UnassignedLeadsComponent,
        SortDirective,
        ViewSelectorComponent,
        ActivityHandler
      ],
      imports: [
        Daterangepicker,
        TabsModule.forRoot(),
        HttpModule,
        MalihuScrollbarModule.forRoot()
      ],
      providers: [
        LeadAdapterClass,
        LeadsService,
        { 
          provide: Router, 
          useClass: class { navigate = jasmine.createSpy("navigate"); }
        },
        AppUserPrinciple
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // showView (Cannot access TopControl component from here so just test in TopControl)
  // it('should show Create Leads', () => {
  //   let btn = fixture.debugElement.query(By.css('#btn-new-lead'));
  //   btn.triggerEventHandler('click', null);

  //   fixture.detectChanges();
    
  //   expect(component.currentComponent).toEqual('new-lead');
  // });

  // it('should show My Leads', () => {
  //   let btn = fixture.debugElement.query(By.css('#btn-my-leads'));
  //   btn.triggerEventHandler('click', null);

  //   fixture.detectChanges();
    
  //   expect(component.currentComponent).toEqual('my-leads');
  // });

  // it('should show Unassigned Leads', () => {
  //   let btn = fixture.debugElement.query(By.css('#btn-unassigned'));
  //   btn.triggerEventHandler('click', null);

  //   fixture.detectChanges();
    
  //   expect(component.currentComponent).toEqual('unassigned');
  // });
});
