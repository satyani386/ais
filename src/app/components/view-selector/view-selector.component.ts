import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {Router} from '@angular/router';

import {AppUserPrinciple} from '../../models/user-principle/app.userPrinciple';


@Component({
  selector: 'app-view-selector',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './view-selector.component.html',
  styleUrls: ['./view-selector.component.scss']
})

export class ViewSelectorComponent implements OnInit, OnDestroy, OnChanges {


  //New Code
  @Output() tabChange = new EventEmitter();
  @Input() newTab: any;
  @Input() current: string;

  public Tabs = [];
  public PageNo = 1;
  public FilteredTabs = [];

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    const that = this;
    if (changes['newTab']) {
      if (typeof this.newTab === 'object') {
        const found = false;
        this.Tabs.forEach(function (item) {
          if (item.id === that.newTab.id) {
            //found = true;
          }
        });

        if (!found) {
          this.Tabs.push(this.newTab);
        }
        this.PageNo = Math.ceil(this.Tabs.length / 4);
        this.tabChange.emit(this.Tabs);
        this.updateData();
      }
    } else if (changes['current']) {
      if (this.current === 'my-leads' || this.current === 'unassigned') {
        this.Tabs.forEach(function (item) {
          item.active = false;
        });
        this.updateData();
      }
    }
  }

  ngOnDestroy() {
    this.updateData();
  }

  constructor(private _router: Router, private UserPrinciple: AppUserPrinciple, private  ref: ChangeDetectorRef) {
    this.Tabs = [];
    if (this.UserPrinciple.getContextObject('leads')) {
      this.Tabs = this.UserPrinciple.getContextObject('leads');
    }
  }

  changeTab(i) {
    this.Tabs[i].active = true;
    this.tabChange.emit(this.Tabs);
    this.updateData();
  }

  remove(index) {
    this.Tabs.splice(index, 1);
    this.updateData();
    this.tabChange.emit(this.Tabs);
  }

  updateData() {
    if (this.Tabs && this.Tabs.length > 0) {
      this.UserPrinciple.setContextObject('leads', this.Tabs);
    }
    else {
      this.UserPrinciple.removeContextObject('leads');
    }
    this.filterTabs();
  }

  changePage(type) {
    if (type === 'inc') {
      this.PageNo++;
    } else if (type === 'dec') {
      this.PageNo--;
    }
    this.filterTabs();
  }

  getLatPage() {
    if (this.Tabs.length === 0) {
      return 1
    }
    return Math.ceil(this.Tabs.length / 4)
  }

  filterTabs() {
    const page = this.PageNo;
    const tabs = this.Tabs;

    tabs.forEach(function (item) {
      item.customClass = 'hidden';
    });

    if (page === 1) {
      for (let i = 0; i < 4; i++) {
        if (tabs[i]) {
          tabs[i].customClass = '';
        }
      }
    } else {
      for (let i = ((page - 1) * 4); i < ((page - 1) * 4) + 4; i++) {
        if (tabs[i]) {
          tabs[i].customClass = '';
        }
      }
    }

    this.Tabs = tabs;
  }
}
