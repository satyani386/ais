import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { ViewSelectorComponent } from './view-selector.component';

import { TabsModule } from 'ngx-bootstrap';
import { Router, RouterModule } from '@angular/router';
import { AppUserPrinciple } from '../../models/user-principle/app.userPrinciple';
import { NewLeadsComponent } from '../../components/leads/new-leads/new-leads.component';

describe('ViewSelectorComponent', () => {
  let component: ViewSelectorComponent;
  let fixture: ComponentFixture<ViewSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSelectorComponent ],
      imports: [
        TabsModule.forRoot(),
      ],
      providers: [
        { 
          provide: Router, 
          useClass: class { navigate = jasmine.createSpy("navigate"); }
        },
        AppUserPrinciple
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // changeTab
  it('should change tab', (done) => {
    spyOn(component, 'updateData').and.callThrough();

    component.tabChange.subscribe(x => {
      expect(component.Tabs[0].active).toEqual(true);
      expect(x).toEqual(component.Tabs);
      done();
    });

    let tab = {
      name: 'New Lead',
      type: 'new',
      active: false,
      removable: true,
      id: 'newTab',
      isNew: true,
      component: NewLeadsComponent,
      phone: [{type: 'home'}],
      email: ['SpecTest@jasmine.com']
    };

    component.Tabs.push(tab);

    fixture.detectChanges();

    component.changeTab(0);

    fixture.detectChanges();

    expect(component.updateData).toHaveBeenCalled();
  });

  // it('should change tab (UI)', fakeAsync(() => {
  //   spyOn(component, 'changeTab').and.callThrough();

  //   let tab = {
  //     name: 'New Lead',
  //     type: 'new',
  //     active: true,
  //     removable: true,
  //     id: 'newTab',
  //     isNew: true,
  //     component: NewLeadsComponent,
  //     phone: [{type: 'home'}],
  //     email: ['']
  //   };

  //   component.Tabs.push(tab);
  //   component.Tabs.push(tab);
  //   component.Tabs.push(tab);
  //   component.Tabs.push(tab);
  //   component.Tabs.push(tab);

  //   tick();
  //   fixture.detectChanges();

  //   // let btn = fixture.debugElement.query(By.css('#change-tab-1'));
  //   // btn.triggerEventHandler('select', null);

  //   let btn = fixture.debugElement.query(By.css('.view-selector-page tab-container .nav-item'));
  //   btn.triggerEventHandler('select', null);

  //   fixture.detectChanges();

  //   expect(component.changeTab).toHaveBeenCalled();
  // }));

  // remove
  it('should remove tab', (done) => {
    spyOn(component, 'updateData').and.callThrough();

    component.tabChange.subscribe(x => {
      expect(component.Tabs[0]).not.toBeTruthy();
      expect(component.Tabs.length).toEqual(oldData - 1);
      expect(x).toEqual(component.Tabs);
      done();
    });

    let tab = {
      name: 'New Lead',
      type: 'new',
      active: false,
      removable: true,
      id: 'newTab',
      isNew: true,
      component: NewLeadsComponent,
      phone: [{type: 'home'}],
      email: ['SpecTest@jasmine.com']
    };

    component.Tabs.push(tab);

    let oldData = component.Tabs.length;

    fixture.detectChanges();

    component.remove(0);

    fixture.detectChanges();

    expect(component.updateData).toHaveBeenCalled();
  });

  // updateData (not in html, called by other functions)
  // updateData() {
  //   if (this.Tabs && this.Tabs.length > 0) {
  //     this.UserPrinciple.setContextObject('leads', this.Tabs);
  //   }
  //   else {
  //     this.UserPrinciple.removeContextObject('leads');
  //   }
  //   this.filterTabs();
  // }

  // changePage
  // changePage(type) {
  //   if (type === 'inc') {
  //     this.PageNo++;
  //   } else if (type === 'dec') {
  //     this.PageNo--;
  //   }
  //   this.filterTabs();
  // }
  // should have More Tabs left arrow enabled after 5 New Leads
  // should have More Tabs right arrow enabled after 5 New Leads and clicked left
  it('should call change page when click More Tabs left arrow', () => {
    spyOn(component, 'changePage').and.callThrough();
    spyOn(component, 'filterTabs').and.callThrough();

    let oldData = component.PageNo;

    let left = fixture.debugElement.query(By.css('#more-tabs-left'));
    let leftEl = left.nativeElement;
    leftEl.disabled = false;
    leftEl.click();

    fixture.detectChanges();

    expect(component.changePage).toHaveBeenCalledWith('dec');
    expect(component.filterTabs).toHaveBeenCalled();
    expect(component.PageNo).toEqual(oldData - 1);
  });

  it('should call change page when click More Tabs right arrow', () => {
    spyOn(component, 'changePage').and.callThrough();
    spyOn(component, 'filterTabs').and.callThrough();
    spyOn(component, 'getLatPage').and.callThrough();

    let oldData = component.PageNo;

    let right = fixture.debugElement.query(By.css('#more-tabs-right'));
    let rightEl = right.nativeElement;
    rightEl.disabled = false;
    rightEl.click();

    fixture.detectChanges();

    expect(component.changePage).toHaveBeenCalledWith('inc');
    expect(component.filterTabs).toHaveBeenCalled();
    expect(component.getLatPage).toHaveBeenCalled();
    expect(component.PageNo).toEqual(oldData + 1);
  });

  // getLastPage (not in html)
  // getLastPage() {
  //   if (this.Tabs.length === 0) {
  //     return 1
  //   }
  //   return Math.ceil(this.Tabs.length / 4)
  // }

  // filterTabs (not in html, called by other functions)
  // filterTabs() {
  //   const page = this.PageNo;
  //   const tabs = this.Tabs;

  //   tabs.forEach(function (item) {
  //     item.customClass = 'hidden';
  //   });

  //   if (page === 1) {
  //     for (let i = 0; i < 4; i++) {
  //       if (tabs[i]) {
  //         tabs[i].customClass = '';
  //       }
  //     }
  //   } else {
  //     for (let i = ((page - 1) * 4); i < ((page - 1) * 4) + 4; i++) {
  //       if (tabs[i]) {
  //         tabs[i].customClass = '';
  //       }
  //     }
  //   }

  //   this.Tabs = tabs;
  // }

  // Not in .ts, UI test for HTML
  it('should have More Tabs arrows disabled by default', () => {
    let left = fixture.debugElement.query(By.css('#more-tabs-left'));
    let leftEl = left.nativeElement;
    let right = fixture.debugElement.query(By.css('#more-tabs-right'));
    let rightEl = right.nativeElement;

    fixture.detectChanges();

    expect(leftEl.disabled).toEqual(true);
    expect(rightEl.disabled).toEqual(true);
  });
});
