import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import {UserProfileService} from '../../services/user-profile/user-profile.service';

@Component({
  selector: 'app-sticky-menu',
  templateUrl: './sticky-menu.component.html',
  styleUrls: ['./sticky-menu.component.scss'],
  animations: [
    trigger('StickyMenuAnimation', [
      state('hide', style({
        display: 'none'
      })),
      state('show', style({
        display: 'inline-block'
      })),
      transition('hide => show', animate('200ms 300ms'))
    ])
  ]
})
export class StickyMenuComponent implements OnInit {
  state: string = 'hide';
  public QuickLinks:any = [];
  constructor(private UserProfileService: UserProfileService) {
    const that = this;
    this.UserProfileService.getQuickLinks().subscribe(function(resp){
      that.QuickLinks = resp;
    });
  }

  ngOnInit() {
  }
  toggleStickyMenu() {
    this.state = (this.state === 'hide' ? 'show' : 'hide');
  }
}
