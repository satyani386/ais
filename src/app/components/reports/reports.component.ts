import { Component, OnInit } from '@angular/core';
import { LeadAdapterClass } from "../../services/uiadaptors/leadsAdapter";
import { UserleadComponent } from "../leads/userlead/userlead.component";
import { DaterangepickerConfig } from "ng2-daterangepicker";
import * as moment from "moment";


@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  /*ngx-bootstrap date range picker*/
  minDate = new Date(2017, 5, 10);
  maxDate = new Date(2018, 9, 15);
  bsValue: Date = new Date();
  bsRangeValue: any = [new Date(2017, 7, 4), new Date(2017, 7, 20)];

  /*Daterangepicker options*/
  public mainInput = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(6, 'month')
  };
  scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  scrollbarOptions2 = { axis: 'y', theme: 'dark-3' };
  public scrollOptions = { axis: 'y', theme: 'dark-3' };
  public Data = [];
  constructor(private LeadAdapterClass: LeadAdapterClass, private DateRanageConfig: DaterangepickerConfig) {

    this.DateRanageConfig.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: false,
      ranges: {
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(4, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
      }
    };
    this.Data = [
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Eric Welgat', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Sai Charan', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Rahul Datir', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'louis Soh', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Demond Jordon', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Mahyar Babaie', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Nancy chan', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Tim Sherman', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Eric Welgat', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Sai Charan', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'rahul Datir', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'louis Soh', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Demond Jordon', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Mahyar Babaie', 'product': 'Auto', 'time': '5 min ago' },
      { 'name': 'Nancy chan', 'product': 'Auto', 'time': '5 min ago' },
    ]
  }

  ngOnInit() {
  }
  showLead(obj) {
    console.log('Open Lead');
    obj['component'] = UserleadComponent;
    obj['type'] = 'my-leads';
    obj['isNew'] = true;
    obj['active'] = true;
    obj['removable'] = true;
    console.log(obj);
    this.LeadAdapterClass.setOnCall(obj)
  }
}
