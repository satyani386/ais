import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';

import { ReportsComponent } from './reports.component';
import { LeadAdapterClass } from "../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../services/leads/leads.service';
import { HttpModule } from '@angular/http';
import { Daterangepicker } from "ng2-daterangepicker";

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

fdescribe('ReportsComponent', () => {
  let component: ReportsComponent;
  let fixture: ComponentFixture<ReportsComponent>;
  let leadAdapterClass: LeadAdapterClass;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsComponent ],
      imports: [
        HttpModule,
        Daterangepicker,
        MalihuScrollbarModule.forRoot()
      ],
      providers: [
        LeadAdapterClass,
        LeadsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsComponent);
    component = fixture.componentInstance;
    leadAdapterClass = fixture.debugElement.injector.get(LeadAdapterClass);
    spyOn(leadAdapterClass, 'setOnCall').and.callThrough();

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // it('should show calendar', () => {

  // });

  // showLead
  it('should show lead', () => {
    spyOn(component, 'showLead').and.callThrough();

    let btn = fixture.debugElement.query(By.css('#reports-show-lead-1'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.showLead).toHaveBeenCalled();
    expect(leadAdapterClass.setOnCall).toHaveBeenCalled();
  });
});
