import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';

import { RecentActivityComponent } from './recent-activity.component';
import { UserleadComponent } from "../leads/userlead/userlead.component"

import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { LeadAdapterClass } from "../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../services/leads/leads.service';
import { HttpModule } from '@angular/http';
import { TabsModule, AccordionModule } from 'ngx-bootstrap';

import { FormsModule } from '@angular/forms';

fdescribe('RecentActivityComponent', () => {
  let component: RecentActivityComponent;
  let fixture: ComponentFixture<RecentActivityComponent>;
  let leadAdapterClass: LeadAdapterClass;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        RecentActivityComponent,
        UserleadComponent
      ],
      imports: [
        MalihuScrollbarModule.forRoot(),
        HttpModule,
        TabsModule.forRoot(),
        AccordionModule.forRoot(),
        FormsModule
      ],
      providers: [
        LeadAdapterClass,
        LeadsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentActivityComponent);
    component = fixture.componentInstance;
    leadAdapterClass = fixture.debugElement.injector.get(LeadAdapterClass);
    spyOn(leadAdapterClass, 'setOnCall').and.callThrough();

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // showLead
  it('should show lead', () => {
    spyOn(component, 'showLead').and.callThrough();

    let btn = fixture.debugElement.query(By.css('#ra-show-lead-1'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.showLead).toHaveBeenCalled();
    expect(leadAdapterClass.setOnCall).toHaveBeenCalled();
  });
});
