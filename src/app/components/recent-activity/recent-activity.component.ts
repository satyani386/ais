import { Component, OnInit } from '@angular/core';
import {LeadAdapterClass} from "../../services/uiadaptors/leadsAdapter";
import {UserleadComponent} from "../leads/userlead/userlead.component"

@Component({
  selector: 'app-recent-activity',
  templateUrl: './recent-activity.component.html',
  styleUrls: ['./recent-activity.component.scss']
})
export class RecentActivityComponent implements OnInit {
  scrollbarOptions2 = {axis: 'y', theme: 'dark-3'};
  public Data = [];
  constructor(private LeadAdapterClass: LeadAdapterClass) {
    this.Data = [
      {'name':'Tim Sherman' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Eric Welgat' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Sai Charan' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Rahul Datir' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Louis Soh' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Demond Jordon' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Mahyar Babaie' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Nancy chan' ,  'product':'Auto', 'time':'5 min ago'},
       {'name':'Tim Sherman' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Eric Welgat' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Sai Charan' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Rahul Datir' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Louis Soh' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Demond Jordon' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Mahyar Babaie' ,  'product':'Auto', 'time':'5 min ago'},
      {'name':'Nancy chan' ,  'product':'Auto', 'time':'5 min ago'},
   
    ]
  }

  ngOnInit() {
  }
   showLead(obj) {
     obj = JSON.parse(JSON.stringify(obj));
    console.log('Open Lead');
    obj['component'] = UserleadComponent;
    obj['type'] = 'my-leads';
    obj['isNew'] = true;
    obj['active'] = true;
    obj['removable'] = true;
    obj.phone = [{ type: 'home' }];
    obj.email = [''];
    obj.product = [{ type: 'Auto' }];
    console.log(obj);
    this.LeadAdapterClass.setOnCall(obj)
  }
}
