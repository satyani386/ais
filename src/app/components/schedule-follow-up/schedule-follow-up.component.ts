import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap"


@Component({
  selector: 'app-schedule-follow-up',
  templateUrl: './schedule-follow-up.component.html',
  styleUrls: ['./schedule-follow-up.component.scss']
})
export class ScheduleFollowUpComponent implements OnInit {

  constructor(public modelRef: BsModalRef) { }

  ngOnInit() {
  }

}
