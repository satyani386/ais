import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleFollowUpComponent } from './schedule-follow-up.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

fdescribe('ScheduleFollowUpComponent', () => {
  let component: ScheduleFollowUpComponent;
  let fixture: ComponentFixture<ScheduleFollowUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleFollowUpComponent ],
      providers: [
        BsModalRef,
        BsModalService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleFollowUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
