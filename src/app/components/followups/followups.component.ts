import {Component, OnInit} from '@angular/core';
import {LeadAdapterClass} from "../../services/uiadaptors/leadsAdapter";
import {UserleadComponent} from "../leads/userlead/userlead.component"
import {BsModalService,BsModalRef} from 'ngx-bootstrap/modal';
import {NotificationComponent} from "./closefolloup/notification.component"


@Component({
  selector: 'app-followups',
  templateUrl: './followups.component.html',
  styleUrls: ['./followups.component.scss']
})
export class FollowupsComponent implements OnInit {
  public scrollbarOptions = { axis: 'y', theme: 'dark-3' };
  public MyFollowups: any = [];
  public Actions = {
    all: false,
    today: true,
    open: false,
    closed: false
  };
  public close1state = true;
  public modalRef: BsModalRef;

  public ActionCondition = 'today';

  public time = {
    hours: ["--",1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    minutes: ["--",0, 15, 30, 45]
  };

  constructor(private LeadAdapterClass: LeadAdapterClass, private ModalService: BsModalService) {
    this.MyFollowups = [
      {
        date: new Date(),
        time: '3 : 15 PM',
        name: 'John Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },{
        date: new Date(),
        time: '3 : 15 PM',
        name: 'John Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date(),
        time: '3 : 15 PM',
        name: 'John Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date(),
        time: '3 : 15 PM',
        name: 'John Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date(),
        time: '3 : 15 PM',
        name: 'John Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date(),
        time: '3 : 15 PM',
        name: 'John Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date('06/03/17'),
        time: '3 : 15 PM',
        name: 'Steve Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date(),
        time: '3 : 15 PM',
        name: 'Rock Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date('06/03/17'),
        time: '3 : 15 PM',
        name: 'Ron Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: true
      },
      {
        date: new Date(),
        time: '3 : 15 PM',
        name: 'Samantha',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date('06/03/17'),
        time: '3 : 15 PM',
        name: 'Bucky',
        phone: '555-555-1234',
        product: 'auto',
        closed: true
      },
      {
        date: new Date('06/03/17'),
        time: '3 : 15 PM',
        name: 'Susan',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      },
      {
        date: new Date('06/03/17'),
        time: undefined,
        name: 'John Smith',
        phone: '555-555-1234',
        product: 'auto',
        closed: false
      }
    ];
  }

  ngOnInit() {
  }

  selectmy(event) {
    const state = event.target['checked'];
    this.MyFollowups.forEach(function (item) {
      item.selected = state;
    });
    this.changeState(state);
  }

  edit(obj: any, index) {
    obj.edit = !obj.edit;
  }

  save(obj, i) {
    obj.edit = false;
  }

  showRow(obj, event) {
    if (
      event.target.type !== 'checkbox' &&
      event.target.getAttribute('role') !== 'name' &&
      event.target.getAttribute('role') !== 'remainder'
    ) {
      obj.show = !obj.show
    }
  }

  showLead(obj) {
    console.log('Open Lead');
    obj['component'] = UserleadComponent;
    obj['type'] = 'my-leads';
    obj['isNew'] = true;
    obj['active'] = true;
    obj['removable'] = true;
    console.log(obj);
    this.LeadAdapterClass.setOnCall(obj)
  }

  filterResults(type) {
    const that = this;
    Object.keys(this.Actions).forEach(function (item) {
      that.Actions[item] = false;
    });

    this.Actions[type] = true;
    this.ActionCondition = type;
  }

  changeState(event) {
    let isFound = false;
    this.MyFollowups.forEach(function (item) {
      if (item.selected) {
        isFound = true;
      }
    });

    this.close1state = !isFound;
  }

  closePopup() {
    this.modalRef = this.ModalService.show(NotificationComponent);
    this.modalRef.content.message = 'Are you sure to close followup?'
  }

  setRemainder(obj) {
    console.log(obj);
    const now = new Date();
    const then = new Date(obj.date);
    if(!obj.time){

      this.showNotificaiton('Please set time to set remainder.' , false);
      return false;
    }

    if(/PM/.test(obj.time)){
      const hour = (obj.time.split(':')[0].trim() - 0) + 12;
      then.setHours(hour);
    }else{
      const hour = (obj.time.split(':')[0].trim() - 0);
      then.setHours(hour);
    }

    const mins = (obj.time.split(' ')[2].trim() - 0);
    then.setMinutes(mins);


    if(now.getTime() < then.getTime()){
      this.showNotificaiton('Remainder set success.' , false);
      obj.remainder = true;
    }else{
      this.showNotificaiton('Please select future date / time.' , false);
    }
  }

  showNotificaiton(message, buttons){
    this.modalRef = this.ModalService.show(NotificationComponent);
    this.modalRef.content.message = message;
    this.modalRef.content.buttons = buttons;
  }
}
