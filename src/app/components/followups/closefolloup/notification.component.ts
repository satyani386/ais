import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap"

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  public message: any = '';
  constructor(public modelRef: BsModalRef) { }

  ngOnInit() {
  }

}
