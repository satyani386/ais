import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { FollowupsComponent } from './followups.component';
import { MyFolloupsPipe } from '../../pipes/util.pipe';
import { FollowuprowclassDirective } from '../../directives/followuprowclass.directive';
import { LeadAdapterClass } from "../../services/uiadaptors/leadsAdapter";
import { LeadsService } from '../../services/leads/leads.service';
import { UserleadComponent } from "../leads/userlead/userlead.component";
import { NotificationComponent } from "./closefolloup/notification.component"

import { HttpModule } from '@angular/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TabsModule, AccordionModule } from 'ngx-bootstrap';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';

import { DatepickerModule } from 'angular2-material-datepicker'

fdescribe('FollowupsComponent', () => {
  let component: FollowupsComponent;
  let fixture: ComponentFixture<FollowupsComponent>;
  let bsModalService: BsModalService;
  let bsModalRef: BsModalRef;
  let leadAdapterClass: LeadAdapterClass;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        FollowupsComponent, 
        MyFolloupsPipe,
        FollowuprowclassDirective,
        UserleadComponent,
        NotificationComponent
      ],
      imports: [
        FormsModule,
        BsDatepickerModule.forRoot(),
        HttpModule,
        TabsModule.forRoot(),
        AccordionModule.forRoot(),
        MalihuScrollbarModule.forRoot(),
        DatepickerModule
      ],
      providers: [
        LeadAdapterClass,
        LeadsService,
        BsModalService,
        BsModalRef
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowupsComponent);
    component = fixture.componentInstance;
    bsModalService = fixture.debugElement.injector.get(BsModalService);
    bsModalRef = fixture.debugElement.injector.get(BsModalRef);
    leadAdapterClass = fixture.debugElement.injector.get(LeadAdapterClass);
    spyOn(bsModalService, 'show').and.callThrough();
    spyOn(bsModalRef, 'hide').and.callThrough();
    spyOn(leadAdapterClass, 'setOnCall').and.callThrough();

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // Defaults
  it('should show today\'s follow-ups by default', () => {
    expect(component.Actions.all).toEqual(false);
    expect(component.Actions.today).toEqual(true);
    expect(component.Actions.open).toEqual(false);
    expect(component.Actions.closed).toEqual(false);
    expect(component.ActionCondition).toEqual('today');
  });

  it('should disable close follow-up button by default', () => {
    let close = fixture.debugElement.query(By.css('#close-my-followup'));
    let closeEl = close.nativeElement;
    
    expect(closeEl.disabled).toEqual(true);
  });
  
  // selectmy
  it('should show select all my follow-ups', async(() => {
    spyOn(component, 'selectmy').and.callThrough();
    
    let event = {
      'target': {
        'checked': true,
      }
    };

    let checkbox = fixture.debugElement.query(By.css('#my-followups-check-all'));
    let checkboxEl = checkbox.nativeElement;
    checkboxEl.click(event);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.selectmy).toHaveBeenCalled();
      expect(component.close1state).toEqual(false);
      
      component.MyFollowups.forEach(function (item) {
        expect(item.selected).toEqual(event.target['checked']);
      });
    })
  }));

  // edit
  it('should show follow-up message in edit', async(() => {
    spyOn(component, 'edit').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-edit-1'));

    let followup = {
      date: new Date(),
      time: '3 : 15 PM',
      name: 'John Smith',
      phone: '555-555-1234',
      product: 'auto',
      closed: false,
      edit: false
    };

    let index = 0;

    let btnEl = btn.nativeElement;
    btnEl.click(followup, index);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      let message = fixture.debugElement.query(By.css('#followup-message'));
      let messageEl = message.nativeElement;
  
      expect(component.edit).toHaveBeenCalled();
      expect(messageEl).toBeTruthy();
      // expect(followup.edit).toEqual(true);
    })

  }));

  // save
  it('should show save my follow-up', async(() => {
    spyOn(component, 'save').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-edit-1'));

    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    let save = fixture.debugElement.query(By.css('#my-followups-save-1'));

    let followup = {
      date: new Date(),
      time: '3 : 15 PM',
      name: 'John Smith',
      phone: '555-555-1234',
      product: 'auto',
      closed: false,
      edit: true
    };

    let index = 1;

    let saveEl = save.nativeElement;
    saveEl.click(followup, index);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.save).toHaveBeenCalled();
      // expect(followup.edit).toEqual(false);
    })
  }));

  // showRow
  it('should show row', () => {
    spyOn(component, 'showRow').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-show-row-1'));

    // let followup = {
    //   date: new Date(),
    //   time: '3 : 15 PM',
    //   name: 'John Smith',
    //   phone: '555-555-1234',
    //   product: 'auto',
    //   closed: false,
    //   show: false
    // };

    // btn.triggerEventHandler('click', null);
    let btnEl = btn.nativeElement;
    btnEl.click();

    fixture.detectChanges();

    // let message = fixture.debugElement.query(By.css('#followup-message'));
    // let messageEl = message.nativeElement;
  
    expect(component.showRow).toHaveBeenCalled();
    // expect(messageEl).toBeTruthy();
    // expect(followup.show).toEqual(true);
  });

  // showLead
  it('should show lead', async(() => {
    spyOn(component, 'showLead').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-show-lead-1'));

    // let obj = {};

    btn.triggerEventHandler('click', null);
    // btn.triggerEventHandler('click', obj);

    fixture.detectChanges();

    // leadAdapterClass.setOnCall().subscribe(
    //   (result) => {
    //     expect(result).toBeDefined();
    //   }
    // );

    fixture.whenStable().then(() => {
      expect(component.showLead).toHaveBeenCalled();
      // expect(obj['component']).toEqual(UserleadComponent);
      // expect(obj['type']).toEqual('my-leads');
      // expect(obj['isNew']).toEqual(true)
      // expect(obj['active']).toEqual(true);
      // expect(obj['removable']).toEqual(true);
      // expect(leadAdapterClass.setOnCall).toHaveBeenCalledWith(obj);
      expect(leadAdapterClass.setOnCall).toHaveBeenCalled();
    })
  }));

  // filterResults
  it('should show all follow-ups', () => {
    spyOn(component, 'filterResults').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-all'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.Actions.all).toEqual(true);
    expect(component.Actions.today).toEqual(false);
    expect(component.Actions.open).toEqual(false);
    expect(component.Actions.closed).toEqual(false);
    expect(component.ActionCondition).toEqual('all');
    expect(component.filterResults).toHaveBeenCalled();
  });

  it('should show today\'s follow-ups', () => {
    spyOn(component, 'filterResults').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-today'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.Actions.all).toEqual(false);
    expect(component.Actions.today).toEqual(true);
    expect(component.Actions.open).toEqual(false);
    expect(component.Actions.closed).toEqual(false);
    expect(component.ActionCondition).toEqual('today');
    expect(component.filterResults).toHaveBeenCalled();
  });

  it('should show open follow-ups', () => {
    spyOn(component, 'filterResults').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-open'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.Actions.all).toEqual(false);
    expect(component.Actions.today).toEqual(false);
    expect(component.Actions.open).toEqual(true);
    expect(component.Actions.closed).toEqual(false);
    expect(component.ActionCondition).toEqual('open');
    expect(component.filterResults).toHaveBeenCalled();
  });

  it('should show closed follow-ups', () => {
    spyOn(component, 'filterResults').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-closed'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();
    
    expect(component.Actions.all).toEqual(false);
    expect(component.Actions.today).toEqual(false);
    expect(component.Actions.open).toEqual(false);
    expect(component.Actions.closed).toEqual(true);
    expect(component.ActionCondition).toEqual('closed');
    expect(component.filterResults).toHaveBeenCalled();
  });

  // changeState
  it('should enable close follow-up button', async(() => {
    // Check follow up so close follow up button is enabled
    spyOn(component, 'changeState').and.callThrough();
    let checkbox = fixture.debugElement.query(By.css('#my-followups-checkbox-1'));
    // checkbox.triggerEventHandler('click', null);
    let checkboxEl = checkbox.nativeElement;
    checkboxEl.click();
    // checkboxEl.checked = true;
    // checkboxEl.selected = true;
    // checkboxEl.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.changeState).toHaveBeenCalled();

      let close = fixture.debugElement.query(By.css('#close-my-followup'));
      let closeEl = close.nativeElement;
    
      expect(component.changeState).toHaveBeenCalled();
      expect(component.close1state).toEqual(false);
      expect(closeEl.disabled).toEqual(false);
    })
  }));

  // closePopup
  it('should show confirmation modal for close follow-up', async(() => {
    spyOn(component, 'closePopup').and.callThrough();
    let close = fixture.debugElement.query(By.css('#close-my-followup'));
    close.triggerEventHandler('click', null);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.closePopup).toHaveBeenCalled();
      expect(bsModalService.show).toHaveBeenCalledWith(NotificationComponent);
      expect(bsModalRef).toBeTruthy();
    })
  }));

  // setReminder
  it('should show modal for set reminder', async(() => {
    spyOn(component, 'setRemainder').and.callThrough();
    let btn = fixture.debugElement.query(By.css('#my-followups-set-reminder-1'));
    btn.triggerEventHandler('click', null);

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.setRemainder).toHaveBeenCalled();
      expect(bsModalService.show).toHaveBeenCalledWith(NotificationComponent);
      expect(bsModalRef).toBeTruthy();
    })
  }));

  // showNotificaiton (not in html, called by setRemainder)
});
