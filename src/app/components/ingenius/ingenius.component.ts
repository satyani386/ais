import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnInit} from '@angular/core';
import {IngeniusAdapterClass} from '../../services/uiadaptors/IngeniusAdapter';
import {LeadAdapterClass} from '../../services/uiadaptors/leadsAdapter';

declare var ice: any;

@Component({
  selector: 'app-ingenius',
  templateUrl: './ingenius.component.html',
  styleUrls: ['./ingenius.component.scss'],
  animations: [
    trigger('StickyMenuAnimation', [
      state('hide', style({
        display: 'none'
      })),
      state('show', style({
        display: 'inline-block'
      })),
      transition('hide => show', animate('200ms 300ms'))
    ])
  ]

})
export class IngeniusComponent implements OnInit {
  public state = 'hide';

  public networkId: any;
  public username: any;
  public password: any;
  public mode: any;
  //public value:any=true;

  @Input() primaryLineNum: any;
  @Input() AGENT_ID: any;
  @Input() PASSWORD: any;
  ctiApi: any;
  iceEndPoint: string;
  apiSetting: APISetting;
  eventData: EventData;
  callStartedEvtCallback: any;
  showHide: boolean;
  hideShow: boolean;
  public callData: any = [];

  public phonecalls = {};

  constructor(private LeadAdapterClass: LeadAdapterClass, private Ingeniusadaptor: IngeniusAdapterClass) {
    //this.ctiApi = new ice.api.CtiApi();

    this.Ingeniusadaptor.getApiSetting().subscribe(apiSetting => {
      this.apiSetting = apiSetting;
    }, function (err) {
      console.log('Error');
    });
    this.showHide = true;
    this.hideShow = false;
    // this.eventData = {}; // initializing with an empty object
  }

  ngOnInit() {

  }


  toggleStickyMenu() {
    this.state = (this.state === 'hide' ? 'show' : 'hide');
  }


  // connect() {
  //     console.log("calling connect function");
  //     //Subscribe to events before calling initialize.
  //     // callStartedEvtCallback(); //Calling the returned callback unsubscribes the event listener.
  //     ice.api.CtiApi.Subscribe(ice.api.EventType.CallEnded, this.handleCtiEvent);
  //     ice.api.CtiApi.Subscribe(ice.api.EventType.CallChanged, this.handleCtiEvent);
  //     ice.api.CtiApi.Subscribe(ice.api.EventType.ServerPriorityChanged, this.handleCtiEvent);
  //     ice.api.CtiApi.Subscribe(ice.api.EventType.HostStatusChanged, this.handleCtiEvent);
  //     ice.api.CtiApi.Subscribe(ice.api.EventType.MonitorStatusChanged, this.handleCtiEvent);
  //     ice.api.CtiApi.Subscribe(ice.api.EventType.TelephonyConnectionLost, this.handleCtiEvent);

  //     this.ctiApi.Connect({
  //         UserId: this.apiSetting.userId,
  //         Userdisplayname: this.apiSetting.userDisplayName,
  //         IceEndpoint: this.apiSetting.iceApiServiceEndpoint,
  //         AppName: this.apiSetting.appName,
  //         AppKey: this.apiSetting.appKey
  //     }).then(function (initializeState) {
  //         console.log((new Date).toLocaleString());
  //         // console.log("Initialize complete: ", initializeState);
  //         // console.log(" Success ", initializeState.Success);
  //         // console.log(" ResponseCode ", initializeState.ResponseCode);
  //         // console.log(" Reason ", initializeState.Reason);
  //         // console.log(" Data: ", initializeState.Data);
  //         // console.log("  IsMonitored ", initializeState.Data.IsMonitored);
  //         // console.log("  IsTelephonyConnected ", initializeState.Data.IsTelephonyConnected);
  //         // console.log("  MonitorParameters ", initializeState.Data.MonitorParameters);
  //     }).catch(function (error) {
  //         console.log("Error initializing ", error);
  //     });
  // }

  // disconnect() {
  //     console.log("calling disconnect function");
  //     this.ctiApi.StopMonitor().then(function (state) {
  //         //parse state
  //         var success = state.Success;
  //     });
  // }

  startMonitor() {
    const that = this;
    this.showHide = !this.showHide;
    this.hideShow = !this.hideShow;

    //Subscribe to events before calling initialize.
    this.callStartedEvtCallback = ice.api.CtiApi.Subscribe(ice.api.EventType.CallStarted, this.handleCtiEvent.bind(this));
    // callStartedEvtCallback(); //Calling the returned callback unsubscribes the event listener.
    //ice.api.CtiApi.Subscribe(ice.api.EventType.CallStarted, this.handleCtiEvent.bind(this));
    ice.api.CtiApi.Subscribe(ice.api.EventType.CallEnded, this.handleCtiEvent.bind(this));
    ice.api.CtiApi.Subscribe(ice.api.EventType.MonitorStatusChanged, this.handleCtiEvent.bind(this));
    ice.api.CtiApi.Subscribe(ice.api.EventType.CallChanged, this.handleCtiEvent.bind(this));
    ice.api.CtiApi.Subscribe(ice.api.EventType.ServerPriorityChanged, this.handleCtiEvent.bind(this));
    ice.api.CtiApi.Subscribe(ice.api.EventType.HostStatusChanged, this.handleCtiEvent.bind(this));
    ice.api.CtiApi.Subscribe(ice.api.EventType.MonitorStatusChanged, this.handleCtiEvent);
    ice.api.CtiApi.Subscribe(ice.api.EventType.TelephonyConnectionLost, this.handleCtiEvent.bind(this));

    this.ctiApi.Connect({
     UserId: this.apiSetting.userId,
            Userdisplayname: this.apiSetting.userDisplayName,
            IceEndpoint: this.apiSetting.iceApiServiceEndpoint,
            AppName: this.apiSetting.appName,
            AppKey: this.apiSetting.appKey
    }).then(function (initializeState) {
      console.log('Initialize started');
      that.Ingeniusadaptor.setConnectionObj(that.ctiApi);
    }).catch(function (error) {
      console.log("Error initializing ", error);
    });

    //Parameter structure for Avaya telephony provider
    this.ctiApi.StartMonitor({
      "PRIMARY_LINE_NUMBER": this.primaryLineNum,
      //  "AGENT_ID": "73280",
      //  "PASSWORD": "73280",
      //  "WORK_MODE":"AUTO_IN"
    }).then(function (commandResponse) {
      console.log('Monitor started')
      //Ready to send commands and receive events.
      //ctiApi.Dial(71482);

    }).catch(function (commandResponse) {
      console.error("Error started monitor ", commandResponse);
    });

    //test call
    this.eventData = {
      logCode: 10,
      logDate: (new Date).toLocaleString(),
      primaryLineNumber: this.primaryLineNum,
      callId: 'ae990c2d-52b8-44a3-8204-a4b8c4d0bb0c',
      callStateCode: 1,
      eventTypeCode: 0,
      isMonitored: true,
      isTelephonyConnected: true,
      remotePartyName: 'csuTest',
      remotePartyNumber: '4047865038',
      responseCode: 1,
      responseReason: 'ok'
    }

    //this.Ingeniusadaptor.postEventLogs(this.eventData)
  }

  stopMonitor() {
    this.showHide = !this.showHide;
    this.hideShow = !this.hideShow;
    this.ctiApi.StopMonitor().then(function (state) {
      //parse state
      const success = state.Success;
    });
    this.callStartedEvtCallback();
  }

  handleCtiEvent(event) {
    const that = this;

    // try {
    //   const eventDataLog = {
    //     logCode: 10,
    //     logDate: (new Date).toLocaleString(),
    //     primaryLineNumber: this.primaryLineNum,
    //     callId: event.CallId,
    //     callStateCode: event.callStateCode,
    //     eventTypeCode: event.eventTypeCode,
    //     isMonitored: event.isMonitored,
    //     isTelephonyConnected: event.isTelephonyConnected,
    //     remotePartyName: event.RemotePartyName,
    //     remotePartyNumber: event.RemotePartyNumber,
    //     responseCode: event.responseCode,
    //     responseReason: event.responseReason
    //   };
    // } catch (error) {
    //   console.log(error)
    // }
    //this.Ingeniusadaptor.postEventLogs(eventDataLog)

    if (event.EventType === 0 && event.State === 0) {
      // this.phonecalls[event.RemotePartyNumber] = event
      console.log(event);
      event.type = 'new-lead';
      event.isNew = true;
      this.LeadAdapterClass.setOnCall(event);
      const phoneNumber = {
        phoneNumber: 6619743199
      };

      this.Ingeniusadaptor.getPhoneData(phoneNumber).subscribe(function (data) {
        that.callData = data;
      }, function (err) {
        console.log(err)
      })
    } else if (event.EventType === 2) {
      this.Ingeniusadaptor.setOnCallResults(this.callData);
    }
    else if (event.EventType === 1) {
      delete this.phonecalls[event.RemotePartyNumber];
    }
  }
}

interface APISetting {
  appName: string;
  appKey: string;
  userId: string;
  userDisplayName: string;
  iceApiServiceEndpoint: string;
}

export class EventData {
  // id: number;
  logCode?: number;
  logDate?: string; //"2017-10-05T11:33:23";
  primaryLineNumber?: string;
  callId?: string;
  callStateCode?: number;
  eventTypeCode?: number;
  isMonitored?: boolean;
  isTelephonyConnected?: boolean;
  remotePartyName?: string;
  remotePartyNumber?: string;
  responseCode?: number;
  responseReason?: string;
}
