import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { IngeniusComponent } from './ingenius.component';

import { IngeniusService } from '../../services/ingenius/ingenius.service';
import { LeadsService } from '../../services/leads/leads.service';

import * as _ from 'underscore';

import { IngeniusAdapterClass } from '../../services/uiadaptors/IngeniusAdapter';

import { Http, HttpModule } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('IngeniusComponent', () => {
  let component: IngeniusComponent;
  let fixture: ComponentFixture<IngeniusComponent>;
  let ingeniusService: IngeniusService;
  let leadsService: LeadsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngeniusComponent ],
      imports: [
        FormsModule,
        HttpModule,
        BrowserAnimationsModule
      ],
      providers: [
        IngeniusService,
        LeadsService,
        IngeniusAdapterClass
      ],
    })
    .compileComponents();

    ingeniusService = getTestBed().get(IngeniusService);
    leadsService = getTestBed().get(LeadsService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngeniusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  // it('startMonitor should call IngeniusService.setConnectionObj', async(() => {
  //   spyOn(ingeniusService, 'setConnectionObj');

  //   fixture.detectChanges();

  //   fixture.whenStable().then(() => {
  //     ingeniusService.getApiSetting();
      
  //     fixture.detectChanges();
      
  //     component.startMonitor();

  //     fixture.detectChanges();
  
  //     expect(ingeniusService.setConnectionObj).toHaveBeenCalled();
  //   })
  // }));

});
